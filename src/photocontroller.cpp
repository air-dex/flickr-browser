#include "photocontroller.hpp"

#include <QQmlEngine>
#include "flickrbrowser.hpp"

PhotoController::PhotoController() : QObject(), photo() {}

void PhotoController::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<PhotoController>(FlickrBrowser::CORE_URI.constData(),
									 FlickrBrowser::MAJOR_VERSION,
									 FlickrBrowser::MINOR_VERSION,
									 "PhotoController");
}

Photo * PhotoController::getPhoto() const {
	return const_cast<Photo *>(&photo);
}

void PhotoController::setPhoto(QString photoID) {
	photo = Photo::getPhoto(photoID);
	emit photoChanged();
}

bool PhotoController::savePhoto(QString photoPath) {
	// Overwrite previous file
	if (QFile::exists(photoPath)) {
		QFile::remove(photoPath);
	}

	return QFile::copy(photo.getFileName(), photoPath);
}

bool PhotoController::savePhoto(QUrl photoURL) {
	return savePhoto(photoURL.toLocalFile());
}
