#include "wallpapercontroller.hpp"

#include <QQmlEngine>
#include "flickrbrowser.hpp"
#include "wallpapermanager.hpp"

WallpaperController::WallpaperController() :
	QObject(),
	wpm(FlickrBrowser::getWallpaperManager())
{
	wpm->retrieveWallpaperDatas();
}

WallpaperController::~WallpaperController() {
	delete wpm;
	wpm = nullptr;
}

void WallpaperController::declareQML()
{
	// @uri FlickrBrowserCore
	qmlRegisterType<WallpaperController>(FlickrBrowser::CORE_URI.constData(),
										 FlickrBrowser::MAJOR_VERSION,
										 FlickrBrowser::MINOR_VERSION,
										 "WallpaperController");
}

bool WallpaperController::setAsWallpaper(QString wallpaperPath, int wallpaperOpt, QColor newColor) {
	WpOpts wopt = WpOpts(wallpaperOpt);

	bool res = wpm->setWallpaper(wallpaperPath, wopt, newColor);

	if (res) {
		refreshWallpaperDatas();
	}

	return res;
}

void WallpaperController::refreshWallpaperDatas() {
	wpm->retrieveWallpaperDatas();
	emit wallpaperChanged();
}

QString WallpaperController::getWallpaperPath() const {
	return wpm->getWpPath();
}

WpOpts WallpaperController::getWallpaperOption() const {
	return wpm->getWpOpts();
}

QColor WallpaperController::getBackgroundColor() const {
	return wpm->getBgColor();
}
