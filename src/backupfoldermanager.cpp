#include "backupfoldermanager.hpp"

#include <QSaveFile>
#include <QtDebug>
#include "backuperror.hpp"
#include "backuputils.hpp"

BackupFolderManager::BackupFolderManager() :
	BackupManager(),
	albumEffFolder(),
	addAlbumsToRollback(false),
	addAlbumContentToRollback(false)
{}

/***************************
 * BackupManager interface *
 ***************************/

bool BackupFolderManager::initBackup(QString backupName, QString & errMsg) {
	rollbackEntity.clear();
	return writeFolder(backupFolder,
					   backupName,
					   effFolder,
					   false,
					   true,
					   rollbackEntity,
					   addAlbumsToRollback,
					   errMsg);
}

bool BackupFolderManager::initAlbumBackup(const Album & album, QString & errMsg)
{
	return writeFolder(effFolder,
					   album.getTitle(),
					   albumEffFolder,
					   true,
					   addAlbumsToRollback,
					   rollbackEntity,
					   addAlbumContentToRollback,
					   errMsg);
}

bool BackupFolderManager::writeFolder(const QDir & root,
									  const QString newFolder,
									  QDir & newRoot,
									  const bool strictly,
									  const bool addRollbackEntities,
									  QFileInfoList & rollbackEntities,
									  bool & addRollBackEntitiesFlag,
									  QString & errMsg) const
{
	QString effectivePath = QDir::cleanPath(root.absoluteFilePath(newFolder));
	QString rootPath = root.absolutePath();

	/*
	 * Simulating new effective root folder, in order not to pollute
	 * user's computer with useless folders.
	 *
	 * Security: the new root should be in the old root.
	 */
	if (!BackupUtils::isDirInRoot(QDir(effectivePath), root, strictly)) {
		// Don't take risks. Abort.
		errMsg = BackupError::FOLDER_OUT_OF_LOCATION.arg(effectivePath);
		return false;
	}

	// Creating the new folder
	if (!root.mkpath(newFolder)) {
		/*
		 * It goes there if new folder's title contains some forbidden
		 * characters for folders.
		 */
		errMsg = BackupError::CANNOT_CREATE_FOLDER.arg(newFolder, rootPath);
		return false;
	}

	newRoot = root;
	newRoot.setFilter(BackupUtils::FILTERZ);

	// Opening the new folder.
	if (!newRoot.cd(newFolder)) {
		errMsg = BackupError::CANNOT_OPEN_FOLDER.arg(effectivePath);
		return false;
	}

	// Security: the new root should (strictly?) be in inside the old one.
	if (!BackupUtils::isDirInRoot(newRoot, root, strictly)) {
		errMsg = BackupError::FOLDER_OUT_OF_LOCATION.arg(effectivePath);
		return false;
	}

	// Add some rollback entities?
	if (addRollbackEntities) {
		bool nrIsRoot = newRoot.absolutePath() == rootPath;
		bool nrDistinctEmptyRoot = !nrIsRoot && newRoot.count() == 0;

		addRollBackEntitiesFlag = !nrDistinctEmptyRoot;

		if (nrDistinctEmptyRoot)  {
			QDir emptyRoot = newRoot;
			QDir parent = emptyRoot;
			parent.setFilter(BackupUtils::FILTERZ);
			bool emptyParentIsNotRoot;

			do {
				if (!parent.cdUp()) {
					// Stop it. At least emptyRoot is an acceptable value.
					emit const_cast<BackupFolderManager *>(this)->
							warning(BackupError::CANNOT_OPEN_FOLDER.arg(
								parent.absolutePath()
							));
					break;
				}

				emptyParentIsNotRoot =    parent.count() < 2
									   && parent.absolutePath() != rootPath;

				if (emptyParentIsNotRoot) {
					emptyRoot = parent;
				}
			} while(emptyParentIsNotRoot);

			/*
			 * All that will be inside emptyRoot will be deleted in case of
			 * rollback. So let's add emptyRoot to rollback entities in order to
			 * remove it recursively if necessary.
			 */
			rollbackEntities << QFileInfo(emptyRoot.absolutePath());
		}
	}

	return true;
}

bool BackupFolderManager::writeAlbumDescription(const Album & album,
												const QString description,
												QString & errMsg)
{
	Q_UNUSED(album)

	QString descFileName = albumEffFolder.absoluteFilePath(BackupUtils::ALBUM_DESCRIPTION_NAMEFILE);
	QSaveFile descFile(descFileName);
	bool isNewFile = !QFileInfo(descFileName).exists();

	const QSet<QString> before = BackupUtils::getTempFiles(albumEffFolder, BackupUtils::ALBUM_DESCRIPTION_NAMEFILE);

	if (!descFile.open(QIODevice::WriteOnly)) {
		errMsg = BackupError::CANNOT_OPEN_FILE.arg(descFileName, descFile.errorString());
		return false;
	}

	const QSet<QString> after = BackupUtils::getTempFiles(albumEffFolder, BackupUtils::ALBUM_DESCRIPTION_NAMEFILE);

	// Register temporary files to rollback
	const QSet<QString> diff = after-before;

	for (QSet<QString>::ConstIterator it = diff.constBegin();
		 it != diff.constEnd();
		 ++it)
	{
		tmpFiles << QFileInfo(albumEffFolder.absoluteFilePath(*it));
	}

	QTextStream stream(&descFile);
	stream << description.toUtf8();

	bool res = descFile.commit();

	if (res && addAlbumContentToRollback && isNewFile) {
		rollbackEntity << QFileInfo(descFile.fileName());
	}
	else if (!res) {
		errMsg = BackupError::CANNOT_WRITE_FILE.arg(descFileName, descFile.errorString());
		descFile.cancelWriting();
	}

	return res;
}

bool BackupFolderManager::backupPhoto(const Album & album,
									  const Photo & photo,
									  QString & errMsg)
{
	Q_UNUSED(album)
	QString photoShortname = photo.getShortname();
	QString photoName = photo.getFileName();
	QString newPhoto = albumEffFolder.absoluteFilePath(photoShortname);
	QFile photoFile(photoName);
	QFile newPhotoFile(newPhoto);
	bool isNewFile = !newPhotoFile.exists();

	if (!isNewFile) {
		if (!newPhotoFile.remove()) {
			errMsg = BackupError::CANNOT_REMOVE_FILE.arg(newPhoto, newPhotoFile.errorString());
			return false;
		}
	}

	bool res = photoFile.copy(newPhoto);

	if (res && addAlbumContentToRollback && isNewFile) {
		rollbackEntity << QFileInfo(newPhotoFile);
	}
	else if (!res) {
		errMsg = BackupError::CANNOT_COPY_FILE.arg(photoName, photoFile.errorString());
	}

	return res;
}

bool BackupFolderManager::afterAlbumBackup(const Album & album,
										   QString & errMsg)
{
	Q_UNUSED(album)
	Q_UNUSED(errMsg)
	albumEffFolder = effFolder;
	return true;
}

bool BackupFolderManager::afterBackup(QString & errMsg) {
	Q_UNUSED(errMsg)
	effFolder = backupFolder;
	return true;
}
