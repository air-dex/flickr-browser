#ifndef ARCHIVETYPE_HPP
#define ARCHIVETYPE_HPP

#include <QObject>
#include <QString>
#include <QJsonObject>
class KArchive;

class ArchiveType : public QObject
{
	Q_OBJECT

	public:
		ArchiveType();
		static void declareQML();

		enum ArchiveEnum {
			DIRECTORY,
			ZIP,
			TARBALL,
			TAR,
			SEPT_ZIP,
			INVALID_ARCHIVE
		};
		Q_ENUM(ArchiveEnum)

		Q_INVOKABLE static const QString toString(ArchiveEnum archiveType);
		Q_INVOKABLE static const QJsonObject toModel(ArchiveEnum archiveType);
		Q_INVOKABLE static const QString toArchiveFormat(ArchiveEnum archiveType);
		/// @brief Init with new. Think to delete in the code later.
		static KArchive * getKArchiveManager(ArchiveEnum archiveType, QString namefile);
};

/// @typedef ArchiveType::ArchiveEnum ArType;
/// @brief Convenience
typedef ArchiveType::ArchiveEnum ArType;

#endif // ARCHIVETYPE_HPP
