#include "photo.hpp"

#include <QQmlEngine>
#include <QImage>
#include <QFileInfo>
#include <QTextStream>
#include "flickrbrowser.hpp"

const QString Photo::METADATA_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

Photo::Photo() :
	QObject(),
	id(""),
	name(""),
	description(""),
	takenAt(),
	importedAt(),
	license(""),
	exif(),
	fileName(""),
	shortname(""),
	chameleonColor()
{
	connect(this, &Photo::fileNameChanged,
			this, &Photo::computeChameleonColorSlot);
}

Photo::Photo(const QString & photoID) : Photo() {
	buildFromID(photoID);
}

Photo::Photo(const Photo & otherPhoto) : Photo() {
	copy(otherPhoto);
}

Photo::Photo(const QJsonObject & photoObj) : Photo() {
	fillWithJSON(photoObj);
	computeChameleonColor();
}

Photo::~Photo() {
	disconnect(this, &Photo::fileNameChanged,
			   this, &Photo::computeChameleonColorSlot);
}

const Photo &Photo::operator=(const Photo & otherPhoto) {
	copy(otherPhoto);
	return *this;
}

const Photo &Photo::operator=(const QJsonObject & photoObj) {
	fillWithJSON(photoObj);
	return *this;
}

void Photo::buildFromID(const QString & photoID) {
	// Retrieve JSON metadata and populate the object with them.
	QString metadataFileName = Photo::getJSONmetadataFilename(photoID);
	QJsonDocument doc = FlickrBrowser::getJSONfromFile(metadataFileName);

	if (!doc.isObject()) {
		// TODO: improve error message with photo id
		qWarning("Cannot retrieve photo metadata");
		this->id = "";
		return;
	}

	fillWithJSON(doc.object());
	computeChameleonColor();
}

void Photo::fillWithJSON(QJsonObject photoObj) {
	this->id = photoObj["id"].toString("");
	this->name = photoObj["name"].toString("");
	this->setFilename();
	this->description = photoObj["description"].toString("");
	this->takenAt = QDateTime::fromString(
		photoObj["date_taken"].toString(""),
		Photo::METADATA_DATE_FORMAT
	);
	this->importedAt = QDateTime::fromString(
		photoObj["date_imported"].toString(""),
		Photo::METADATA_DATE_FORMAT
	);
	this->license = photoObj["license"].toString("");

	// Filling EXIF
	FlickrBrowser::fillExifWithJSON(this->exif, photoObj["exif"].toObject());
}

void Photo::copy(const Photo & otherPhoto) {
	this->id = otherPhoto.id;
	this->name = otherPhoto.name;
	this->description = otherPhoto.description;
	this->takenAt = otherPhoto.takenAt;
	this->importedAt = otherPhoto.importedAt;
	this->license = otherPhoto.license;
	this->exif = otherPhoto.exif;
	this->fileName = otherPhoto.fileName;
	this->shortname = otherPhoto.shortname;
	this->chameleonColor = otherPhoto.chameleonColor;
}

void Photo::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<Photo>(FlickrBrowser::CORE_URI.constData(),
						   FlickrBrowser::MAJOR_VERSION,
						   FlickrBrowser::MINOR_VERSION,
						   "Photo");
}

bool Photo::operator==(const Photo & otherPhoto) const {
	return this->id == otherPhoto.id;
}

bool Photo::isValid() const {
	return ALL_VALID_IDS.contains(this->id);
}

QMap<QString, Photo> Photo::ALL_PHOTOS = QMap<QString, Photo>();

Photo Photo::getPhoto(QString photoID) {
	if (ALL_PHOTOS.contains(photoID)) {
		return ALL_PHOTOS[photoID];
	}
	else {
		Photo photo(photoID);

		if (photo.isValid()) {
			ALL_PHOTOS[photoID] = photo;
		}

		return photo;
	}
}

QUrl Photo::getFileURL() const {
	return QUrl::fromLocalFile(this->fileName);
}

// Photo folders
const QDir Photo::PHOTO_FOLDER = setPhotoFolder();
const QDir Photo::METADATA_FOLDER = setMetadataFolder();

const QDir Photo::setPhotoFolder() {
	QDir photoDir = FlickrBrowser::FB_ROOT;
	photoDir.cd("Datas");
	photoDir.cd("Photos");

	return photoDir;
}

const QDir Photo::setMetadataFolder() {
	QDir metadataDir = FlickrBrowser::FB_ROOT;
	metadataDir.cd("Datas");
	metadataDir.cd("Metadata");

	return metadataDir;
}

// Photo filename
const QString Photo::FILENAME_FILTER_PATTERN = "*_%1_o.*";
const QString Photo::SUPER_FILENAME_FILTER_PATTERN = "%1_%2_o.*";

void Photo::setFilename() {
	// Retrieve filename
	QStringList filters;
	QString filter = FILENAME_FILTER_PATTERN.arg(this->id);
	filters << filter;
	QFileInfoList filenames = Photo::PHOTO_FOLDER.entryInfoList(filters,
																QDir::Files,
																QDir::Name);

	// There should be only one filename.
	if (filenames.empty()) {
		// TODO: improve error message, with photo id (and name too ?).
		qWarning("There is not any photo for this ID.");
		this->shortname = "";
		this->fileName = "";
	}
	else {
		if (filenames.size() > 1) {
			/*
			 * NOTE: it might unlikely happen because of wildcards.
			 * Regex cannot be used for filters. If name is not used here and
			 * if another photo unlikely contains "_<a valid photo ID>_o." in
			 * its name, this photo will be included in the result.
			 */
			// TODO: improve error message, with photo id and all filenames (and name too?).
			qWarning("Photo should be unique and it is not. So there might be an error with the corresponding photo");
		}

		this->shortname = filenames[0].fileName();
		this->fileName = filenames[0].absoluteFilePath();
	}
}

// Metadata filenames
const QString Photo::JSON_METADATA_FILENAME = "photo_%1.json";
const QString Photo::METADATA_FILENAME_WILDCARD = "photo_*.json";
const QRegularExpression Photo::METADATA_FILENAME_REGEX = Photo::buildMetadataRegex();

QString Photo::getJSONmetadataFilename(const QString & photoID) {
	return METADATA_FOLDER.filePath(JSON_METADATA_FILENAME.arg(photoID));
}

const QRegularExpression Photo::buildMetadataRegex() {
	QString regex = "";
	QTextStream stream(&regex);

	stream << "^"
		   << QRegularExpression::escape("photo_")
		   << "(?<photoid>\\d+)"
		   << QRegularExpression::escape(".json")
		   << "$";

	return QRegularExpression(regex);
}

// All valid IDs.
QStringList Photo::ALL_VALID_IDS;

const QStringList Photo::getAllValidIDs() {
	return ALL_VALID_IDS;
}

void Photo::buildAllValidIDsList() {
	QStringList filters;

	filters << METADATA_FILENAME_WILDCARD;
	QStringList metadataFiles = METADATA_FOLDER.entryList(filters,
														  QDir::Files,
														  QDir::Name);

	for (QStringList::Iterator it = metadataFiles.begin();
		 it != metadataFiles.end();
		 ++it)
	{
		QRegularExpressionMatch match = METADATA_FILENAME_REGEX.match(*it);

		if (match.hasMatch()) {
			ALL_VALID_IDS << match.captured("photoid");
		}
	}
}

// File extension
QString Photo::getFileExtension() const {
	return QFileInfo(fileName).suffix();
}

// Chameleon color handling
void Photo::computeChameleonColor() {
	const QImage photo(fileName);

	if (photo.isNull()) {
		// TODO: error message for null photos.
		chameleonColor = QColor();
	}
	else {
		const qulonglong photoDim = qulonglong(photo.width() * photo.height());

		if (photoDim == 0) {
			// TODO: error message for 0-pixel photos.
			chameleonColor = QColor();
		}
		else {
			qulonglong totalRed = 0, totalGreen = 0, totalBlue = 0;

			// TODO: it's quite long. Try to optimize?
			for (int x = 0 ; x < photo.width(); x++) {
				for (int y = 0 ; y < photo.height(); y++) {
					const QColor pixelColor = photo.pixelColor(x, y);

					totalRed += qulonglong(pixelColor.red());
					totalGreen += qulonglong(pixelColor.green());
					totalBlue += qulonglong(pixelColor.blue());
				}
			}

			chameleonColor.setRgb(int(totalRed / photoDim),
								  int(totalGreen / photoDim),
								  int(totalBlue / photoDim));
		}
	}
}

void Photo::computeChameleonColorSlot() {
	computeChameleonColor();
	emit chameleonColorChanged();
}

// Getters
QString Photo::getId() const {
	return id;
}

QString Photo::getName() const {
	return name;
}

QString Photo::getDescription() const {
	return description;
}

QDateTime Photo::getTakenAt() const {
	return takenAt;
}

QDateTime Photo::getImportedAt() const {
	return importedAt;
}

QString Photo::getLicense() const {
	return license;
}

QMap<QString, QString> Photo::getExif() const {
	return exif;
}

QJsonObject Photo::getJSONExif() const {
	return FlickrBrowser::jsonifyExifMap(this->exif);
}

QString Photo::getFileName() const {
	return fileName;
}

QString Photo::getShortname() const {
	return shortname;
}

QColor Photo::getChameleonColor() const {
	return chameleonColor;
}
