#include "copyingprogress.hpp"

#include <QMetaType>

CopyingProgress::CopyingProgress() : albumProgress(), photoProgress() {}

QJsonObject CopyingProgress::toObject() const
{
	QJsonObject res;
	res["albums"] = albumProgress.toObject();
	res["photos"] = photoProgress.toObject();
	return res;
}

void CopyingProgress::declareQt() {
	qRegisterMetaType<CopyingProgress>("CopyingProgress");
}

ProgressGauge & CopyingProgress::albumProgressRef() {
	return albumProgress;
}

ProgressGauge & CopyingProgress::photoProgressRef() {
	return photoProgress;
}

void CopyingProgress::reset() {
	albumProgress.reset();
	photoProgress.reset();
}
