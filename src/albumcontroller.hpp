#ifndef ALBUMCONTROLLER_HPP
#define ALBUMCONTROLLER_HPP

#include <QObject>
#include "album.hpp"

class AlbumController : public QObject
{
	Q_OBJECT

	public:
		AlbumController();
		static void declareQML();

		/// @brief Changing the underlying album with the new album ID.
		Q_INVOKABLE void setAlbum(QString albumID);

		// Getters and setters
		Album * getAlbum() const;

	protected:
		Q_PROPERTY(Album * album
				   READ getAlbum
				   NOTIFY albumChanged)

		/// @brief Underlying album
		Album album;

	signals:
		void albumChanged();
};

#endif // ALBUMCONTROLLER_HPP
