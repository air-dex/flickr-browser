#ifndef BACKUPUTILS_HPP
#define BACKUPUTILS_HPP

#include <QDir>
#include <QFile>
#include <QSet>
#include <QString>
#include <QUuid>
#include "backupstate.hpp"
class BackupManager;

/// @brief Utilities for backups.
namespace BackupUtils {
	/// @brief File name for album description.
	extern const QString ALBUM_DESCRIPTION_NAMEFILE;

	/// @brief Filters for counting entries in folders: subfolders
	/// except . and .., files, drives, hidden files and system files.
	extern const QDir::Filters FILTERZ;

	/********************
	 * Security methods *
	 ********************/

	/// @brief Security method for operations on folders. Checking if
	/// the folder dir is in the directory whose path is rootPath.
	bool isDirInRoot(const QDir & dir, const QDir root, bool strictly);

	/// @brief Security method for operations on files. Checking if
	/// the file file is in the directory whose path is rootPath.
	bool isFileInRoot(const QFile & file, const QDir root, bool strictly);

	/// @brief Backend method for "isXXXInRoot".
	bool isInRoot(const QString leaf, const QDir rootPath, bool strictly);

	// Other utils

	/// @brief get temporary files
	/// @param folder Folder to search. Have to pass it by copy, in order to
	/// use a distinct one. entryList() will give real results if changes.
	QSet<QString> getTempFiles(QDir folder, const QString fileName);

	/// @brief Reencoding strings in order to avoid encoding issues in archives.
	/// @bug Known issue for 7ZIP, which might have misencoded characters.
	QString formatEntryNames(const QString entryName);

	QString getBackupNameFromUUID(QUuid id);
	BackupManager * getBackupManager(QUuid id);
	// TODO: inline?
	QUuid getBackupID(const BackupManager * backupMgr);
	// TODO: inline?
	QUuid getBackupID(QObject * sender);
	SaveState getBackupState(const BackupManager * backupMgr);
	SaveState getBackupState(QUuid id);
}

#endif // BACKUPUTILS_HPP
