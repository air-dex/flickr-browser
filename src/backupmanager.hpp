#ifndef BACKUPMANAGER_HPP
#define BACKUPMANAGER_HPP

#include <QDir>
#include <QFileInfoList>
#include <QString>
#include "backupstate.hpp"
#include "flickrbrowserthread.hpp"
#include "archivetype.hpp"
#include "album.hpp"
#include "photo.hpp"
#include "backuplog.hpp"
#include "copyingprogress.hpp"

/// @class BackupManager
/// @brief Base class for backuping processes. Uses a template method design
/// pattern for this.
class BackupManager : public FlickrBrowserThread
{
	Q_OBJECT

	public:
		BackupManager();
		static BackupManager * getBackupManager(ArType archiveType);

		/// @brief Template method for backuping.
		void backup(QStringList albumIDs, QString backupLocation, QString backupName);

		QString getBackupName() const;
		QString getName() const;

		SaveState getBackupState() const;
		void setBackupState(const SaveState & value);
		BackupLogList getLogs() const;
		CopyingProgress getBackupProgress() const;
		ProgressGauge getRollbackProgress() const;

	protected slots:
		virtual void terminate() override;

	protected:
		QString name;

		SaveState backupState;

		BackupLogList logs;
		CopyingProgress backupProgress;
		ProgressGauge rollbackProgress;

		void addLog(LogLvl lvl, QString msg);

		/**********
		 * Thread *
		 **********/

		void run() override;
		bool oohYouDBetterStahp();

		/******************
		 * Backup entries *
		 ******************/

		QStringList albumIDs;
		QString backupLocation;
		QString backupName;

		/*****************************
		 * Error messages management *
		 *****************************/

		/// @brief Error message attribute
		QString errorMessage;

		/******************
		 * Backup process *
		 ******************/
		/// @brief Original backup folder, as specified.
		QDir backupFolder;

		/// @brief Effective backup folder.
		QDir effFolder;

		/// @brief Entities to rollback.
		QFileInfoList rollbackEntity;

		/// @brief Capturing temporary files there.
		QFileInfoList tmpFiles;

		/// @brief Template method for backuping.
		bool backupProcess(QStringList albumIDs, QString backupLocation, QString backupName);

		/// @brief Formerly endBackup();
		bool stahpProcess(QStringList & errorMessages, bool doRollback = true);

		// Template method's variant methods.

		/// @brief Creating the basis (root folder or the archive, for example).
		virtual bool initBackup(QString backupName, QString & errMsg) = 0;

		/// @brief Creating the folder for backuping its photos.
		virtual bool initAlbumBackup(const Album & album, QString & errMsg) = 0;

		/// @brief Writing album description in a file.
		virtual bool writeAlbumDescription(const Album & album, const QString description, QString & errMsg) = 0;

		/// @brief Backuping a photo.
		virtual bool backupPhoto(const Album & album, const Photo & photo, QString & errMsg) = 0;

		/// @brief Treatments after backuping photos.
		virtual bool afterAlbumBackup(const Album & album, QString & errMsg) = 0;

		/// @brief Treatments after backuping all album.
		///
		/// Backup folder should be at location at the end of this method.
		virtual bool afterBackup(QString & errMsg) = 0;

		/// @brief Rollback backup in case of failure.
		virtual bool rollbackBackup(QString & errMsg);

		/// @brief Release backup datas. Default method with base struct.
		virtual bool releaseBackupData(QString & errMsg);

	signals:
		void successfulBackup(QString backupName);
		void failedBackup(QString backupName, QString errMsg);
		void successfulStahp(QString backupName, QString errMsg);
		void failedStahp(QString backupName, QString errMsg);
		void backupStateChanged();
		void backupProgressing(const CopyingProgress progress);
		void rollbackProgressing(const ProgressGauge progress);
		void infoing(QString msg);
		void warning(QString msg);
		void alerting(QString msg);
		void newLog(const BackupLog log);

	protected slots:
		void info(QString msg);
		void warn(QString msg);
		void alert(QString msg);
		void finishBackup();
		void onSuccessfulBackup(QString backupName);
		void onFailedBackup(QString backupName, QString errMsg);
		void onSuccessfulStahp(QString backupName, QString errMsg);
		void onFailedStahp(QString backupName, QString errMsg);
};

#endif // BACKUPMANAGER_HPP
