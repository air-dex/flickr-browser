#ifndef BACKUPARCHIVEMANAGER_HPP
#define BACKUPARCHIVEMANAGER_HPP

#include "backupmanager.hpp"
#include "archivetype.hpp"
class KArchive;

/// @brief BackupManager for archives: ZIP, TAR, TARBALL and SEPT_ZIP.
class BackupArchiveManager : public BackupManager
{
	public:
		BackupArchiveManager(ArType archiveType);
		virtual ~BackupArchiveManager() override;

	protected:
		ArType archiveType;

		static const QString ROOT_ARCHIVE_FOLDER_NAME;
		static const QString TWO_FOLDERS_PATTERN;

		// Backup datas
		/// @brief The archive entity
		KArchive * archive;

		/// @brief Current entry for album.
		QString albumEntry;

		/***************************
		 * BackupManager interface *
		 ***************************/

		/// @brief Creating the archive and the KArchive manager (in other).
		virtual bool initBackup(QString backupName, QString & errMsg) override;

		/// @brief Creating a directory entry for the album. Might be useful
		/// in case of empty album.
		virtual bool initAlbumBackup(const Album & album, QString & errMsg) override;

		/// @brief Put the description in a file entry.
		virtual bool writeAlbumDescription(const Album & album, const QString description, QString & errMsg) override;

		/// @brief Creating a file entry for the photo.
		virtual bool backupPhoto(const Album & album, const Photo & photo, QString & errMsg) override;

		/// @brief Nothing special to do.
		virtual bool afterAlbumBackup(const Album & album, QString & errMsg) override;

		/// @brief Closing archive.
		virtual bool afterBackup(QString & errMsg) override;

		/// @brief Release backup datas. Default method with base struct.
		virtual bool releaseBackupData(QString & errMsg) override;
};

#endif // BACKUPARCHIVEMANAGER_HPP
