#ifndef ARCHIVECONTROLLER_HPP
#define ARCHIVECONTROLLER_HPP

#include <QObject>
#include <QJsonArray>
#include <QUuid>
#include "threadstore.hpp"
#include "backuplog.hpp"
#include "copyingprogress.hpp"
#include "progressgauge.hpp"
class BackupManager;

class ArchiveController : public QObject
{
	Q_OBJECT

	public:
		ArchiveController();
		static void declareQML();

		// int because of QML who gives an int instead of an ArType.
		Q_INVOKABLE void saveAlbum(QStringList albumIDs, QString location, QString archiveName, int archiveTypeInt);
		Q_INVOKABLE void stahpBackup(QUuid backupUUID);
		Q_INVOKABLE void deleteBackupManager(QUuid backupID);

		/*********************
		 * Querying a backup *
		 *********************/
		Q_INVOKABLE int getBackupState(QUuid backupID);	// int because of QML
		Q_INVOKABLE QJsonArray getBackupLogs(QUuid backupID);
		Q_INVOKABLE QJsonObject getCopyingProgress(QUuid backupID);
		Q_INVOKABLE QJsonObject getRollbackProgress(QUuid backupID);
		Q_INVOKABLE bool isBackupFinished(QUuid backupID);

	protected:
		/****************************************
		 * BackupManager signal/slot connection *
		 ****************************************/

		void connectBackupManager(BackupManager *& backupMgr);
		void disconnectBackupManager(BackupManager *& backupMgr);

	signals:
		// Backup signals
		void newBackupProcess(QUuid backupID);
		void backupOK(QString action, QString info, QUuid backupID);
		void backupKO(QString action, QString info, QUuid backupID);
		void newBackupState(QUuid backupID, int backupState);	// int because of QML
		// TODO: progress becomes a CopyingProgress, what it truely is.
		void backupProgressing(QUuid backupID, QJsonObject progress);
		// TODO: progress becomes a ProgressGauge, what it truely is.
		void rollbackProgressing(QUuid backupID, QJsonObject progress);
		// TODO: log becomes a BackupLog, what it truely is.
		void newLog(QUuid backupID, QJsonObject log);
		void cannotClearBackup(QString backupInfo, QString errMsg, QUuid backupID);
		void backupCleared(QUuid backupID);

	public slots:
		void backupStarted();
		void backupFinished();
		void backupEndedOK(QString backupName);
		void backupEndedKO(QString backupName, QString errMsg);
		void backupStahppedOK(QString backupName, QString errMsg);
		void backupStahppedKO(QString backupName, QString errMsg);
		void backupStateChanged();
		void backupProgress(const CopyingProgress progress);
		void rollbackProgress(const ProgressGauge progress);
		void neoLogging(const BackupLog log);
};

#endif // ARCHIVECONTROLLER_HPP
