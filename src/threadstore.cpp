#include "threadstore.hpp"

#include "flickrbrowserthread.hpp"

ThreadStore::ThreadStore() :
	QMap<QUuid, FlickrBrowserThread *>(),
	graveyard()
{}

ThreadStore::~ThreadStore() {
	// Finishing threads in the map. And delete them later too?
	for (QMap<QUuid, FlickrBrowserThread *>::Iterator it = begin();
		 it != end();
		 ++it)
	{
		if (it.value() == nullptr) {
			continue;
		}

		if (it.value()->isRunning()) {
			it.value()->stahp(true);
			it.value()->wait();
		}

		it.value()->deleteLater();
	}
}

ThreadStore & ThreadStore::getThreadStore() {
	static ThreadStore store;
	return store;
}

bool ThreadStore::storeThread(FlickrBrowserThread * thread) {
	bool threadNotNull = thread != nullptr;

	if (threadNotNull) {
		insert(thread->getId(), thread);
	}

	return threadNotNull;
}

FlickrBrowserThread * ThreadStore::getThread(QUuid id) const {
	return value(id, nullptr);
}

bool ThreadStore::isOver(QUuid threadID) const {
	return graveyard.contains(threadID);
}

void ThreadStore::removeThread(FlickrBrowserThread * thread) {
	if (thread == nullptr) {
		return;
	}

	QUuid threadID = thread->getId();

	remove(threadID);
	graveyard << threadID;
	thread->deleteLater();
}
