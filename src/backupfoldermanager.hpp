#ifndef BACKUPFOLDERMANAGER_HPP
#define BACKUPFOLDERMANAGER_HPP

#include "backupmanager.hpp"

/// @brief BackupManager for folder backups.
class BackupFolderManager : public BackupManager
{
	Q_OBJECT

	public:
		BackupFolderManager();

	protected:
		// Backup datas structure
		/// @brief Current folder, where album content is stored.
		QDir albumEffFolder;

		/// @brief Adding albums path to rollbacks?
		bool addAlbumsToRollback = false;

		/// @brief Adding album's content (photo + description) path
		/// to rollbacks?
		bool addAlbumContentToRollback = false;

		/***************************
		 * BackupManager interface *
		 ***************************/

		/// @brief Creating the root folder for backup and opening it.
		virtual bool initBackup(QString backupName, QString & errMsg) override;

		/// @brief Creating the root folder for backuping album and opening it.
		virtual bool initAlbumBackup(const Album & album, QString & errMsg) override;

		/// @brief Put the description in a file.
		virtual bool writeAlbumDescription(const Album & album, const QString description, QString & errMsg) override;

		/// @brief Copying the photo in the album folder.
		virtual bool backupPhoto(const Album & album, const Photo & photo, QString & errMsg) override;

		/// @brief Going back to the root backup folder.
		virtual bool afterAlbumBackup(const Album & album, QString & errMsg) override;

		/// @brief Going back to the root backup folder. Might be useful
		/// in case of rollback.
		virtual bool afterBackup(QString & errMsg) override;

	private:
		/// @fn bool writeFolder(const QDir & root,
		///                      const QString newFolder,
		///                      QDir & newRoot,
		///                      const bool strictly,
		///                      const bool addRollbackEntities,
		///                      QFileInfoList & rollbackEntities,
		///                      bool & addRollBackEntitiesFlag,
		///                      QString & errMsg) const;
		/// @brief Internals. It creates the @p newFolder folder inside the
		/// @p root folder. It opens @p root / @p newFolder in the @p newRoot
		/// folder, which is set by this method. It also checks if @p newRoot
		/// is a subfolder of @p root , preventing backups being outside of
		/// root location (backup root folder or album root folders).
		///
		/// This method is used to ensure that folders containing child backup
		/// components (albums for root, photos and description for albums) are
		/// really located inside the backup. Indeed, @p newFolder value may
		/// contain a relative path which might locate the child component out
		/// of the backup location. This method ensures that it will not be
		/// the case.
		///
		/// However it is not the only thing that this method actually does.
		/// It also handles rollback entities management. If @p newRoot is an
		/// empty folder, it prefers adding it to the rollback entities list
		/// instead of its child components. The list will be shorter and the
		/// backup process will not spend time adding child entities to the list
		/// later. For this, you have to set the @p addRollbackEntities
		/// parameter to true. The folder added to the rollback entities list
		/// might not be @p newRoot. It is its highest parent which is not
		/// (recursively) empty.
		/// @param root Root folder
		/// @param newFolder The folder to create and open.
		/// @param strictly Should newRoot strictly be inside root?
		/// @param newRoot The new root: @p root / @p newFolder.
		/// @param addRollbackEntities Should the method add the new empty root
		/// to the rollback entities list?
		/// @param rollbackEntities Reference to rollback entities list.
		/// @param addRollBackEntitiesFlag Should child components (albums for
		/// root backup, album description and photos for album) be added to
		/// the rollback entities list later? It is useless if the new empty
		/// root is already in the list.
		/// @param errMsg String to capture potential error messages.
		/// @return true if @p newRoot is successfully created and opened, false
		/// otherwise. There will be an error message in @p errMsg in case of
		/// failure.
		bool writeFolder(const QDir & root,
						 const QString newFolder,
						 QDir & newRoot,
						 const bool strictly,
						 const bool addRollbackEntities,
						 QFileInfoList & rollbackEntities,
						 bool & addRollBackEntitiesFlag,
						 QString & errMsg) const;
};

#endif // BACKUPFOLDERMANAGER_HPP
