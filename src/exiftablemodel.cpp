#include "exiftablemodel.hpp"

#include <QQmlEngine>
#include <QModelIndex>

#include "flickrbrowser.hpp"

ExifTableModel::ExifTableModel(QObject *parent) :
	QAbstractTableModel(parent),
	exifDatas()
{}

ExifTableModel::ExifTableModel(QMap<QString, QString> exifDatas) :
	QAbstractTableModel(),
	exifDatas(exifDatas)
{}

ExifTableModel::~ExifTableModel() {}

void ExifTableModel::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<ExifTableModel>(FlickrBrowser::CORE_URI,
									FlickrBrowser::MAJOR_VERSION,
									FlickrBrowser::MINOR_VERSION,
									"ExifTableModel");
}

QStringList ExifTableModel::getExifTags() const {
	return exifDatas.keys();
}

QStringList ExifTableModel::getExifValues() const {
	return exifDatas.values();
}

QStringList ExifTableModel::getExifRow(int row) const {
	QStringList textRow;

	if (row > 0 || row < exifDatas.count()) {
		QString key = exifDatas.keys()[row];
		textRow << key << exifDatas[key];
	}

	return textRow;
}

// Getters and setters
QJsonObject ExifTableModel::getExifDatas() const {
	return FlickrBrowser::jsonifyExifMap(exifDatas);
}

void ExifTableModel::setExifDatas(const QMap<QString, QString> & value) {
	beginResetModel();
	exifDatas = value;
	emit exifDatasChanged();
	endResetModel();
}

void ExifTableModel::setExifDatas(QJsonObject value) {
	beginResetModel();
	FlickrBrowser::fillExifWithJSON(exifDatas, value);
	emit exifDatasChanged();
	endResetModel();
}

// Implementing QAbstractTableModel
int ExifTableModel::rowCount(const QModelIndex & parent) const {
	Q_UNUSED(parent);
	return exifDatas.size();
}

int ExifTableModel::columnCount(const QModelIndex &parent) const {
	Q_UNUSED(parent);
	return 2;
}

QVariant ExifTableModel::data(const QModelIndex & index, int role) const {
	if (!index.isValid()) {
		return QVariant();
	}

	QStringList exifTags = exifDatas.keys();
	int row = index.row();

	// Avoiding OoB
	if (row < 0 || row >= exifTags.size()) {
		return QVariant();
	}

	QString key = exifTags[index.row()];
	QString data = "";

	switch (index.column()) {
		// First column: The tag
		case 0:
			data = key;
			break;

		// Second column: The value
		case 1:
			data = exifDatas[key];
			break;

		// Should not happen
		default:
			return QVariant();
	}

	return (role == Qt::DisplayRole) ?
				QVariant::fromValue<QString>(data)
			  : QVariant();
}

QVariant ExifTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
	if (role != Qt::DisplayRole || orientation == Qt::Vertical) {
		return QVariant();
	}

	QString header = "";
	switch (section) {
		case 0:
			header = tr("Balise");
			break;

		case 1:
			header = tr("Valeur");
			break;

		default:
			break;
	}

	return QVariant::fromValue<QString>(header);
}
