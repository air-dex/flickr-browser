#ifndef PHOTO_HPP
#define PHOTO_HPP

#include <QObject>
#include <QColor>
#include <QDateTime>
#include <QDir>
#include <QJsonObject>
#include <QMap>
#include <QRegularExpression>
#include <QUrl>

class Photo : public QObject
{
	Q_OBJECT

	public:
		Photo();
		Photo(const QString & photoID);
		Photo(const Photo & otherPhoto);
		Photo(const QJsonObject & photoObj);
		virtual ~Photo();
		const Photo & operator=(const Photo & otherPhoto);
		const Photo & operator=(const QJsonObject & photoObj);

		static void declareQML();

		bool operator==(const Photo & otherPhoto) const;
		bool isValid() const;

		static void buildAllValidIDsList();

		static Photo getPhoto(QString photoID);

		/// @brief File URL : file://<absolute path>
		Q_INVOKABLE QUrl getFileURL() const;

		Q_INVOKABLE QString getFileExtension() const;

		// Getters
		QString getId() const;
		QString getName() const;
		QString getDescription() const;
		QDateTime getTakenAt() const;
		QDateTime getImportedAt() const;
		QString getLicense() const;
		QMap<QString, QString> getExif() const;
		QJsonObject getJSONExif() const;
		QString getFileName() const;
		QString getShortname() const;
		QColor getChameleonColor() const;
		static const QStringList getAllValidIDs();

	protected:
		Q_PROPERTY(QString photo_id
				   READ getId
				   NOTIFY idChanged)
		QString id;

		Q_PROPERTY(QString name
				   READ getName
				   NOTIFY nameChanged)
		QString name;

		Q_PROPERTY(QString description
				   READ getDescription
				   NOTIFY descriptionChanged)
		QString description;

		Q_PROPERTY(QDateTime taken_at
				   READ getTakenAt
				   NOTIFY takenAtChanged)
		QDateTime takenAt;

		Q_PROPERTY(QDateTime imported_at
				   READ getImportedAt
				   NOTIFY importedAtChanged)
		QDateTime importedAt;

		Q_PROPERTY(QString license
				   READ getLicense
				   NOTIFY licenseChanged)
		QString license;

		Q_PROPERTY(QJsonObject exif
				   READ getJSONExif
				   NOTIFY exifChanged)
		QMap<QString, QString> exif;

		Q_PROPERTY(QString fileName
				   READ getFileName
				   NOTIFY fileNameChanged)
		QString fileName;

		Q_PROPERTY(QString shortname
				   READ getFileName
				   NOTIFY shortnameChanged)

		QString shortname;

		Q_PROPERTY(QColor chameleon_color
				   READ getChameleonColor
				   NOTIFY chameleonColorChanged)

		/// @brief Average color of all the pixels.
		/// Long to compute so it is set in an attribute.
		QColor chameleonColor;

		// Building photo internals
		void buildFromID(const QString & photoID);
		void fillWithJSON(QJsonObject photoObj);
		void copy(const Photo & otherPhoto);
		void computeChameleonColor();

		/// @brief Date format in metadata
		static const QString METADATA_DATE_FORMAT;

		// Photo folder
		static const QDir PHOTO_FOLDER;
		static const QDir setPhotoFolder();
		static const QDir METADATA_FOLDER;
		static const QDir setMetadataFolder();

		// Photo Filename
		void setFilename();

		/// @brief Pattern for filename filter
		static const QString FILENAME_FILTER_PATTERN;
		/// @brief With name too
		static const QString SUPER_FILENAME_FILTER_PATTERN;

		// Metadata filenames
		static const QString JSON_METADATA_FILENAME;
		static QString getJSONmetadataFilename(const QString & photoID = "");

		/// @brief ^photo_(?<photoid>\d+)\.json$
		static const QRegularExpression METADATA_FILENAME_REGEX;
		static const QString METADATA_FILENAME_WILDCARD;
		static const QRegularExpression buildMetadataRegex();

		// All valid IDs.
		static QStringList ALL_VALID_IDS;

		// All photos
		/// @brief Map with all photos, indexed with their IDs.
		static QMap<QString, Photo> ALL_PHOTOS;

	protected slots:
		void computeChameleonColorSlot();

	signals:
		// NOTIFY signals
		void idChanged();
		void nameChanged();
		void descriptionChanged();
		void takenAtChanged();
		void importedAtChanged();
		void licenseChanged();
		void exifChanged();
		void fileNameChanged();
		void shortnameChanged();
		void chameleonColorChanged();
};

#endif // PHOTO_HPP
