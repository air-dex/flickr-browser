#ifndef EXIFTABLEMODEL_HPP
#define EXIFTABLEMODEL_HPP

#include <QAbstractTableModel>
#include <QJsonObject>

class ExifTableModel : public QAbstractTableModel
{
	Q_OBJECT

	public:
		ExifTableModel(QObject *parent = nullptr);
		ExifTableModel(QMap<QString, QString> exifDatas);
		virtual ~ExifTableModel() override;
		static void declareQML();

		// Needed by QML for computing column sizes
		Q_INVOKABLE QStringList getExifTags() const;
		Q_INVOKABLE QStringList getExifValues() const;
		Q_INVOKABLE QStringList getExifRow(int row) const;

		// Getters and setters
		QJsonObject getExifDatas() const;
		void setExifDatas(const QMap<QString, QString> & value);
		void setExifDatas(QJsonObject value);

		// Implementing QAbstractTableModel
		int rowCount(const QModelIndex &parent = QModelIndex()) const override;
		int columnCount(const QModelIndex &parent = QModelIndex()) const override;
		QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
		// TODO: flags?


	protected:
		Q_PROPERTY(QJsonObject exifDatas
				   READ getExifDatas
				   WRITE setExifDatas
				   NOTIFY exifDatasChanged)

		/// @brief EXIF datas
		QMap<QString, QString> exifDatas;

	signals:
		void exifDatasChanged();
};

#endif // EXIFTABLEMODEL_HPP
