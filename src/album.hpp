#ifndef ALBUM_HPP
#define ALBUM_HPP

#include <QObject>
#include <QDateTime>
#include <QJsonArray>
#include <QJsonObject>
#include <QRegularExpression>

/// @brief A Flickr album.
class Album : public QObject
{
	Q_OBJECT

	public:
		Album();
		Album(QJsonObject albumDatas);
		Album(const Album & otherAlbum);
		const Album & operator=(const Album & otherAlbum);
		const Album & operator=(const QJsonObject & albumDatas);
		virtual ~Album();

		static void declareQML();
		bool operator==(const Album & otherAlbum) const;
		Q_INVOKABLE bool isValid() const;
		Q_INVOKABLE bool isRoll() const;

		// Building after because of static member initialization
		static void fillAllAlbumsMap();
		static void buildRoll();

		// Getters
		QString getId() const;
		uint getPhotoCount() const;
		QString getTitle() const;
		QString getDescription() const;
		QDateTime getCreatedAt() const;
		QDateTime getLastUpdatedAt() const;
		QString getCoverPhotoID() const;
		QJsonArray getPhotoIDsJSON() const;
		QStringList getPhotoIDs() const;
		static Album getAlbum(QString albumID);
		static const QString getRollId();
		static const Album & getRoll();
		/// @brief Getter on all Flickr albums (except roll).
		static const QMap<QString, Album> & getAllFlickrAlbums();

	protected:
		Q_PROPERTY(QString album_id
				   READ getId
				   NOTIFY idChanged)
		QString id;

		Q_PROPERTY(uint photo_count
				   READ getPhotoCount
				   NOTIFY photoCountChanged)
		uint photoCount;

		Q_PROPERTY(QString title
				   READ getTitle
				   NOTIFY titleChanged)
		QString title;

		Q_PROPERTY(QString description
				   READ getDescription
				   NOTIFY descriptionChanged)
		QString description;

		Q_PROPERTY(QDateTime created_at
				   READ getCreatedAt
				   NOTIFY createdAtChanged)
		QDateTime createdAt;

		Q_PROPERTY(QDateTime last_updated_at
				   READ getLastUpdatedAt
				   NOTIFY lastUpdatedAtChanged)
		QDateTime lastUpdatedAt;

		Q_PROPERTY(QString cover_photo_id
				   READ getCoverPhotoID
				   NOTIFY coverPhotoIDChanged)
		QString coverPhotoID;

		Q_PROPERTY(QJsonArray photo_ids
				   READ getPhotoIDsJSON
				   NOTIFY photoIdsChanged)
		QStringList photoIDs;

		// Init methods
		void copy(const Album & otherAlbum);
		void fillWithJSON(const QJsonObject & albumDatas);

		/// @brief ^https://www.flickr.com/photos//(?<photoid>\d+)$
		static const QRegularExpression COVER_PHOTO_REGEX;
		static QRegularExpression setCoverPhotoRegex();

		// Camera roll
		Q_PROPERTY(QString ROLL_ID READ getRollId CONSTANT)

		/// @brief Album roll, a fake album containing all photos.
		/// It is not supposed to be a valid and known Flickr Album ID.
		static const QString ROLL_ID;
		static Album ROLL;

		/// @brief Map with all albums indexed with their IDs.
		static QMap<QString, Album> FLICKR_ALBUMS;

	signals:
		// NOTIFY signals
		void idChanged();
		void photoCountChanged();
		void titleChanged();
		void descriptionChanged();
		void createdAtChanged();
		void lastUpdatedAtChanged();
		void coverPhotoIDChanged();
		void photoIdsChanged();
};

#endif // ALBUM_HPP
