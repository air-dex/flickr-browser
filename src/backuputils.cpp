#include "backuputils.hpp"

#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QTextCodec>

#include "threadstore.hpp"
#include "backupmanager.hpp"

const QString BackupUtils::ALBUM_DESCRIPTION_NAMEFILE = "description.txt";
const QDir::Filters BackupUtils::FILTERZ = (QDir::NoDotAndDotDot | QDir::AllEntries | QDir::Hidden | QDir::System);

/********************
 * Security methods *
 ********************/

bool BackupUtils::isDirInRoot(const QDir& dir, const QDir root, bool strictly) {
	return isInRoot(dir.absolutePath(), root, strictly);
}

bool BackupUtils::isFileInRoot(const QFile & file,
							   const QDir root,
							   bool strictly)
{
	return isInRoot(file.fileName(), root, strictly);
}

bool BackupUtils::isInRoot(const QString leaf, const QDir root, bool strictly) {
	QString pattern = "^%1";

	if (strictly) {
		// There has to be something else after.
		pattern += ".+$";	// pattern == /^%1.+$/
	}

	QRegularExpression regex(pattern.arg(QRegularExpression::escape(root.absolutePath())));
	QRegularExpressionMatch match = regex.match(leaf);
	return match.hasMatch();
}

// Other utils

QSet<QString> BackupUtils::getTempFiles(QDir folder, const QString fileName) {
	const QString TEMP_FILE_PATTERN = "%1.*";
	QStringList filters;
	filters << TEMP_FILE_PATTERN.arg(fileName);

	folder.setNameFilters(filters);
	folder.setFilter(QDir::Files);
	return folder.entryList().toSet();
}


// FIXME: known issue for 7ZIP, which might have misencoded characters.
QString BackupUtils::formatEntryNames(const QString entryName) {
	QTextCodec * codec = QTextCodec::codecForName("IBM 850");
	return QString::fromLocal8Bit(codec->fromUnicode(entryName));
}

QString BackupUtils::getBackupNameFromUUID(QUuid id) {
	BackupManager * thread = BackupUtils::getBackupManager(id);
	return (thread == nullptr) ? "" : thread->getName();
}

BackupManager * BackupUtils::getBackupManager(QUuid id) {
	static ThreadStore & store = ThreadStore::getThreadStore();
	return qobject_cast<BackupManager *>(store.getThread(id));
}

QUuid BackupUtils::getBackupID(const BackupManager * backupMgr) {
	return backupMgr ? backupMgr->getId() : FlickrBrowserThread::getNullId();
}

QUuid BackupUtils::getBackupID(QObject * sender) {
	return BackupUtils::getBackupID(qobject_cast<BackupManager *>(sender));
}

SaveState BackupUtils::getBackupState(const BackupManager * backupMgr) {
	return backupMgr ? backupMgr->getBackupState() : BackupState::INVALID_STATE;
}

SaveState BackupUtils::getBackupState(QUuid id) {
	return BackupUtils::getBackupState(BackupUtils::getBackupManager(id));
}
