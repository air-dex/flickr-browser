#include "albumcontroller.hpp"

#include <QQmlEngine>
#include "flickrbrowser.hpp"

AlbumController::AlbumController() : QObject(), album() {}

void AlbumController::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<AlbumController>(FlickrBrowser::CORE_URI.constData(),
									 FlickrBrowser::MAJOR_VERSION,
									 FlickrBrowser::MINOR_VERSION,
									 "AlbumController");
}

void AlbumController::setAlbum(QString albumID) {
	album = Album::getAlbum(albumID);
	emit albumChanged();
}

// Getters and setters
Album * AlbumController::getAlbum() const {
	return const_cast<Album *>(&album) ;
}
