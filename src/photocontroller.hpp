#ifndef PHOTOCONTROLLER_HPP
#define PHOTOCONTROLLER_HPP

#include <QObject>
#include "photo.hpp"

class PhotoController : public QObject
{
	Q_OBJECT

	public:
		PhotoController();
		static void declareQML();

		Photo * getPhoto() const;

		/// @brief Changing the underlying photo with the new photo ID.
		Q_INVOKABLE void setPhoto(QString photoID);

		// TODO: Saving photos with a QSaveFile
		Q_INVOKABLE bool savePhoto(QString photoPath);
		Q_INVOKABLE bool savePhoto(QUrl photoURL);

	protected:
		Q_PROPERTY(Photo * photo
				   READ getPhoto
				   NOTIFY photoChanged)

		/// @brief Underlying photo
		Photo photo;

	signals:
		void photoChanged();
};

#endif // PHOTOCONTROLLER_HPP
