#ifndef WALLPAPERCONTROLLER_HPP
#define WALLPAPERCONTROLLER_HPP

#include <QColor>
#include <QObject>
#include "wallpaperopts.hpp"
class WallpaperManager;

class WallpaperController : public QObject
{
	Q_OBJECT

	public:
		WallpaperController();
		virtual ~WallpaperController();
		static void declareQML();

		// int because of QML who gives an int instead of a WpOpts.
		Q_INVOKABLE bool setAsWallpaper(QString wallpaperPath, int wallpaperOpt, QColor newColor);
		Q_INVOKABLE void refreshWallpaperDatas();

	protected:
		WallpaperManager * wpm;

		Q_PROPERTY(QString wallpaper_path
				   READ getWallpaperPath
				   NOTIFY wallpaperChanged)

		QString getWallpaperPath() const;

		Q_PROPERTY(WallpaperOpts::WallpaperOptsEnum wallpaper_option
				   READ getWallpaperOption
				   NOTIFY wallpaperChanged)

		WpOpts getWallpaperOption() const;

		Q_PROPERTY(QColor background_color
				   READ getBackgroundColor
				   NOTIFY wallpaperChanged)

		QColor getBackgroundColor() const;

	signals:
		void wallpaperChanged();
};

#endif // WALLPAPERCONTROLLER_HPP
