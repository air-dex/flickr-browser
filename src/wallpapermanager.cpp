#include "wallpapermanager.hpp"

WallpaperManager::WallpaperManager() :
	valid(false),
	wpPath(""),
	wpOpts(WpOpts::INVALID_OPT),
	bgColor()
{}

WallpaperManager::~WallpaperManager() {}

bool WallpaperManager::isValid() const {
	return valid;
}

QString WallpaperManager::getWpPath() const {
	return wpPath;
}

WpOpts WallpaperManager::getWpOpts() const {
	return wpOpts;
}

QColor WallpaperManager::getBgColor() const {
	return bgColor;
}
