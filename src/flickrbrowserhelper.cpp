#include "flickrbrowserhelper.hpp"

#include <QQmlEngine>
#include <QStandardPaths>

#include "flickrbrowser.hpp"
#include "album.hpp"
#include "backuputils.hpp"

FlickrBrowserHelper::FlickrBrowserHelper() : QObject() {}

void FlickrBrowserHelper::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<FlickrBrowserHelper>(FlickrBrowser::CORE_URI.constData(),
										 FlickrBrowser::MAJOR_VERSION,
										 FlickrBrowser::MINOR_VERSION,
										 "FlickrBrowserHelper");
}

QJsonArray FlickrBrowserHelper::getAlbumSelectModel() {
	QJsonArray albums;
	const QMap<QString, Album> & allAlbums = Album::getAllFlickrAlbums();

	for (QMap<QString, Album>::ConstIterator it = allAlbums.begin();
		 it != allAlbums.end();
		 ++it)
	{
		QJsonObject album;
		album["id"] = it->getId();
		album["title"] = it->getTitle();
		album["selected"] = false;	// Field used for multiselect ComboBoxes.
		albums.append(album);
	}

	return albums;
}

QUrl FlickrBrowserHelper::getHomeFolderURL() {
	return QUrl::fromLocalFile(QDir::homePath());
}

QUrl FlickrBrowserHelper::getPicturesFolderURL() {
	return QUrl::fromLocalFile(QStandardPaths::standardLocations(QStandardPaths::PicturesLocation)[0]);
}

QString FlickrBrowserHelper::getBackupNameFromUUID(QUuid id) {
	return BackupUtils::getBackupNameFromUUID(id);
}
