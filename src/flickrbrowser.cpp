#include "flickrbrowser.hpp"

#include <QFile>
#include <QJsonParseError>
#include <QQuickStyle>

#include "album.hpp"
#include "flickrbrowserhelper.hpp"
#include "albumcontroller.hpp"
#include "photocontroller.hpp"
#include "photo.hpp"
#include "exiftablemodel.hpp"
#include "wallpaperopts.hpp"
#include "wallpapercontroller.hpp"
#include "archivetype.hpp"
#include "archivecontroller.hpp"
#include "backupstate.hpp"
#include "loglevael.hpp"
#include "progressgauge.hpp"
#include "copyingprogress.hpp"

#ifdef Q_OS_WIN
	#include "windows/windowswpm.hpp"
#endif

// Program root
const QDir FlickrBrowser::FB_ROOT = FlickrBrowser::setFBRoot();

QDir FlickrBrowser::setFBRoot() {
	QDir root = QDir::current();
	root.cdUp();
	QDir::setCurrent(root.absolutePath());

	return root;
}

// Pre- and post- treatments.
void FlickrBrowser::initFlickrBrowser() {
	FlickrBrowser::styleFlickrBrowser();

	// Albums and photos special entities.
	Photo::buildAllValidIDsList();
	Album::fillAllAlbumsMap();
	Album::buildRoll();

	ProgressGauge::declareQt();
	CopyingProgress::declareQt();

	// Registering classes to QML.
	FlickrBrowserHelper::declareQML();
	Album::declareQML();
	AlbumController::declareQML();
	PhotoController::declareQML();
	Photo::declareQML();
	ExifTableModel::declareQML();
	WallpaperOpts::declareQML();
	WallpaperController::declareQML();
	ArchiveType::declareQML();
	ArchiveController::declareQML();
	BackupState::declareQML();
	LogLevael::declareQML();
}

void FlickrBrowser::postFlickrBrowser() {
	// Nothing for the moment
}

void FlickrBrowser::styleFlickrBrowser() {
	QQuickStyle::setStyle("Default");
}

// For QML declaration
const QByteArray FlickrBrowser::CORE_URI = "FlickrBrowserCore";
const int FlickrBrowser::MAJOR_VERSION = 1;
const int FlickrBrowser::MINOR_VERSION = 0;

// Utils
QJsonDocument FlickrBrowser::getJSONfromFile(const QString & fileName) {
	QFile jsonFile(fileName);

	if (!jsonFile.exists()) {
		// TODO: improve error case here, with file name.
		qWarning("JSON file not found. Abort.");
		return QJsonDocument();
	}

	jsonFile.open(QIODevice::ReadOnly);
	QByteArray fileContent = jsonFile.readAll();
	jsonFile.close();

	// Parsing albums data
	QJsonParseError parseErr;
	QJsonDocument doc = QJsonDocument::fromJson(fileContent, &parseErr);

	if (parseErr.error != QJsonParseError::NoError) {
		// TODO: improve error case here. add error message
		qWarning("JSON datas cannot be parsed. Abort.");
		return QJsonDocument();
	}

	return doc;
}

void FlickrBrowser::fillExifWithJSON(QMap<QString, QString> & exifMap, const QJsonObject & jsonExif) {
	exifMap.clear();

	for (QJsonObject::ConstIterator it = jsonExif.begin();
		 it != jsonExif.end();
		 ++it)
	{
		exifMap.insert(it.key(), it->toString(""));
	}
}

QJsonObject FlickrBrowser::jsonifyExifMap(const QMap<QString, QString> & exifMap) {
	QJsonObject jsonEXIF;

	for (QMap<QString, QString>::ConstIterator it = exifMap.begin();
		 it != exifMap.end();
		 ++it)
	{
		jsonEXIF.insert(it.key(), it.value());
	}

	return jsonEXIF;
}

wchar_t * FlickrBrowser::stringToWChar(QString str) {
	wchar_t * res = new wchar_t[qulonglong(str.length() + 1)];

	if (res) {
		str.toWCharArray(res);
		res[str.length()] = 0;
	}

	return res;
}

WallpaperManager * FlickrBrowser::getWallpaperManager()
{
#ifdef Q_OS_WIN
	return new WindowsWPM;
#else
	qFatal("Wallpaper manager should not be NULL");
	return nullptr;
#endif
}
