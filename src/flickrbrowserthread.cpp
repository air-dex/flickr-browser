#include "flickrbrowserthread.hpp"

#include "threadstore.hpp"

FlickrBrowserThread::FlickrBrowserThread() :
	QThread(),
	id(QUuid::createUuid())
{
	setTerminationEnabled(true);
	ThreadStore::getThreadStore().storeThread(this);
}

// Thread management

void FlickrBrowserThread::terminate() {
	QThread::terminate();
}

void FlickrBrowserThread::stahp(bool brutal) {
	if (isFinished()) {
		return;
	}

	if (brutal) {
		FlickrBrowserThread::terminate();
	}
	else {
		terminate();
	}
}

// Getters

const QUuid FlickrBrowserThread::NULL_ID = QUuid();

QUuid FlickrBrowserThread::getId() const {
	return id;
}

const QUuid FlickrBrowserThread::getNullId() {
	return NULL_ID;
}
