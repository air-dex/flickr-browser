#include "loglevael.hpp"

#include <QQmlEngine>
#include "flickrbrowser.hpp"

LogLevael::LogLevael() : QObject() {}

void LogLevael::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<LogLevael>(FlickrBrowser::CORE_URI.constData(),
							   FlickrBrowser::MAJOR_VERSION,
							   FlickrBrowser::MINOR_VERSION,
							   "LogLevael");
}
