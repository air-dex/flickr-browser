#include "archivecontroller.hpp"

#include <QQmlEngine>
#include "flickrbrowser.hpp"
#include "archivetype.hpp"
#include "backupmanager.hpp"
#include "backuperror.hpp"
#include "backupstate.hpp"
#include "backuputils.hpp"
#include "threadstore.hpp"

ArchiveController::ArchiveController() : QObject() {}

void ArchiveController::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<ArchiveController>(FlickrBrowser::CORE_URI.constData(),
									   FlickrBrowser::MAJOR_VERSION,
									   FlickrBrowser::MINOR_VERSION,
									   "ArchiveController");
}

/****************
 * Save & stahp *
 ****************/

void ArchiveController::saveAlbum(QStringList albumIDs,
								  QString location,
								  QString archiveName,
								  int archiveTypeInt)
{
	ArType archiveType = ArType(archiveTypeInt);

	if (archiveType == ArType::INVALID_ARCHIVE) {
		emit backupKO(
			BackupError::BACKUP_KO.arg(archiveName),
			BackupError::CANNOT_INIT_BACKUP.arg(
				archiveName, BackupError::UNKONWN_ARCHIVE_TYPE
			),
			BackupManager::getNullId()
		);
		return;
	}

	BackupManager * backupMgr = BackupManager::getBackupManager(archiveType);

	if (backupMgr == nullptr) {
		emit backupKO(
			BackupError::BACKUP_KO.arg(archiveName),
			BackupError::CANNOT_INIT_BACKUP.arg(
				archiveName, BackupError::CANNOT_GET_BACKUP_MANAGER
			),
			BackupManager::getNullId()
		);
	}
	else {
		connectBackupManager(backupMgr);
		backupMgr->backup(albumIDs, location, archiveName);
	}
}

void ArchiveController::stahpBackup(QUuid backupUUID) {
	BackupManager * backupMgr = BackupUtils::getBackupManager(backupUUID);

	if (backupMgr == nullptr) {
		emit backupKO(
			BackupError::BACKUP_STAHPPED_KO.arg(""),
			BackupError::CANNOT_GET_BACKUP_MANAGER,
			BackupManager::getNullId()
		);
		return;
	}

	backupMgr->stahp();
	backupMgr->wait();
}

void ArchiveController::deleteBackupManager(QUuid backupID) {
	BackupManager * backupMgr = BackupUtils::getBackupManager(backupID);

	if (!backupMgr) {
		emit cannotClearBackup(BackupError::CANNOT_CLEAR_BACKUP.arg(""),
							   BackupError::NULL_BACKUP_MANAGER,
							   backupID);
		return;
	}

	if (backupMgr->isRunning()) {
		emit cannotClearBackup(BackupError::CANNOT_CLEAR_BACKUP.arg(backupMgr->getName()),
							   BackupError::RUNNING_BACKUP,
							   backupID);
		return;
	}

	ThreadStore::getThreadStore().removeThread(backupMgr);
	emit backupCleared(backupID);
}

/****************************************
 * BackupManager signal/slot connection *
 ****************************************/

void ArchiveController::connectBackupManager(BackupManager *& backupMgr) {
	if (backupMgr == nullptr) {
		return;
	}

	connect(backupMgr, &BackupManager::started,
			this, &ArchiveController::backupStarted);
	connect(backupMgr, &BackupManager::finished,
			this, &ArchiveController::backupFinished);
	connect(backupMgr, &BackupManager::successfulBackup,
			this, &ArchiveController::backupEndedOK);
	connect(backupMgr, &BackupManager::failedBackup,
			this, &ArchiveController::backupEndedKO);
	connect(backupMgr, &BackupManager::successfulStahp,
			this, &ArchiveController::backupStahppedOK);
	connect(backupMgr, &BackupManager::failedStahp,
			this, &ArchiveController::backupStahppedKO);
	connect(backupMgr, &BackupManager::backupStateChanged,
			this, &ArchiveController::backupStateChanged);
	connect(backupMgr, &BackupManager::backupProgressing,
			this, &ArchiveController::backupProgress);
	connect(backupMgr, &BackupManager::rollbackProgressing,
			this, &ArchiveController::rollbackProgress);
	connect(backupMgr, &BackupManager::newLog,
			this, &ArchiveController::neoLogging);
}

void ArchiveController::disconnectBackupManager(BackupManager *& backupMgr) {
	if (backupMgr == nullptr) {
		return;
	}

	disconnect(backupMgr, &BackupManager::started,
			   this, &ArchiveController::backupStarted);
	disconnect(backupMgr, &BackupManager::finished,
			   this, &ArchiveController::backupFinished);
	disconnect(backupMgr, &BackupManager::successfulBackup,
			   this, &ArchiveController::backupEndedOK);
	disconnect(backupMgr, &BackupManager::failedBackup,
			   this, &ArchiveController::backupEndedKO);
	disconnect(backupMgr, &BackupManager::successfulStahp,
			   this, &ArchiveController::backupStahppedOK);
	disconnect(backupMgr, &BackupManager::failedStahp,
			   this, &ArchiveController::backupStahppedKO);
	disconnect(backupMgr, &BackupManager::backupStateChanged,
			   this, &ArchiveController::backupStateChanged);
	disconnect(backupMgr, &BackupManager::backupProgressing,
			   this, &ArchiveController::backupProgress);
	disconnect(backupMgr, &BackupManager::rollbackProgressing,
			   this, &ArchiveController::rollbackProgress);
	disconnect(backupMgr, &BackupManager::newLog,
			   this, &ArchiveController::neoLogging);
}

/*********
 * Slots *
 *********/

void ArchiveController::backupStarted() {
	BackupManager * backupMgr = qobject_cast<BackupManager *>(sender());

	if (backupMgr == nullptr) {
		return;
	}
	emit newBackupProcess(backupMgr->getId());
}

void ArchiveController::backupFinished() {
	BackupManager * backupMgr = qobject_cast<BackupManager *>(sender());
	disconnectBackupManager(backupMgr);
}

void ArchiveController::backupEndedOK(QString backupName) {
	emit backupOK(
		BackupError::BACKUP_OK.arg(backupName),
		"",
		BackupUtils::getBackupID(sender())
	);
}

void ArchiveController::backupEndedKO(QString backupName, QString errMsg) {
	emit backupKO(
		BackupError::BACKUP_KO.arg(backupName),
		errMsg,
		BackupUtils::getBackupID(sender())
	);
}

void ArchiveController::backupStahppedOK(QString backupName, QString errMsg) {
	emit backupOK(
		BackupError::BACKUP_STAHPPED_OK.arg(backupName),
		errMsg.isEmpty() ? "" : BackupError::BACKUP_STAHPPED_OK_ERR.arg(errMsg),
		BackupUtils::getBackupID(sender())
	);
}

void ArchiveController::backupStahppedKO(QString backupName, QString errMsg) {
	emit backupKO(
		BackupError::BACKUP_STAHPPED_KO.arg(backupName),
		errMsg,
		BackupUtils::getBackupID(sender())
	);
}

void ArchiveController::backupStateChanged() {
	BackupManager * backupMgr = qobject_cast<BackupManager *>(sender());

	emit newBackupState(BackupUtils::getBackupID(backupMgr),
						BackupUtils::getBackupState(backupMgr));
}

void ArchiveController::backupProgress(const CopyingProgress progress)
{
	emit backupProgressing(BackupUtils::getBackupID(sender()), progress.toObject());
}

void ArchiveController::rollbackProgress(const ProgressGauge progress) {
	emit rollbackProgressing(BackupUtils::getBackupID(sender()), progress.toObject());
}

void ArchiveController::neoLogging(const BackupLog log) {
	emit newLog(BackupUtils::getBackupID(sender()), log.toObject());
}

/*********************
 * Querying a backup *
 *********************/
int ArchiveController::getBackupState(QUuid backupID) {
	return int(BackupUtils::getBackupState(backupID));
}

QJsonArray ArchiveController::getBackupLogs(QUuid backupID) {
	const BackupManager * backupMgr = BackupUtils::getBackupManager(backupID);

	if (!backupMgr) {
		// No backup manager, no logs.
		return QJsonArray();
	}

	const BackupLogList logs = backupMgr->getLogs();
	QJsonArray res;

	for (BackupLogList::ConstIterator it = logs.constBegin();
		 it != logs.constEnd();
		 ++it)
	{
		res << it->toObject();
	}

	return res;
}

QJsonObject ArchiveController::getCopyingProgress(QUuid backupID) {
	const BackupManager * backupMgr = BackupUtils::getBackupManager(backupID);
	const CopyingProgress progress = backupMgr ?
										 backupMgr->getBackupProgress()
									   : CopyingProgress();
	return progress.toObject();
}

QJsonObject ArchiveController::getRollbackProgress(QUuid backupID) {
	const BackupManager * backupMgr = BackupUtils::getBackupManager(backupID);
	const ProgressGauge progress = backupMgr ?
									   backupMgr->getRollbackProgress()
									 : ProgressGauge();
	return progress.toObject();
}

bool ArchiveController::isBackupFinished(QUuid backupID) {
	static ThreadStore & store = ThreadStore::getThreadStore();
	return BackupUtils::getBackupState(backupID) == SaveState::FINISH
			|| store.isOver(backupID);
}
