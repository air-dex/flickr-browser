#include "wallpaperopts.hpp"

#include <QQmlEngine>
#include "flickrbrowser.hpp"

WallpaperOpts::WallpaperOpts() : QObject() {}

void WallpaperOpts::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<WallpaperOpts>(FlickrBrowser::CORE_URI.constData(),
								   FlickrBrowser::MAJOR_VERSION,
								   FlickrBrowser::MINOR_VERSION,
								   "WallpaperOpts");
}

const QString WallpaperOpts::toStringOpt(WpOpts opt) {
	switch (opt) {
		case CENTER:
			return tr("Centrer");

		case TILE:
			return tr("Mosaïque");

		case STRETCH:
			return tr("Étirer");

		case KEEP_ASPECT:
			return tr("Préserver le ratio");

		case CROP_TO_FIT:
			return tr("Rogner");

		case SPAN:
			return tr("Étendre");

		default:
			return tr("Option invalide");
	}
}

const QJsonObject WallpaperOpts::toModel(WpOpts opt) {
	QJsonObject res;
	res["id"] = opt;
	res["text"] = WallpaperOpts::toStringOpt(opt);
	res["selected"] = false;	// For multiselect use
	return res;
}
