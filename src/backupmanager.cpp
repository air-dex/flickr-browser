#include "backupmanager.hpp"

#include <QtDebug>
#include "backuperror.hpp"
#include "backuputils.hpp"
#include "backupfoldermanager.hpp"
#include "backuparchivemanager.hpp"

BackupManager::BackupManager() :
	FlickrBrowserThread(),
	name(""),
	backupState(BackupState::NOTHING),
	logs(),
	backupProgress(),
	rollbackProgress(),
	albumIDs(),
	backupLocation(""),
	backupName(""),
	errorMessage(""),
	backupFolder(),
	effFolder(),
	rollbackEntity(),
	tmpFiles()
{
	connect(this, &BackupManager::infoing,
			this, &BackupManager::info);
	connect(this, &BackupManager::warning,
			this, &BackupManager::warn);
	connect(this, &BackupManager::alerting,
			this, &BackupManager::alert);
	connect(this, &BackupManager::finished,
			this, &BackupManager::finishBackup);
	connect(this, &BackupManager::successfulBackup,
			this, &BackupManager::onSuccessfulBackup);
	connect(this, &BackupManager::failedBackup,
			this, &BackupManager::onFailedBackup);
	connect(this, &BackupManager::successfulStahp,
			this, &BackupManager::onSuccessfulStahp);
	connect(this, &BackupManager::failedStahp,
			this, &BackupManager::onFailedStahp);
}

BackupManager * BackupManager::getBackupManager(ArType archiveType) {
	switch (archiveType) {
		case ArchiveType::DIRECTORY:
			return new BackupFolderManager;

		case ArchiveType::ZIP:
		case ArchiveType::TAR:
		case ArchiveType::TARBALL:
		case ArchiveType::SEPT_ZIP:
			return new BackupArchiveManager(archiveType);

		default:
			return nullptr;
	}
}

/**********
 * Thread *
 **********/

void BackupManager::backup(QStringList albumIDs,
						   QString backupLocation,
						   QString backupName)
{
	if (isRunning()) {
		errorMessage = BackupError::CANNOT_INIT_BACKUP.arg(
			backupName,
			BackupError::ALREADY_SAVING
		);
		emit failedBackup(backupName, errorMessage);
		return;
	}

	this->albumIDs = albumIDs;
	this->backupLocation = backupLocation;
	this->backupName = backupName;
	this->name = this->backupLocation + '/' + this->backupName;

	start();
}

void BackupManager::run() {
	QString backupName = this->backupName;
	emit backupProcess(albumIDs, backupLocation, backupName) ?
				successfulBackup(backupName)
			  : failedBackup(backupName, errorMessage);
}

void BackupManager::terminate() {
	QString backupName = this->backupName;
	emit oohYouDBetterStahp() ?
				successfulStahp(backupName, errorMessage)
			  : failedStahp(backupName, errorMessage);
	FlickrBrowserThread::terminate();
}

/**********
 * STAHP! *
 **********/

bool BackupManager::oohYouDBetterStahp() {
	setBackupState(BackupState::STAHP);
	emit infoing(BackupError::STAHP_BACKUP_INFO.arg(backupName));

	// Keeping previous error message.
	QStringList errorMessages;
	QStringList allErrorMessages;
	allErrorMessages << errorMessage;

	// Terminating the thread.
	if (isFinished()) {
		errorMessages << BackupError::NOT_RUNNING_BACKUP;
	}

	// Backup process end.
	bool res = stahpProcess(errorMessages);

	if (!errorMessages.isEmpty()) {
		allErrorMessages << BackupError::CANNOT_STAHP_BACKUP.arg(backupName, errorMessages.join('\n'));
	}

	errorMessage = allErrorMessages.join('\n');
	if (!errorMessage.isEmpty()) {
		emit warning(errorMessage);
	}

	emit infoing(BackupError::STAHP_BACKUP_INFO_END.arg(backupName));
	return res;
}

/***********
 * Getters *
 ***********/

QString BackupManager::getBackupName() const {
	return backupName;
}

/******************
 * Backup process *
 ******************/

bool BackupManager::backupProcess(QStringList albumIDs,
								  QString backupLocation,
								  QString backupName)
{
	QString errMsg = "";
	QStringList errorMessages;

	emit infoing(BackupError::BEGIN_SAVE_INFO.arg(backupName));
	setBackupState(BackupState::BEGIN);

	backupFolder.setPath(backupLocation);
	backupFolder.makeAbsolute();
	backupFolder.setFilter(BackupUtils::FILTERZ);

	// Location test
	if (!backupFolder.exists()) {
		emit alerting(BackupError::PREMATURE_END_SAVE_INFO.arg(backupName));
		setBackupState(BackupState::FAIL);
		errorMessages << BackupError::TWO_MSGS_PATTERN.arg(
			BackupError::CANNOT_CREATE_BACKUP_DATA.arg(
				backupName,
				BackupError::LOCATION_NOT_EXISTS.arg(backupLocation)
			),
			BackupError::ABORT_BACKUP_ERRMSG
		);
		stahpProcess(errorMessages, false);
		errorMessage = errorMessages.join('\n');
		emit alerting(errorMessage);
		return false;
	}

	// Creating basis for the backup. Failure reason is set by the method.
	if (!initBackup(backupName, errMsg)) {
		emit alerting(BackupError::PREMATURE_END_SAVE_INFO.arg(backupName));
		setBackupState(BackupState::FAIL);
		errorMessages << BackupError::TWO_MSGS_PATTERN.arg(
			BackupError::CANNOT_INIT_BACKUP.arg(backupName, errMsg),
			BackupError::ABORT_BACKUP_ERRMSG
		);
		stahpProcess(errorMessages);
		errorMessage = errorMessages.join('\n');
		emit alerting(errorMessage);
		return false;
	}

	// Backuping albums
	backupProgress.reset();
	backupProgress.albumProgressRef().setNb(albumIDs.count());
	setBackupState(BackupState::COPY);
	emit backupProgressing(backupProgress);

	for (QList<QString>::ConstIterator itAlbum = albumIDs.begin();
		 itAlbum != albumIDs.end();
		 ++itAlbum)
	{
		Album album = Album::getAlbum(*itAlbum);

		if (!album.isValid()) {
			emit warning(BackupError::CANNOT_INIT_ALBUM_BACKUP.arg(
				album.getTitle(),
				album.getId(),
				BackupError::INVALID_ALBUM.arg(*itAlbum)
			));
			backupProgress.albumProgressRef().decNb();
			emit backupProgressing(backupProgress);
			continue;
		}

		// Creating the album backup folder.
		if (!initAlbumBackup(album, errMsg)) {
			emit warning(BackupError::CANNOT_INIT_ALBUM_BACKUP.arg(
				album.getTitle(),
				album.getId(),
				errMsg
			));
			backupProgress.albumProgressRef().decNb();
			emit backupProgressing(backupProgress);
			continue;
		}

		emit infoing(BackupError::ALBUM_BACKUP_INFO.arg(album.getTitle(), backupName));

		// Progression counter
		backupProgress.albumProgressRef().incNo();
		backupProgress.photoProgressRef().resetNo();
		backupProgress.photoProgressRef().setNb(int(album.getPhotoCount()));

		// Writing album description in a text file.
		emit backupProgressing(backupProgress);

		QString description = "";
		QTextStream stream(&description);

		stream << album.getTitle() << "\n\n"
			   << album.getDescription() << "\n\n"
			   << tr("Contient %1 photo(s)").arg(album.getPhotoCount())
			   << "\n\n"
			   << tr("Créé le %1").arg(
					  album.getCreatedAt().toString(Qt::SystemLocaleLongDate)
					)
			   << "\n\n"
			   << tr("Modifié pour la dernière fois le %1").arg(
					  album.getLastUpdatedAt().toString(Qt::SystemLocaleLongDate)
					)
			   << '\n';

		if (!writeAlbumDescription(album, description, errMsg)) {
			emit warning(BackupError::CANNOT_WRITE_ALBUM_DESCRIPTION.arg(
				album.getTitle(),
				album.getId(),
				errMsg
			));
		}

		// Backuping photos.
		QStringList photoIDs = album.getPhotoIDs();

		for (QList<QString>::ConstIterator itPhoto = photoIDs.begin() ;
			 itPhoto != photoIDs.end();
			 ++itPhoto)
		{
			Photo photo = Photo::getPhoto(*itPhoto);

			if (!photo.isValid()) {
				emit warning(BackupError::CANNOT_BACKUP_PHOTO.arg(
					photo.getName(),
					photo.getId(),
					album.getTitle(),
					album.getId(),
					BackupError::INVALID_PHOTO.arg(*itPhoto)
				));
				backupProgress.photoProgressRef().decNb();
				emit backupProgressing(backupProgress);
				continue;
			}

			backupProgress.photoProgressRef().incNo();
			emit backupProgressing(backupProgress);

			if (!backupPhoto(album, photo, errMsg)) {
				emit warning(BackupError::CANNOT_BACKUP_PHOTO.arg(
					photo.getName(),
					photo.getId(),
					album.getTitle(),
					album.getId(),
					errMsg
				));
				backupProgress.photoProgressRef().decNo();
				backupProgress.photoProgressRef().decNb();
				emit backupProgressing(backupProgress);
			}
		}

		if(!afterAlbumBackup(album, errMsg)) {
			emit warning(BackupError::CANNOT_END_ALBUM_BACKUP.arg(
				album.getTitle(),
				album.getId(),
				errMsg
			));
		}
	}

	emit infoing(BackupError::CLOSING_SAVE_INFO.arg(backupName));
	setBackupState(BackupState::CLOSING);
	bool res = afterBackup(errMsg);

	if (!res) {
		setBackupState(BackupState::FAIL);
		errorMessages << BackupError::TWO_MSGS_PATTERN.arg(
			BackupError::CANNOT_END_BACKUP.arg(backupName, errMsg),
			BackupError::ABORT_BACKUP_ERRMSG
		);
	}

	stahpProcess(errorMessages, !res);
	errorMessage = errorMessages.join('\n');
	if(!errorMessage.isEmpty()) {
		emit warning(errorMessage);
	}

	emit infoing(BackupError::END_SAVE_INFO.arg(backupName));
	return res;
}

bool BackupManager::releaseBackupData(QString & errMsg) {
	Q_UNUSED(errMsg)
	return true;
}

bool BackupManager::rollbackBackup(QString & errMsg) {
	// Init rollback communication
	rollbackProgress.reset();
	rollbackProgress.setNb(rollbackEntity.count());
	emit infoing(BackupError::BEGIN_ROLLBACK_INFO.arg(backupName));
	setBackupState(BackupState::ROLLBACK);
	emit rollbackProgressing(rollbackProgress);

	// Add tempfiles if they still exists.
	for (QFileInfoList::ConstIterator tmpInfo = tmpFiles.begin();
		 tmpInfo != tmpFiles.end();
		 ++tmpInfo)
	{
		if (tmpInfo->exists()) {
			rollbackEntity << *tmpInfo;
			rollbackProgress.incNb();
			emit rollbackProgressing(rollbackProgress);
		}
	}

	// Let's delete!
	QStringList errMsgs;

	for (QFileInfoList::Iterator rmInfo = rollbackEntity.begin();
		 rmInfo != rollbackEntity.end();
		 ++rmInfo)
	{
		QString rollBackAbsolutePath = rmInfo->absoluteFilePath();

		if (rmInfo->isDir()) {
			QDir rmdir(rollBackAbsolutePath);

			if (!BackupUtils::isDirInRoot(rmdir, backupFolder, true)) {
				errMsgs << BackupError::TWO_MSGS_PATTERN.arg(
					BackupError::FOLDER_OUT_OF_LOCATION.arg(rollBackAbsolutePath),
					BackupError::ABORT_REMOVE
				);
				rollbackProgress.decNb();
				emit rollbackProgressing(rollbackProgress);
				continue;
			}

			rollbackProgress.incNo();
			emit rollbackProgressing(rollbackProgress);

			if (!rmdir.removeRecursively()) {
				errMsgs << BackupError::CANNOT_REMOVE_FOLDER.arg(rollBackAbsolutePath);
				rollbackProgress.decNo();
				rollbackProgress.decNb();
				emit rollbackProgressing(rollbackProgress);
			}
		}
		else if (rmInfo->isFile()) {
			QFile rmfile(rollBackAbsolutePath);

			if (!BackupUtils::isFileInRoot(rmfile, backupFolder, true)) {
				errMsgs << BackupError::TWO_MSGS_PATTERN.arg(
					BackupError::FILE_OUT_OF_LOCATION.arg(rollBackAbsolutePath),
					BackupError::ABORT_REMOVE
				);
				rollbackProgress.decNb();
				emit rollbackProgressing(rollbackProgress);
				continue;
			}

			rollbackProgress.incNo();
			emit rollbackProgressing(rollbackProgress);

			if (!rmfile.remove()) {
				errMsgs << BackupError::CANNOT_REMOVE_FILE.arg(
					rmfile.fileName(),
					rmfile.errorString()
				);
				rollbackProgress.decNo();
				rollbackProgress.decNb();
				emit rollbackProgressing(rollbackProgress);
			}
		}
		else {
			errMsgs << BackupError::UNEXPECTED_BACKUPED_ELEMENT.arg(rollBackAbsolutePath);
			rollbackProgress.decNb();
			emit rollbackProgressing(rollbackProgress);
		}
	}

	emit infoing(BackupError::END_ROLLBACK_INFO.arg(backupName));
	errMsg = errMsgs.join(' ');
	return errMsgs.isEmpty();
}

bool BackupManager::stahpProcess(QStringList & errorMessages, bool doRollback) {
	QString errMsg = "";
	bool res = true;

	if (doRollback && !rollbackBackup(errMsg)) {
		errorMessages << BackupError::CANNOT_ROLLBACK_BACKUP.arg(backupName, errMsg);
		res = false;
	}

	setBackupState(BackupState::CLOSING);
	if (!releaseBackupData(errMsg)) {
		errorMessages << BackupError::CANNOT_RELEASE_BACKUP_DATA.arg(backupName, errMsg);
		res = false;
	}

	backupLocation = "";
	backupName = "";
	albumIDs.clear();

	return res;
}

void BackupManager::finishBackup() {
	setBackupState(BackupState::FINISH);
}

void BackupManager::onSuccessfulBackup(QString backupName) {
	emit infoing(BackupError::BACKUP_OK.arg(backupName));
}

void BackupManager::onFailedBackup(QString backupName, QString errMsg) {
	emit alerting(BackupError::BACKUP_KO.arg(backupName));
	emit alerting(errMsg);
}

void BackupManager::onSuccessfulStahp(QString backupName, QString errMsg) {
	emit infoing(BackupError::BACKUP_STAHPPED_OK.arg(backupName));

	if (!errMsg.isEmpty()) {
		emit warning(BackupError::BACKUP_STAHPPED_OK_ERR.arg(errMsg));
	}
}

void BackupManager::onFailedStahp(QString backupName, QString errMsg) {
	emit alerting(BackupError::BACKUP_STAHPPED_KO.arg(backupName));
	emit alerting(errMsg);
}

/********
 * Logs *
 ********/

void BackupManager::info(QString msg) {
	qInfo() << msg;
	addLog(LogLevael::INFO, msg);
}

void BackupManager::warn(QString msg) {
	qWarning() << msg;
	addLog(LogLevael::WARN, msg);
}

void BackupManager::alert(QString msg) {
	qCritical() << msg;
	addLog(LogLevael::CRITICAL, msg);
}

void BackupManager::addLog(LogLvl lvl, QString msg) {
	const BackupLog log(lvl, msg);
	logs.append(log);
	emit newLog(log);
}

/*********************
 * Getters & setters *
 *********************/

SaveState BackupManager::getBackupState() const {
	return backupState;
}

void BackupManager::setBackupState(const SaveState & value) {
	backupState = value;
	emit backupStateChanged();
}

QString BackupManager::getName() const {
	return name;
}

BackupLogList BackupManager::getLogs() const {
	return logs;
}

ProgressGauge BackupManager::getRollbackProgress() const {
	return rollbackProgress;
}

CopyingProgress BackupManager::getBackupProgress() const {
	return backupProgress;
}
