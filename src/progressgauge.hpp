#ifndef PROGRESSGAUGE_HPP
#define PROGRESSGAUGE_HPP

#include <QJsonObject>

class ProgressGauge
{
	public:
		ProgressGauge();
		static void declareQt();
		QJsonObject toObject() const;

		int getNo() const;
		void incNo();
		void resetNo();
		void decNo();

		int getNb() const;
		void setNb(int value);
		void incNb();
		void decNb();

		void reset();

	protected:
		int no;
		int nb;
};

#endif // PROGRESSGAUGE_HPP
