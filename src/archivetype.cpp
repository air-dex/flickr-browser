#include "archivetype.hpp"

#include <QQmlEngine>
#include "flickrbrowser.hpp"
#include <KZip>
#include <K7Zip>
#include <KTar>

ArchiveType::ArchiveType() : QObject() {}

void ArchiveType::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<ArchiveType>(FlickrBrowser::CORE_URI.constData(),
								 FlickrBrowser::MAJOR_VERSION,
								 FlickrBrowser::MINOR_VERSION,
								 "ArchiveType");
}

KArchive * ArchiveType::getKArchiveManager(ArchiveEnum archiveType, QString namefile) {
	switch (archiveType) {
		case ZIP:
			return new KZip(namefile);

		case TARBALL:
		case TAR:
			return new KTar(namefile);

		case SEPT_ZIP:
			return new K7Zip(namefile);

		case DIRECTORY:
		default:
			return nullptr;
	}
}

const QString ArchiveType::toString(ArType archiveType) {
	switch (archiveType) {
		case DIRECTORY:
			return tr("Dossier");

		case ZIP:
			return tr("Zip (.zip)");

		case TARBALL:
			return tr("Tarball (.tar.gz)");

		case TAR:
			return tr("Tar (.tar)");

		case SEPT_ZIP:
			return tr("7-Zip (.7z)");

		default:
			return tr("Type d'archive inconnu.");
	}
}

const QString ArchiveType::toArchiveFormat(ArType archiveType) {
	switch (archiveType) {
		case ZIP:
			return "zip";

		case TARBALL:
			return "tar.gz";

		case TAR:
			return "tar";

		case SEPT_ZIP:
			return "7z";

		default:
			return "";
	}
}

const QJsonObject ArchiveType::toModel(ArType archiveType) {
	QJsonObject res;
	res["id"] = archiveType;
	res["text"] = toString(archiveType);
	res["selected"] = false;	// For multiselect use
	return res;
}
