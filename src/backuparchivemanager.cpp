#include "backuparchivemanager.hpp"

#include <QFile>
#include <QFileInfo>
#include <QSet>
#include <QtDebug>
#include <KArchive>
#include "backuperror.hpp"
#include "backuputils.hpp"

BackupArchiveManager::BackupArchiveManager(ArType archiveType) :
	BackupManager(),
	archiveType(archiveType),
	archive(nullptr),
	albumEntry("")
{}

BackupArchiveManager::~BackupArchiveManager() {
	if (archive != nullptr) {
		delete archive;
		archive = nullptr;
	}
}

const QString BackupArchiveManager::ROOT_ARCHIVE_FOLDER_NAME = "albums";
const QString BackupArchiveManager::TWO_FOLDERS_PATTERN = "%1/%2";

/***************************
 * BackupManager interface *
 ***************************/

bool BackupArchiveManager::releaseBackupData(QString & errMsg) {
	Q_UNUSED(errMsg)
	if (archive != nullptr) {
		delete archive;
		archive = nullptr;
	}
	return true;
}

bool BackupArchiveManager::initBackup(QString backupName, QString & errMsg) {
	QString archiveFilePath = QDir::cleanPath(backupFolder.absoluteFilePath(backupName));

	// Test if the archive will be inside the backup location.
	if (!BackupUtils::isFileInRoot(QFile(archiveFilePath), backupFolder, false)) {
		errMsg = BackupError::FILE_OUT_OF_LOCATION.arg(archiveFilePath);
		return false;
	}

	archive = ArchiveType::getKArchiveManager(archiveType, archiveFilePath);

	if (archive == nullptr) {
		errMsg = BackupError::CANNOT_CREATE_ARCHIVE_MANAGER.arg(archiveFilePath);
		return false;
	}

	QFileInfo realArchiveInfo(archive->fileName());
	effFolder = realArchiveInfo.dir();
	rollbackEntity << realArchiveInfo;

	// Capturing the temporary file: capturing existing ones.
	const QSet<QString> tmpBefore = BackupUtils::getTempFiles(effFolder, realArchiveInfo.fileName());

	// Opening archive (temp file to capture will be created too).
	if (!archive->open(QIODevice::WriteOnly)) {
		errMsg = BackupError::CANNOT_OPEN_ARCHIVE.arg(
			archive->fileName(),
			archive->errorString()
		);
		return false;
	}

	// Capturing the temporary file: capturing current ones: previous + the new.
	const QSet<QString> tmpAfter = BackupUtils::getTempFiles(effFolder, realArchiveInfo.fileName());
	const QSet<QString> diff = tmpAfter-tmpBefore;

	for (QSet<QString>::ConstIterator it = diff.constBegin();
		 it != diff.constEnd();
		 ++it)
	{
		tmpFiles << QFileInfo(effFolder.absoluteFilePath(*it));
	}

	// Writing new directory.
	bool res = archive->writeDir(ROOT_ARCHIVE_FOLDER_NAME);

	if (!res) {
		errMsg = BackupError::CANNOT_WRITE_ROOT_ARCHIVE_FOLDER.arg(
			ROOT_ARCHIVE_FOLDER_NAME,
			archive->fileName(),
			archive->errorString()
		);
	}

	return res;
}

bool BackupArchiveManager::initAlbumBackup(const Album & album,
										   QString & errMsg)
{
	if (archive == nullptr) {
		errMsg = BackupError::NULL_ARCHIVE_MANAGER;
		return false;
	}

	albumEntry = TWO_FOLDERS_PATTERN.arg(
		ROOT_ARCHIVE_FOLDER_NAME,
		album.getTitle()
	);

	bool res = archive->writeDir(BackupUtils::formatEntryNames(albumEntry));

	if (!res) {
		errMsg = BackupError::CANNOT_WRITE_ALBUM_ARCHIVE_FOLDER.arg(
			albumEntry,
			album.getTitle(),
			album.getId(),
			archive->fileName(),
			archive->errorString()
		);
	}

	return res;
}

bool BackupArchiveManager::writeAlbumDescription(const Album & album,
												 const QString description,
												 QString & errMsg)
{
	if (archive == nullptr) {
		errMsg = BackupError::NULL_ARCHIVE_MANAGER;
		return false;
	}

	QString descEntry = TWO_FOLDERS_PATTERN.arg(
		albumEntry,
		BackupUtils::ALBUM_DESCRIPTION_NAMEFILE
	);
	bool res = archive->writeFile(BackupUtils::formatEntryNames(descEntry),
										  description.toUtf8());

	if (!res) {
		errMsg = BackupError::CANNOT_WRITE_ALBUM_ARCHIVE_FILE.arg(
			descEntry,
			album.getTitle(),
			album.getId(),
			archive->fileName(),
			archive->errorString()
		);
	}

	return res;
}

bool BackupArchiveManager::backupPhoto(const Album & album,
									   const Photo & photo,
									   QString & errMsg)
{
	if (archive == nullptr) {
		errMsg = BackupError::NULL_ARCHIVE_MANAGER;
		return false;
	}

	QFile photoFile(photo.getFileName());

	photoFile.open(QIODevice::ReadOnly);
	if (!photoFile.isOpen()) {
		errMsg = BackupError::CANNOT_OPEN_FILE.arg(
			photo.getFileName(),
			photoFile.errorString()
		);
		return false;
	}

	QString photoEntry = TWO_FOLDERS_PATTERN.arg(
		albumEntry,
		photo.getShortname()
	);
	bool res = archive->writeFile(BackupUtils::formatEntryNames(photoEntry),
								  photoFile.readAll());
	photoFile.close();

	if (!res) {
		errMsg = BackupError::CANNOT_WRITE_ALBUM_ARCHIVE_FILE.arg(
			photoEntry,
			album.getTitle(),
			album.getId(),
			archive->fileName(),
			archive->errorString()
		);
	}

	return res;
}

bool BackupArchiveManager::afterAlbumBackup(const Album & album,
											QString & errMsg)
{
	Q_UNUSED(album)
	Q_UNUSED(errMsg)
	albumEntry = "";
	return true;
}

bool BackupArchiveManager::afterBackup(QString & errMsg) {
	if (archive == nullptr) {
		errMsg = BackupError::NULL_ARCHIVE_MANAGER;
		return false;
	}

	bool res = archive->close();

	if (!res) {
		errMsg = BackupError::CANNOT_CLOSE_ARCHIVE.arg(
			archive->fileName(),
			archive->errorString()
		);
	}

	return res;
}
