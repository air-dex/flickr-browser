#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "flickrbrowser.hpp"

int main(int argc, char *argv[])
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	QGuiApplication app(argc, argv);

	FlickrBrowser::initFlickrBrowser();
	// Current path is supposed to be at the program root now.

	// Init QML Engine
	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
	int res = engine.rootObjects().isEmpty() ? -1 : app.exec();

	FlickrBrowser::postFlickrBrowser();

	return res;
}
