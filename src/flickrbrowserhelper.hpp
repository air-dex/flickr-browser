#ifndef FLICKRBROWSERHELPER_HPP
#define FLICKRBROWSERHELPER_HPP

#include <QObject>
#include <QJsonArray>
#include <QUrl>
#include <QUuid>
#include <QString>

/// @brief Gate to the backoffice
class FlickrBrowserHelper : public QObject
{
	Q_OBJECT

	public:
		FlickrBrowserHelper();
		static void declareQML();

		/// @brief Model for album combo boxes.
		/// Each album is represented by the following JSON object:
		/// { id: "albumID", title: "albumm title" }
		Q_INVOKABLE QJsonArray getAlbumSelectModel();

		Q_INVOKABLE QUrl getPicturesFolderURL();
		Q_INVOKABLE QUrl getHomeFolderURL();

		Q_INVOKABLE QString getBackupNameFromUUID(QUuid id);
};

#endif // FLICKRBROWSERHELPER_HPP
