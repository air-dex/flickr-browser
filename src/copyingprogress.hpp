#ifndef COPYINGPROGRESS_HPP
#define COPYINGPROGRESS_HPP

#include "progressgauge.hpp"

class CopyingProgress
{
	public:
		CopyingProgress();
		QJsonObject toObject() const;
		static void declareQt();

		ProgressGauge & albumProgressRef();
		ProgressGauge & photoProgressRef();

		void reset();

	protected:
		ProgressGauge albumProgress;
		ProgressGauge photoProgress;
};

#endif // COPYINGPROGRESS_HPP
