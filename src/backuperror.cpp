#include "backuperror.hpp"

#include <QObject>

/******************
 * Error messages *
 ******************/

const QString BackupError::TWO_MSGS_PATTERN = "%1 %2";

// Tier 1
const QString BackupError::CANNOT_CREATE_BACKUP_DATA = QObject::tr("Impossible d'initialiser les données de sauvegarde pour la sauvegarde %1 : %2");
const QString BackupError::CANNOT_INIT_BACKUP = QObject::tr("Impossible d'initialiser la sauvegarde %1 : %2");
const QString BackupError::CANNOT_INIT_ALBUM_BACKUP = QObject::tr("Impossible d'initialiser la sauvegarde de l'album %1 (n°%2) : %3");
const QString BackupError::CANNOT_WRITE_ALBUM_DESCRIPTION = QObject::tr("Impossible d'écrire la description de l'album %1 (n°%2) : %3");
const QString BackupError::CANNOT_BACKUP_PHOTO = QObject::tr("Impossible de sauvegarder la photo %1 (n°%2) de l'album %3 (n°%4) : %5");
const QString BackupError::CANNOT_END_ALBUM_BACKUP = QObject::tr("Impossible de terminer la sauvegarde de l'album %1 (n°%2) : %3");
const QString BackupError::CANNOT_END_BACKUP = QObject::tr("Impossible de terminer la sauvegarde %1 : %2");
const QString BackupError::CANNOT_ROLLBACK_BACKUP = QObject::tr("Impossible d'effacer totalement la sauvegarde %1 :\n\n%2");
const QString BackupError::CANNOT_RELEASE_BACKUP_DATA = QObject::tr("Impossible de libérer les données de sauvegarde pour la sauvegarde %1 : %2");
const QString BackupError::CANNOT_STAHP_BACKUP = QObject::tr("Problème lors de l'interruption de la sauvegarde %1 :\n\n%2");

// Tier 2
const QString BackupError::CANNOT_GET_BACKUP_MANAGER = QObject::tr("Impossible d'obtenir de quoi sauvegarder.");
const QString BackupError::ABORT_BACKUP_ERRMSG = QObject::tr("Sauvegarde abandonnée.");
const QString BackupError::LOCATION_NOT_EXISTS = QObject::tr("L'emplacement %1 n'existe pas.");
const QString BackupError::INVALID_ALBUM = QObject::tr("L'album n°%1 n'est pas valide.");
const QString BackupError::INVALID_PHOTO = QObject::tr("La photo n°%1 n'est pas valide.");
const QString BackupError::FILE_OUT_OF_LOCATION = QObject::tr("Le fichier %1 est en dehors de la sauvegarde.");
const QString BackupError::FOLDER_OUT_OF_LOCATION = QObject::tr("Le dossier %1 est en dehors de la sauvegarde.");
const QString BackupError::CANNOT_OPEN_FILE = QObject::tr("Le fichier %1 ne peut être ouvert : %2");
const QString BackupError::CANNOT_REMOVE_FILE = QObject::tr("Le fichier %1 ne peut être effacé : %2");
const QString BackupError::CANNOT_REMOVE_FOLDER = QObject::tr("Le dossier %1 ne peut être effacé.");
const QString BackupError::ABORT_REMOVE = QObject::tr("Il ne sera pas effacé.");
const QString BackupError::UNEXPECTED_BACKUPED_ELEMENT = QObject::tr("Elément sauvegardé %1 inattendu.");
const QString BackupError::CANNOT_CREATE_FOLDER = QObject::tr("Le dossier %1 ne peut être créé dans %2.");
const QString BackupError::CANNOT_OPEN_FOLDER = QObject::tr("Le dossier %1 ne peut être ouvert.");
const QString BackupError::CANNOT_COPY_FILE = QObject::tr("Le fichier %1 ne peut être copié dans %2 : %3");
const QString BackupError::CANNOT_WRITE_FILE = QObject::tr("Le fichier %1 ne peut être modifié : %2");
const QString BackupError::NULL_ARCHIVE_MANAGER = QObject::tr("Gestionnaire d'archive null.");
const QString BackupError::CANNOT_CREATE_ARCHIVE_MANAGER = QObject::tr("Impossible d'instancier le gestionnaire d'archive pour la sauvegarde %1.");
const QString BackupError::CANNOT_OPEN_ARCHIVE = QObject::tr("Impossible d'ouvrir l'archive %1 : %2");
const QString BackupError::CANNOT_CLOSE_ARCHIVE = QObject::tr("Impossible de fermer l'archive %1 : %2");
const QString BackupError::CANNOT_WRITE_ROOT_ARCHIVE_FOLDER = QObject::tr("Impossible d'ajouter le dossier racine %1 à l'archive %2 : %3");
const QString BackupError::CANNOT_WRITE_ALBUM_ARCHIVE_FOLDER = QObject::tr("Impossible d'ajouter le dossier %1, lié à l'album %2 (n°%3), à l'archive %4 : %5");
const QString BackupError::CANNOT_WRITE_ALBUM_ARCHIVE_FILE = QObject::tr("Impossible d'ajouter le fichier %1, lié à l'album %2 (n°%3), à l'archive %4 : %5");
const QString BackupError::NOT_RUNNING_BACKUP = QObject::tr("Il n'y a pas de sauvegarde en cours.");
const QString BackupError::RUNNING_BACKUP = QObject::tr("La sauvegarde est toujours en cours.");
const QString BackupError::UNKONWN_ARCHIVE_TYPE = QObject::tr("Type d'archive inconnu");
const QString BackupError::ALREADY_SAVING = QObject::tr("Sauvegarde déjà en cours.");
const QString BackupError::NULL_BACKUP_MANAGER = QObject::tr("Gestionnaire de sauvarde null.");
const QString BackupError::CANNOT_CLEAR_BACKUP = QObject::tr("Impossible de retirer la sauvegarde %1 de la liste.");

// Info Tier 1
const QString BackupError::BACKUP_OK = QObject::tr("La sauvegarde %1 a été effectuée avec succès.");
const QString BackupError::BACKUP_KO = QObject::tr("La sauvegarde %1 n'a pu être effectuée.");
const QString BackupError::BACKUP_STAHPPED_OK = QObject::tr("Sauvegarde %1 interrompue avec succès.");
const QString BackupError::BACKUP_STAHPPED_KO = QObject::tr("La sauvegarde %1 n'a pu être interrompue correctement.");
const QString BackupError::BACKUP_STAHPPED_OK_ERR = QObject::tr("Les erreurs suivantes se sont toutefois produises : %1");

// Info Tier 2
const QString BackupError::BEGIN_SAVE_INFO = QObject::tr("Début de la sauvegarde %1.");
const QString BackupError::PREMATURE_END_SAVE_INFO = QObject::tr("Fin prématurée de la sauvegarde %1.");
const QString BackupError::CLOSING_SAVE_INFO = QObject::tr("Clôture de la sauvegarde %1.");
const QString BackupError::END_SAVE_INFO = QObject::tr("Fin de la sauvegarde %1.");
const QString BackupError::BEGIN_ROLLBACK_INFO = QObject::tr("Effacement de la sauvegarde %1.");
const QString BackupError::END_ROLLBACK_INFO = QObject::tr("Fin de l'effacement de la sauvegarde %1.");
const QString BackupError::ALBUM_BACKUP_INFO = QObject::tr("Copie de l'album %1 dans la sauvegarde %2.");
const QString BackupError::STAHP_BACKUP_INFO = QObject::tr("Interruption forcée de la sauvegarde %1.");
const QString BackupError::STAHP_BACKUP_INFO_END = QObject::tr("Fin de l'interruption forcée de la sauvegarde %1.");
