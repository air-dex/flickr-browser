#ifndef BACKUPLOG_HPP
#define BACKUPLOG_HPP

#include "loglevael.hpp"
#include <QString>
#include <QList>
#include <QJsonObject>

class BackupLog {
	public:
		BackupLog(LogLvl lvl = LogLevael::INVALID_LEVAEL, QString msg = "");
		QJsonObject toObject() const;

	protected:
		LogLvl levael;
		QString logText;
};

typedef QList<BackupLog> BackupLogList;

#endif // BACKUPLOG_HPP
