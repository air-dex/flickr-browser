#ifndef BACKUPSTATE_HPP
#define BACKUPSTATE_HPP

#include <QObject>

class BackupState : public QObject
{
	Q_OBJECT

	public:
		BackupState();
		static void declareQML();

		enum BState {
			NOTHING,
			BEGIN,
			COPY,
			CLOSING,
			FINISH,
			ROLLBACK,
			FAIL,
			STAHP,
			INVALID_STATE
		};
		Q_ENUM(BState)

		Q_INVOKABLE static QString stringState(BState bstate);
};

typedef BackupState::BState SaveState;

#endif // BACKUPSTATE_HPP
