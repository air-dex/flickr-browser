#ifndef FLICKRBROWSER_HPP
#define FLICKRBROWSER_HPP

#include <QDir>
#include <QJsonDocument>
#include "wallpapermanager.hpp"

/// @brief Namespace for global.
namespace FlickrBrowser {
	// Program root
	/// @brief Program root (cf. readme).
	extern const QDir FB_ROOT;

	// TODO: sets all folders in the main()?
	/// @brief Both init FB_ROOT and sets the current path to it.
	QDir setFBRoot();

	// Pre- and post- treatments.
	/// @brief Pre-treatments
	void initFlickrBrowser();

	/// @brief Post-treatments
	void postFlickrBrowser();

	void styleFlickrBrowser();

	// For QML declaration
	extern const QByteArray CORE_URI;
	extern const int MAJOR_VERSION;
	extern const int MINOR_VERSION;

	// Utils
	QJsonDocument getJSONfromFile(const QString & fileName);
	void fillExifWithJSON(QMap<QString, QString> & exifMap, const QJsonObject & jsonExif);
	QJsonObject jsonifyExifMap(const QMap<QString, QString> & exifMap);
	/// @brief pointer with new array here. Free space with delete[]
	wchar_t * stringToWChar(QString str);

	WallpaperManager * getWallpaperManager();
}

#endif // FLICKRBROWSER_HPP
