#ifndef WALLPAPEROPTS_HPP
#define WALLPAPEROPTS_HPP

#include <QObject>
#include <QJsonObject>

class WallpaperOpts : public QObject
{
	Q_OBJECT

	public:
		WallpaperOpts();
		static void declareQML();

		/// @see https://docs.microsoft.com/fr-fr/windows/desktop/api/shlobj_core/ns-shlobj_core-_tagwallpaperopt
		enum WallpaperOptsEnum {
			/// @brief Corresponds to WPSTYLE_CENTER
			CENTER = 0,

			/// @brief Corresponds to WPSTYLE_TILE
			TILE,

			/// @brief Corresponds to WPSTYLE_STRETCH
			STRETCH,

			/// @brief Corresponds to WPSTYLE_KEEPASPECT
			KEEP_ASPECT,

			/// @brief Corresponds to WPSTYLE_CROPTOFIT
			CROP_TO_FIT,

			/// @brief Corresponds to WPSTYLE_SPAN
			SPAN,

			INVALID_OPT
		};
		Q_ENUM(WallpaperOptsEnum)

		Q_INVOKABLE static const QString toStringOpt(WallpaperOptsEnum opt);
		Q_INVOKABLE static const QJsonObject toModel(WallpaperOptsEnum opt);
};

/// @typedef WallpaperOpts::WallpaperOptsEnum WpOpts
/// @brief Shortening convenience
typedef WallpaperOpts::WallpaperOptsEnum WpOpts;

#endif // WALLPAPEROPTS_HPP
