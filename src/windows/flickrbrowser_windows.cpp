#include "flickrbrowser_windows.hpp"

#include "windows_includes.hpp"

const unsigned int FlickrBrowser::WINDOWS_FILEPATH_MAX_LENGTH = 32768;

IActiveDesktop * FlickrBrowser::getActiveDesktop(HRESULT & hr) {
	IActiveDesktop * pActiveDesktop;

	hr = CoCreateInstance(CLSID_ActiveDesktop, nullptr, CLSCTX_INPROC_SERVER,
						  IID_IActiveDesktop, reinterpret_cast<void**>(&pActiveDesktop));

	return pActiveDesktop;
}

DWORD FlickrBrowser::wallpaperOptToDword(WpOpts wopt) {
	switch (wopt) {
		case WpOpts::CENTER:
			return WPSTYLE_CENTER;

		case WpOpts::TILE:
			return WPSTYLE_TILE;

		case WpOpts::STRETCH:
			return WPSTYLE_STRETCH;

		case WpOpts::KEEP_ASPECT:
			return WPSTYLE_KEEPASPECT;

		case WpOpts::CROP_TO_FIT:
			return WPSTYLE_CROPTOFIT;

		case WpOpts::SPAN:
			return WPSTYLE_SPAN;

		default:
			return WPSTYLE_MAX + 1;
	}
}

WpOpts FlickrBrowser::dwordToWallpaperOpt(DWORD dwopt) {
	switch (dwopt) {
		case WPSTYLE_CENTER:
			return WpOpts::CENTER;

		case WPSTYLE_TILE:
			return WpOpts::TILE;

		case WPSTYLE_STRETCH:
			return WpOpts::STRETCH;

		case WPSTYLE_KEEPASPECT:
			return WpOpts::KEEP_ASPECT;

		case WPSTYLE_CROPTOFIT:
			return WpOpts::CROP_TO_FIT;

		case WPSTYLE_SPAN:
			return WpOpts::SPAN;

		default:
			return WpOpts::INVALID_OPT;
	}
}

WALLPAPEROPT FlickrBrowser::setWallpaperOpt(WpOpts wopt) {
	WALLPAPEROPT res;

	res.dwSize = sizeof(WALLPAPEROPT);
	res.dwStyle = FlickrBrowser::wallpaperOptToDword(wopt);

	return res;
}
