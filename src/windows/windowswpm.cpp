#include "windowswpm.hpp"

#include <QString>
#include "windows_includes.hpp"
#include "../flickrbrowser.hpp"
#include "flickrbrowser_windows.hpp"

WindowsWPM::WindowsWPM() : WallpaperManager() {}

WindowsWPM::~WindowsWPM() {}

bool WindowsWPM::setWallpaper(QString newPath, WpOpts newWpOpt, QColor newBgColor) {
	CoInitialize(nullptr);
	HRESULT hr;
	IActiveDesktop * pActiveDesktop = FlickrBrowser::getActiveDesktop(hr);

	if (FAILED(hr)) {
		// TODO: retrieve error message and write it.
		pActiveDesktop->Release();
		CoUninitialize();
		return false;
	}

	// Setting wallpaper
	PCWSTR wPath = FlickrBrowser::stringToWChar(newPath);
	hr = pActiveDesktop->SetWallpaper(wPath, 0);
	delete[] wPath;
	wPath = nullptr;

	if (FAILED(hr)) {
		// TODO: retrieve error message and write it.
		pActiveDesktop->Release();
		CoUninitialize();
		return false;
	}

	// Set wallpaper options
	WALLPAPEROPT wopts = FlickrBrowser::setWallpaperOpt(newWpOpt);
	hr = pActiveDesktop->SetWallpaperOptions(&wopts, 0);

	if (FAILED(hr)) {
		// TODO: retrieve error message and write it.
		pActiveDesktop->Release();
		CoUninitialize();
		return false;
	}

	// Set new background color
	if (newBgColor.isValid()) {
		COLORREF newColor = RGB(newBgColor.red(), newBgColor.green(), newBgColor.blue());
		INT colorIndex[1];
		colorIndex[0] = COLOR_DESKTOP;
		BOOL colorRes = SetSysColors(1, colorIndex, &newColor);

		if (colorRes == FALSE) {
			// TODO: error message while failing to set it.
			pActiveDesktop->Release();
			CoUninitialize();
			return false;
		}
	}

	// Commit changes
	hr = pActiveDesktop->ApplyChanges(AD_APPLY_ALL);
	pActiveDesktop->Release();
	CoUninitialize();

	if (FAILED(hr)) {
		// TODO: retrieve error message and write it.
		return false;
	}

	// All is OK! \o/
	return true;
}

bool WindowsWPM::retrieveWallpaperDatas() {
	CoInitialize(nullptr);
	HRESULT hr;
	IActiveDesktop * pActiveDesktop = FlickrBrowser::getActiveDesktop(hr);

	if (FAILED(hr)) {
		// TODO: retrieve error message and write it.
		pActiveDesktop->Release();
		CoUninitialize();
		return false;
	}

	// Retrieve wallpaper path.
	PWSTR wpPath = new wchar_t[qulonglong(FlickrBrowser::WINDOWS_FILEPATH_MAX_LENGTH)];
	hr = pActiveDesktop->GetWallpaper(wpPath, FlickrBrowser::WINDOWS_FILEPATH_MAX_LENGTH, AD_GETWP_IMAGE);

	if (SUCCEEDED(hr)) {
		this->wpPath = QString::fromWCharArray(wpPath, int(FlickrBrowser::WINDOWS_FILEPATH_MAX_LENGTH));
		this->wpPath.truncate(this->wpPath.indexOf(QChar::Null));
		delete[] wpPath;
		wpPath = nullptr;
	} else {
		// TODO: retrieve error message and write it.
		delete[] wpPath;
		wpPath = nullptr;
		pActiveDesktop->Release();
		CoUninitialize();
		return false;
	}

	// Retrieve and set datas.
	WALLPAPEROPT wopts;
	wopts.dwSize = sizeof(WALLPAPEROPT);
	hr = pActiveDesktop->GetWallpaperOptions(&wopts, 0);

	if (SUCCEEDED(hr)) {
		this->wpOpts = FlickrBrowser::dwordToWallpaperOpt(wopts.dwStyle);
	} else {
		// TODO: retrieve error message and write it.
		pActiveDesktop->Release();
		CoUninitialize();
		return false;
	}

	// Retrieve background color
	COLORREF bgColor = GetSysColor(COLOR_DESKTOP);
	this->bgColor.setRgb(GetRValue(bgColor), GetGValue(bgColor), GetBValue(bgColor));

	pActiveDesktop->Release();
	CoUninitialize();
	return true;
}
