#ifndef WINDOWSWPM_HPP
#define WINDOWSWPM_HPP

#include "../wallpapermanager.hpp"

/// @brief WallpaperManager implementation for Microsoft Windows.
class WindowsWPM : public WallpaperManager
{
	public:
		WindowsWPM();
		virtual ~WindowsWPM();

		// WallpaperManager interface
		virtual bool setWallpaper(QString newPath, WpOpts newWpOpt, QColor newBgColor);
		virtual bool retrieveWallpaperDatas();
};

#endif // WINDOWSWPM_HPP
