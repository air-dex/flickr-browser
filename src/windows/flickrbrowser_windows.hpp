#ifndef FLICKRBROWSER_WINDOWS_HPP
#define FLICKRBROWSER_WINDOWS_HPP

#include "windows_includes.hpp"
#include "../wallpaperopts.hpp"

/// @brief Here spécific to Windows
namespace FlickrBrowser {
	extern const unsigned int WINDOWS_FILEPATH_MAX_LENGTH;

	IActiveDesktop * getActiveDesktop(HRESULT & hr);
	DWORD wallpaperOptToDword(WpOpts wopt);
	WALLPAPEROPT setWallpaperOpt(WpOpts wopt);
	WpOpts dwordToWallpaperOpt(DWORD dwopt);
}

#endif // FLICKRBROWSER_WINDOWS_HPP
