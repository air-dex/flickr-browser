#ifndef LOGLEVAEL_HPP
#define LOGLEVAEL_HPP

#include <QObject>

class LogLevael : public QObject
{
	Q_OBJECT

	public:
		LogLevael();
		static void declareQML();

		enum Levael {
			INVALID_LEVAEL,
			DEBUG,
			INFO,
			WARN,
			CRITICAL
		};
		Q_ENUM(Levael)
};

typedef LogLevael::Levael LogLvl;

#endif // LOGLEVAEL_HPP
