#include "backuplog.hpp"

BackupLog::BackupLog(LogLvl lvl, QString msg) : levael(lvl), logText(msg) {}

QJsonObject BackupLog::toObject() const {
	QJsonObject res;

	res["levael"] = int(levael);
	res["log_text"] = logText;

	return res;
}
