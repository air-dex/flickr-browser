#ifndef FLICKRBROWSERTHREAD_HPP
#define FLICKRBROWSERTHREAD_HPP

#include <QThread>
#include <QUuid>

class FlickrBrowserThread : public QThread
{
	Q_OBJECT

	public:
		FlickrBrowserThread();
		QUuid getId() const;

		static const QUuid getNullId();

	public slots:
		/// @brief Emergency stop.
		void stahp(bool brutal = false);

	protected slots:
		virtual void terminate();

	protected:
		QUuid id;
		static const QUuid NULL_ID;
};

#endif // FLICKRBROWSERTHREAD_HPP
