#include "album.hpp"

#include <QQmlEngine>
#include <QDir>
#include <QJsonDocument>
#include <QString>
#include <QTextStream>

#include "flickrbrowser.hpp"
#include "photo.hpp"

Album::Album() :
	QObject(),
	id(""),
	photoCount(0),
	title(""),
	description(""),
	createdAt(),
	lastUpdatedAt(),
	coverPhotoID(""),
	photoIDs()
{}

Album::Album(QJsonObject albumDatas) : QObject () {
	fillWithJSON(albumDatas);
}

Album::Album(const Album & otherAlbum) : QObject () {
	copy(otherAlbum);
}

Album::~Album() {}

void Album::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<Album>(FlickrBrowser::CORE_URI.constData(),
						   FlickrBrowser::MAJOR_VERSION,
						   FlickrBrowser::MINOR_VERSION,
						   "Album");
}

const Album & Album::operator=(const Album & otherAlbum) {
	copy(otherAlbum);
	return *this;
}

const Album & Album::operator=(const QJsonObject & albumDatas) {
	fillWithJSON(albumDatas);
	return *this;
}

void Album::copy(const Album & otherAlbum) {
	this->id = otherAlbum.id;
	this->photoCount = otherAlbum.photoCount;
	this->title = otherAlbum.title;
	this->description = otherAlbum.description;
	this->createdAt = otherAlbum.createdAt;
	this->lastUpdatedAt = otherAlbum.lastUpdatedAt;
	this->coverPhotoID = otherAlbum.coverPhotoID;
	this->photoIDs = otherAlbum.photoIDs;
}

void Album::fillWithJSON(const QJsonObject & albumDatas) {
	bool ok = false;

	this->id = albumDatas.value("id").toString("");
	this->title = albumDatas.value("title").toString("");
	this->description = albumDatas.value("description").toString("");

	// Photo count is in a string. It is not à number.
	uint nbPhotos = albumDatas.value("photo_count").toString("").toUInt(&ok);
	this->photoCount = ok ? nbPhotos : 0;

	// Set dates with timestamp in JSON.
	qlonglong timestamp = albumDatas.value("created").toString("").toLongLong(&ok);
	this->createdAt.setSecsSinceEpoch(ok ? timestamp : 0);
	timestamp = albumDatas.value("last_updated").toString("").toLongLong(&ok);
	this->lastUpdatedAt.setSecsSinceEpoch(ok ? timestamp : 0);

	// The JSON Object gives an URL to parse for the cover photo.
	QString photoURL = albumDatas.value("cover_photo").toString("");
	QRegularExpressionMatch match = COVER_PHOTO_REGEX.match(photoURL);
	this->coverPhotoID = match.hasMatch() ? match.captured("photoid") : "";

	// Filling photosID with the "photos" JSON list.
	QJsonArray jsonPhotos = albumDatas.value("photos").toArray();
	this->photoIDs.clear();
	for (QJsonArray::Iterator it = jsonPhotos.begin();
		 it != jsonPhotos.end();
		 ++it)
	{
		this->photoIDs.append(it->toString(""));
	}
}

// Cover photo regex
const QRegularExpression Album::COVER_PHOTO_REGEX = Album::setCoverPhotoRegex();

QRegularExpression Album::setCoverPhotoRegex() {
	QString regex = "";
	QTextStream textStream(&regex);

	textStream << "^"
			   << QRegularExpression::escape("https://www.flickr.com/photos//")
			   << "(?<photoid>\\d+)"
			   << "$";

	return QRegularExpression(regex);
}

// Camera roll
const QString Album::ROLL_ID = "Roll";
Album Album::ROLL;

void Album::buildRoll() {
	Album::ROLL.id = Album::ROLL_ID;
	Album::ROLL.photoIDs = Photo::getAllValidIDs();
	Album::ROLL.photoCount = uint(Album::ROLL.photoIDs.count());
}

bool Album::isRoll() const {
	return this->id == Album::ROLL_ID;
}

// All Flickr albums
QMap<QString, Album> Album::FLICKR_ALBUMS;

const QMap<QString, Album> & Album::getAllFlickrAlbums() {
	return FLICKR_ALBUMS;
}

void Album::fillAllAlbumsMap() {
	// Extracting albums data from albums.json
	QString albumFilename = "";
	QTextStream stream(&albumFilename);

	stream << FlickrBrowser::FB_ROOT.currentPath()
		   << QDir::separator() << "Datas"
		   << QDir::separator() << "Metadata"
		   << QDir::separator() << "albums.json";

	QJsonDocument doc = FlickrBrowser::getJSONfromFile(albumFilename);

	/*
	 * Parsed data should be a JSON object with a field called "albums".
	 * The field "albums" should be a JSON array.
	 */
	if (!doc.isObject()) {
		// TODO: improve error case
		qWarning("Unexpected album datas. They should be into a JSON Object");
		return;
	}

	QJsonObject albumsObj = doc.object();

	if (!albumsObj.contains("albums") || !albumsObj["albums"].isArray()) {
		// TODO: improve error case
		qWarning("Unexpected album datas. They should be into a JSON array field called \"albums\".");
		return;
	}

	QJsonArray albums = albumsObj["albums"].toArray();

	for(QJsonArray::Iterator it = albums.begin();
		it != albums.end();
		++it)
	{
		if (!it->isObject()) {
			QString errMsg = "";
			QTextStream errStr(&errMsg);
			errStr << "Expecting JSON object for an album. Got this instead: "
				   << it->toString();
			qWarning(errMsg.toLatin1().data());
			continue;
		}

		Album a = it->toObject();
		if (a.isValid()) {
			FLICKR_ALBUMS.insert(a.getId(), a);
		}
	}
}

Album Album::getAlbum(QString albumID) {
	return albumID == ROLL_ID ? ROLL : FLICKR_ALBUMS.value(albumID);
}

// Other
bool Album::isValid() const {
	return this->id != "";
}

bool Album::operator==(const Album & otherAlbum) const {
	return this->id == otherAlbum.id;
}

// Getters and setters
QString Album::getId() const {
	return this->id;
}

QString Album::getTitle() const {
	return this->title;
}

uint Album::getPhotoCount() const {
	return this->photoCount;
}

QString Album::getDescription() const {
	return this->description;
}

QDateTime Album::getCreatedAt() const {
	return this->createdAt;
}

QDateTime Album::getLastUpdatedAt() const {
	return this->lastUpdatedAt;
}

QString Album::getCoverPhotoID() const {
	return this->coverPhotoID;
}

QJsonArray Album::getPhotoIDsJSON() const {
	return QJsonArray::fromStringList(this->photoIDs);
}

QStringList Album::getPhotoIDs() const {
	return this->photoIDs;
}

const QString Album::getRollId() {
	return Album::ROLL_ID;
}

const Album & Album::getRoll() {
	return Album::ROLL;
}
