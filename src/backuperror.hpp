#ifndef BACKUPERROR_HPP
#define BACKUPERROR_HPP

#include <QString>

/*****************************
 * Error messages management *
 *****************************/
namespace BackupError
{
	extern const QString TWO_MSGS_PATTERN;

	// Error messages

	// Tier 1: backup process general errors.
	extern const QString CANNOT_CREATE_BACKUP_DATA;
	extern const QString CANNOT_INIT_BACKUP;
	extern const QString CANNOT_INIT_ALBUM_BACKUP;
	extern const QString CANNOT_WRITE_ALBUM_DESCRIPTION;
	extern const QString CANNOT_BACKUP_PHOTO;
	extern const QString CANNOT_END_ALBUM_BACKUP;
	extern const QString CANNOT_END_BACKUP;
	extern const QString CANNOT_ROLLBACK_BACKUP;
	extern const QString CANNOT_RELEASE_BACKUP_DATA;
	extern const QString CANNOT_STAHP_BACKUP;

	// Tier 2: operations errors.
	extern const QString CANNOT_GET_BACKUP_MANAGER;
	extern const QString ABORT_BACKUP_ERRMSG;
	extern const QString LOCATION_NOT_EXISTS;
	extern const QString INVALID_ALBUM;
	extern const QString INVALID_PHOTO;
	extern const QString FILE_OUT_OF_LOCATION;
	extern const QString FOLDER_OUT_OF_LOCATION;
	extern const QString CANNOT_OPEN_FILE;
	extern const QString CANNOT_REMOVE_FILE;
	extern const QString CANNOT_REMOVE_FOLDER;
	extern const QString ABORT_REMOVE;
	extern const QString UNEXPECTED_BACKUPED_ELEMENT;
	extern const QString CANNOT_CREATE_FOLDER;
	extern const QString CANNOT_OPEN_FOLDER;
	extern const QString CANNOT_COPY_FILE;
	extern const QString CANNOT_WRITE_FILE;
	extern const QString NULL_ARCHIVE_MANAGER;
	extern const QString CANNOT_CREATE_ARCHIVE_MANAGER;
	extern const QString CANNOT_OPEN_ARCHIVE;
	extern const QString CANNOT_CLOSE_ARCHIVE;
	extern const QString CANNOT_WRITE_ROOT_ARCHIVE_FOLDER;
	extern const QString CANNOT_WRITE_ALBUM_ARCHIVE_FOLDER;
	extern const QString CANNOT_WRITE_ALBUM_ARCHIVE_FILE;
	extern const QString NOT_RUNNING_BACKUP;
	extern const QString RUNNING_BACKUP;
	extern const QString UNKONWN_ARCHIVE_TYPE;
	extern const QString ALREADY_SAVING;
	extern const QString NULL_BACKUP_MANAGER;
	extern const QString CANNOT_CLEAR_BACKUP;

	// Info messages

	// Info Tier 1: high-levelled info messages about what happened
	extern const QString BACKUP_OK;
	extern const QString BACKUP_KO;
	extern const QString BACKUP_STAHPPED_OK;
	extern const QString BACKUP_STAHPPED_KO;
	extern const QString BACKUP_STAHPPED_OK_ERR;

	// Info Tier 2: basic info
	extern const QString BEGIN_SAVE_INFO;
	extern const QString PREMATURE_END_SAVE_INFO;
	extern const QString CLOSING_SAVE_INFO;
	extern const QString END_SAVE_INFO;
	extern const QString BEGIN_ROLLBACK_INFO;
	extern const QString END_ROLLBACK_INFO;
	extern const QString ALBUM_BACKUP_INFO;
	extern const QString STAHP_BACKUP_INFO;
	extern const QString STAHP_BACKUP_INFO_END;
}

#endif // BACKUPERROR_HPP
