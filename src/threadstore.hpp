#ifndef THREADSTORE_HPP
#define THREADSTORE_HPP

#include <QMap>
#include <QUuid>
class FlickrBrowserThread;

class ThreadStore : protected QMap<QUuid, FlickrBrowserThread *>
{
	public:
		static ThreadStore & getThreadStore();

		bool storeThread(FlickrBrowserThread * thread);
		void removeThread(FlickrBrowserThread * thread);
		FlickrBrowserThread * getThread(QUuid id) const;
		bool isOver(QUuid threadID) const;

	protected:
		ThreadStore();
		virtual ~ThreadStore();

		/// @brief Deleted thread UUIDs.
		QList<QUuid> graveyard;
};

#endif // THREADSTORE_HPP
