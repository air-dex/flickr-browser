#ifndef WALLPAPERMANAGER_HPP
#define WALLPAPERMANAGER_HPP

#include <QString>
#include <QColor>
#include "wallpaperopts.hpp"

class WallpaperManager
{
	public:
		WallpaperManager();
		virtual ~WallpaperManager();

		virtual bool setWallpaper(QString newPath, WpOpts newWpOpt, QColor newBgColor = QColor()) = 0;

		/// @brief Retrieves and set values from Windows datas.
		/// @return true if data are now valid, false otherwise.
		virtual bool retrieveWallpaperDatas() = 0;

		bool isValid() const;
		QString getWpPath() const;
		WpOpts getWpOpts() const;
		QColor getBgColor() const;

	protected:
		bool valid;
		QString wpPath;
		WpOpts wpOpts;
		QColor bgColor;
};

#endif // WALLPAPERMANAGER_HPP
