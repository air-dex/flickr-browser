#include "progressgauge.hpp"

#include <QMetaType>

ProgressGauge::ProgressGauge() : no(0), nb(0) {}

void ProgressGauge::declareQt() {
	qRegisterMetaType<ProgressGauge>("ProgressGauge");
}

QJsonObject ProgressGauge::toObject() const {
	QJsonObject res;
	res["no"] = no;
	res["nb"] = nb;
	return res;
}

int ProgressGauge::getNo() const {
	return no;
}

void ProgressGauge::incNo() {
	no++;
}

void ProgressGauge::resetNo() {
	no = 0;
}

void ProgressGauge::decNo() {
	no--;
}

int ProgressGauge::getNb() const {
	return nb;
}

void ProgressGauge::setNb(int value) {
	nb = value;
}

void ProgressGauge::incNb() {
	nb++;
}

void ProgressGauge::decNb() {
	nb--;
}

void ProgressGauge::reset() {
	no = nb = 0;
}
