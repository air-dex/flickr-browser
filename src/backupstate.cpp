#include "backupstate.hpp"

#include <QQmlEngine>
#include "flickrbrowser.hpp"
#include <QtDebug>

BackupState::BackupState() : QObject() {}

void BackupState::declareQML() {
	// @uri FlickrBrowserCore
	qmlRegisterType<BackupState>(FlickrBrowser::CORE_URI.constData(),
								 FlickrBrowser::MAJOR_VERSION,
								 FlickrBrowser::MINOR_VERSION,
								 "BackupState");
}

QString BackupState::stringState(SaveState bstate) {
	switch (bstate) {
		case BackupState::NOTHING:
			return "nothing";

		case BackupState::BEGIN:
			return "begin";

		case BackupState::COPY:
			return "copy";

		case BackupState::CLOSING:
			return "closing";

		case BackupState::FINISH:
			return "finish";

		case BackupState::ROLLBACK:
			return "rollback";

		case BackupState::FAIL:
			return "fail";

		case BackupState::STAHP:
			return "stahp";

		default:
			return "";
	}
}
