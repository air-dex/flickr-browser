#!/bin/pwsh

$root = (Get-Location).Path	# Pas de \ à la fin.
$cmake_exe = 'C:\Program Files\CMake\bin\cmake.exe'
$QT_ROOT = 'C:\Qt\5.12.0\msvc2017_64'

# Mise en place des submodules.
git submodule add https://github.com/madler/zlib.git 3rdparty/zlib/src
git submodule add git://anongit.kde.org/extra-cmake-modules.git 3rdparty/ecm/src
git submodule add git://anongit.kde.org/karchive.git 3rdparty/karchive/src
cd libs/zlib
git reset --hard v1.2.11
cd ../ecm
git reset --hard v5.56.0
cd ../karchive
git reset --hard v5.56.0
cd ../..
git commit -a -m "Karchive submodules"

# Build zlib dans bin/zlib
cd bin/zlib
&$cmake_exe "$root\libs\zlib" -DCMAKE_BUILD_TYPE=debug -DCMAKE_INSTALL_PREFIX="$root\bin\zlib\bin" -G 'Visual Studio 15 2017 Win64'
&$cmake_exe --build .
&$cmake_exe --build . --target install
$ZLIB_ROOT_DIR = "$root\bin\zlib\bin"

# Build ECM dans bin/ecm
cd ../ecm
&$cmake_exe "$root\libs\ecm" -DCMAKE_BUILD_TYPE=debug -DCMAKE_INSTALL_PREFIX="$root\bin\ecm\bin" -G 'Visual Studio 15 2017 Win64'
&$cmake_exe --build .
&$cmake_exe --build . --target install
$ECM_DIR = "$root\bin\ecm\bin\share\ECM\cmake"

# LZMA stuff
# LZMA DL from the website : https://tukaani.org/xz/ (prebuilt-bin)
$LZMA_DIR="$root\libs\lzma"
lib /def:"$LZMA_DIR\doc\liblzma.def" /out:"$LZMA_DIR\bin_x86-64\liblzma.lib" /machine:x64

# Build KArchive dans bin/karchive
cd ../karchive
&$cmake_exe "$root\libs\karchive" `
	-DCMAKE_BUILD_TYPE=debug `
	-DCMAKE_INSTALL_PREFIX="$root\bin\karchive\bin" `
	-G 'Visual Studio 15 2017 Win64' `
	-DZLIB_INCLUDE_DIR="$ZLIB_ROOT_DIR\include" `
	-DZLIB_LIBRARY="$ZLIB_ROOT_DIR\lib\zlibd.lib" `
	-DECM_DIR="$ECM_DIR" `
	-DCMAKE_PREFIX_PATH="$QT_ROOT" `
	-DLIBLZMA_INCLUDE_DIR="$LZMA_DIR\include" `
	-DLIBLZMA_LIBRARY="$LZMA_DIR\bin_x86-64\liblzma.lib"
&$cmake_exe --build .
&$cmake_exe --build . --target install
