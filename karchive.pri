# karchive.pri: Binding KArchive to Flickr Browser.

INCLUDEPATH += $$PWD/3rdparty/karchive/build/$$MODE/bin/include/KF5/KArchive
LIBS += $$PWD/3rdparty/karchive/build/$$MODE/bin/lib/KF5Archive.lib

ready_karchive.name = ready_karchive
ready_karchive.commands = $$POWERSHELL -Cmd Ready-Karchive -$$MODE -cmakeEXE $$CMAKE_EXEC

wipeout_all_libs.name = wipeout_all_libs
wipeout_all_libs.commands = $$POWERSHELL -Cmd Wipeout-All-Libs -$$MODE

QMAKE_EXTRA_TARGETS += ready_karchive wipeout_all_libs
