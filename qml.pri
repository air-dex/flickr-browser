# QML files

DISTFILES += \
	qml/main.qml \
	qml/FlickrBrowser.qml \
	qml/FlickrBrowserTabs.qml \
	qml/Constants.qml \
	qml/ViewAlbumTab.qml \
	qml/AlbumViewer.qml \
	qml/AlbumHeader.qml \
	qml/PhotoViewer.qml \
	qml/FlickrBrowserImage.qml \
	qml/PhotoMetadata.qml \
	qml/AlbumRoll.qml \
	qml/AlbumButtonRow.qml \
	qml/ShowFullscreenWindow.qml \
	qml/ExifWindow.qml \
	qml/ExifDatasView.qml \
	qml/FlickrBrowserLabel.qml \
	qml/CameraRollTab.qml \
	qml/SaveImageDialog.qml \
	qml/SetAsWallpaperWindow.qml \
	qml/SaveImageKOMessageBox.qml \
	qml/FlickrBrowserComboBox.qml \
	qml/BackupTab.qml \
	qml/AlbumsMultiSelect.qml \
	qml/FlickrBrowserFileDialog.qml \
	qml/BackupPrompt.qml \
	qml/ProgressPrompt.qml \
	qml/SavePrompt.qml \
	qml/IndefiniteProgress.qml

qml_files.source = ui/qml
qml_files.target = ui

DEPLOYMENTFOLDERS += qml_files

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =
