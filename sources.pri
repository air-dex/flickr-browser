SOURCES += \
	src/main.cpp \
	src/flickrbrowserhelper.cpp \
	src/album.cpp \
	src/flickrbrowser.cpp \
	src/albumcontroller.cpp \
	src/photocontroller.cpp \
	src/photo.cpp \
	src/exiftablemodel.cpp \
	src/wallpaperopts.cpp \
	src/wallpapermanager.cpp \
	src/wallpapercontroller.cpp \
	src/archivetype.cpp \
	src/archivecontroller.cpp \
	src/backupmanager.cpp \
	src/backupfoldermanager.cpp \
	src/backuparchivemanager.cpp \
	src/backuputils.cpp \
	src/backuperror.cpp \
	src/flickrbrowserthread.cpp \
	src/threadstore.cpp \
	src/backupstate.cpp \
	src/loglevael.cpp \
	src/backuplog.cpp \
	src/progressgauge.cpp \
	src/copyingprogress.cpp

HEADERS += \
	src/flickrbrowserhelper.hpp \
	src/album.hpp \
	src/flickrbrowser.hpp \
	src/albumcontroller.hpp \
	src/photocontroller.hpp \
	src/photo.hpp \
	src/exiftablemodel.hpp \
	src/wallpaperopts.hpp \
	src/wallpapermanager.hpp \
	src/wallpapercontroller.hpp \
	src/archivetype.hpp \
	src/archivecontroller.hpp \
	src/backupmanager.hpp \
	src/backupfoldermanager.hpp \
	src/backuparchivemanager.hpp \
	src/backuputils.hpp \
	src/backuperror.hpp \
	src/flickrbrowserthread.hpp \
	src/threadstore.hpp \
	src/backupstate.hpp \
	src/loglevael.hpp \
	src/backuplog.hpp \
	src/progressgauge.hpp \
	src/copyingprogress.hpp

# OS-related sources

# Windows
WINDOWS_SOURCES = \
	src/windows/flickrbrowser_windows.cpp \
	src/windows/windowswpm.cpp

WINDOWS_HEADERS = \
	src/windows/windows_includes.hpp \
	src/windows/flickrbrowser_windows.hpp \
	src/windows/windowswpm.hpp

win32 {
	SOURCES += $$WINDOWS_SOURCES
	HEADERS += $$WINDOWS_HEADERS
} else {
	DISTFILES += $$WINDOWS_SOURCES $$WINDOWS_HEADERS
}
