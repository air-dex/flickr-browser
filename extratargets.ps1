# extratargets.ps1 : extra targets for project

Param(
    [string] $cmd = '',

    # MSVC 2017 informations
	[string] $msvc_version = '14',
    [string] $msvc_install_dir = 'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community',

    # Windows SDK Information
    # TODO : complete
    [string] $winsdk_version = '10',
    [string] $winsdk_root = 'C:\Program Files (x86)\Windows Kits\10\',
    
    # Qt informations
	[string] $qtversion = '5.12.0',
    [string] $qtroot = 'C:\Qt',
    [string] $qtdir = "$qtroot\$qtversion\msvc2017_64",

    # Cmake information
    [string] $cmakeDir = 'C:\Program Files\CMake',

    # Using a custom make (nmake?) program instead of Qt's JOM.
    [switch] $avoidJOM,
    [string] $jomPath = "$qtroot\Tools\QtCreator\bin"
)

function dummyMoore {}

function Augment-Env {
    # Augment $env:PATH

    # Add Qt binaries and Cmake
    $newPaths = @("$qtdir\bin", "$cmakeDir\bin")

    # Have to add the building tools to the PATH env variable. Otherwise this error will occur for qmake:
	#       Project ERROR: Cannot run compiler 'cl'. Maybe you forgot to setup the environment?
	$clpath = (Resolve-Path -Path "$msvc_install_dir\VC\Tools\MSVC\$msvc_version*\bin\HostX64\x64").Path
    if ($null -ne $clpath) {
        $newPaths += $clpath
    }

    if (-not $avoidJOM) {
        $newPaths += $jomPath
    }

    # Add new pathes
    $env:path += (';' + ($newPaths -join ';'))

    # Other env variables
    $env:QTDIR = $qtdir
}

############################
# Compiling Flickr Browser #
############################

function Run-QMake {
    param (
        [string] $qmakeEXE = 'qmake.exe',
        [string] $makeEXE = 'nmake.exe',
        [string] $jomEXE = 'jom.exe',
        [switch] $debug = $false,
        [switch] $qmlDebug = $false,
        [switch] $useQtQuickCompiler = $false,
        [switch] $separateDebugInfo = $false
    )

    $config = @()
    $mode = ''

    if ($debug) {
        $mode = 'debug'
    } else {
        $mode = 'release'
    }
    $config += $mode

    if ($qmlDebug) { $config += 'qml_debug' }

    if ($useQtQuickCompiler) { $config += 'qtquickcompiler' }

    if ($separateDebugInfo) { $config += ('force_debug_info', 'separate_debug_info') }

    $configOpts = '"CONFIG += ' + ($config -join ' ') + '"'
	
	if (-not $avoidJOM) {
		$makeEXE = $jomEXE
	}

    Push-Location -StackName $loc_stack "$root\bin"
    &$qmakeEXE "$root\FlickrBrowser.pro" -spec win32-msvc $configOpts
    &$makeEXE qmake_all
    Pop-Location -StackName $loc_stack
}

# Just building following makefile instructions.
# FIXME: not working due to missing Windows stuff (likely from Windows SDK).
function Build-FB {
    param (
        [string] $makeEXE = 'nmake.exe',
        [string] $jomEXE = 'jom.exe'
    )

    Push-Location -StackName $loc_stack -Path "$root\bin"

    if ($avoidJOM) {
		&$makeEXE
	}
	else {
		&$jomEXE
    }
    
    Pop-Location -StackName $loc_stack
}

# Cleans what is built by compilers but preserve makefiles.
function Clean-FB {
    Param (
        [switch] $debug,
        [switch] $release
    )

    if ($debug) {
        Write-Host "Clean Flickr Browser, profile debug"
        Push-Location -StackName $loc_stack -Path "$root\bin\debug"
        Remove-Item -Recurse -Force -Path * -Exclude .gitkeep
        Pop-Location -StackName $loc_stack
    }

    if ($release) {
        Write-Host "Clean Flickr Browser, profile release"
        Push-Location -StackName $loc_stack -Path "$root\bin\release"
        Remove-Item -Recurse -Force -Path * -Exclude .gitkeep
        Pop-Location -StackName $loc_stack
    }
}

# Cleans all build stuff: things built by compilers + makefiles.
function Wipeout-FB {
    Param (
        [switch] $debug,
        [switch] $release
    )

    if ($debug) { Write-Host "Wipe out Flickr Browser, profile debug" }
    if ($release) { Write-Host "Wipe out Flickr Browser, profile release" }

    Clean-FB -debug:$debug -release:$release
    Push-Location -StackName $loc_stack -Path "$root\bin"
    Get-ChildItem -Force * `
        | Where-Object { @('debug', 'release') -notcontains $_.Name } `
        | Remove-Item -Recurse -Force
    Pop-Location -StackName $loc_stack
}

############################
# Stuff for 3rd party libs #
############################

function Cmake-Lib-Internals  {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [string] $libname,
        [string] $buildType,
        [string] $buildDir = '.'
    )

    $buildProfile = $buildType.ToLower()
    Write-Host "cmake for $libname, profile $buildProfile"
    Push-Location -StackName $loc_stack -Path "$root\3rdparty\$libname"
    $lib_dir = (Get-Location).Path
    Set-Location (Resolve-Path -Path "build\$buildProfile").Path

    if (-not (Test-Path -Path $buildDir)) {
        New-Item -ItemType Directory $buildDir
    }

    &$cmakeEXE "$lib_dir\src" `
        -DCMAKE_BUILD_TYPE=$buildType `
        -DCMAKE_INSTALL_PREFIX="$((Resolve-Path -Path $buildDir).Path)" `
        -G 'Visual Studio 15 2017 Win64'
    Pop-Location -StackName $loc_stack
}

function Cmake-Lib  {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [string] $libname,
        [switch] $debug,
        [switch] $release,
        [string] $buildDir = '.'
    )

    if ($debug) {
        Cmake-Lib-Internals -buildType Debug -libname $libname -cmakeEXE $cmakeEXE -buildDir $buildDir
    }

    if ($release) {
        Cmake-Lib-Internals -buildType Release -libname $libname -cmakeEXE $cmakeEXE -buildDir $buildDir
    }
}

function Cmake-Build-Lib-Internals  {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [string] $libname,
        [string] $buildType,
        [string] $target = ''
    )

    $buildType = $buildType.ToLower()
    Write-Host "cmake build $target for $libname, profile $buildType"
    Push-Location -StackName $loc_stack -Path (Resolve-Path "$root\3rdparty\$libname\build\$buildType").Path

    if ($target.Length -gt 0) {
        &$cmakeEXE --build . --target $target
    }
    else {
        &$cmakeEXE --build .
    }

    Pop-Location -StackName $loc_stack
}

function Cmake-Build-Lib {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [string] $libname,
        [switch] $debug,
        [switch] $release,
        [string] $target = ''
    )

    if ($debug) {
        Cmake-Build-Lib-Internals -buildType Debug -target $target -libname $libname -cmakeEXE $cmakeEXE
    }

    if ($release) {
        Cmake-Build-Lib-Internals -buildType Release -target $target -libname $libname -cmakeEXE $cmakeEXE
    }
}

function Wipeout-Lib  {
    Param (
        [string] $libname,
        [switch] $debug,
        [switch] $release
    )

    Push-Location -StackName $loc_stack -Path "$root\3rdparty\$libname\build"

    if ($debug) {
        Write-Host "Wipe out $libname, profile debug"
        Set-Location debug
        Remove-Item -Recurse -Force * -Exclude .gitkeep
        Set-Location ..
    }

    if ($release) {
        Write-Host "Wipe out $libname, profile release"
        Set-Location release
        Remove-Item -Recurse -Force * -Exclude .gitkeep
        Set-Location ..
    }
    
    Pop-Location -StackName $loc_stack
}

function Reset-Submodule  {
    Param (
        [string] $gitEXE = 'git.exe',
        [string] $libname,
        [string] $gitURL,
        [string] $libversion = '',
        [switch] $isCommit = $false
    )

    $submoduleName = "3rdparty/$libname/src"

    &$gitEXE submodule deinit -f -- "$submoduleName"
    Remove-Item -Recurse -Force "$root\.git\modules\$submoduleName"
    &$gitEXE rm -f $submoduleName
    &$gitEXE submodule add $gitURL "$submoduleName"
    
    # Force the version
    if ($libversion.Length -gt 0) {
        Set-Version-Lib @args
    }
}

function Update-Submodule  {
    Param (
        [string] $gitEXE = 'git.exe',
        [string] $libname
    )

    &$gitEXE submodule update --remote "3rdparty/$libname/src"
}

function Set-Version-Lib  {
    Param (
        [string] $gitEXE = 'git.exe',
        [string] $libname,
        [string] $libversion = '',
        [switch] $isCommit = $false
    )
    
    Push-Location -StackName $loc_stack -Path "$root\$3rdparty\$libname\src"

    if ($isCommit) {
        &$gitEXE reset --hard $libversion
    }
    else {
        &$gitEXE reset --hard "v$libversion"
    }

    Pop-Location -StackName $loc_stack
}

##################
# Compiling zlib #
##################

# Just Runs CMake for zlib
function Cmake-zlib {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )

    Cmake-Lib -libname zlib -cmakeEXE $cmakeEXE -debug:$debug -release:$release -buildDir .\bin
}

# Building zlib
function Build-zlib {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )

    Cmake-Build-Lib -libname zlib -cmakeEXE $cmakeEXE -debug:$debug -release:$release
}

# Installng zlib
function Install-zlib {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )

    Cmake-Build-Lib -libname zlib -target install -cmakeEXE $cmakeEXE -debug:$debug -release:$release
}

# Just cleans things built by compilers.
function Clean-zlib {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )
    
    Cmake-Build-Lib -libname zlib -target clean -cmakeEXE $cmakeEXE -debug:$debug -release:$release
}

# Wipes out everything built, by Cmake or compilers.
function Wipeout-zlib {
    Param (
        [switch] $debug,
        [switch] $release
    )

    Wipeout-Lib -libname zlib -debug:$debug -release:$release
}

#################
# Compiling ECM #
#################

# Just Runs CMake for ECM
function Cmake-ECM {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )

    Cmake-Lib -libname ecm -cmakeEXE $cmakeEXE -debug:$debug -release:$release  -buildDir .\bin
}

# Building ECM
function Build-ECM {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )
    
    Cmake-Build-Lib -libname ecm -cmakeEXE $cmakeEXE -debug:$debug -release:$release
}

# Installng ECM
function Install-ECM {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )
    
    Cmake-Build-Lib -libname ecm -target install -cmakeEXE $cmakeEXE -debug:$debug -release:$release
}

# Just cleans things built by compilers.
function Clean-ECM {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )
    
    Cmake-Build-Lib -libname ecm -target clean -cmakeEXE $cmakeEXE -debug:$debug -release:$release
}

# Wipes out everything built, by Cmake or compilers.
function Wipeout-ECM {
    Param (
        [switch] $debug,
        [switch] $release
    )

    Wipeout-Lib -libname ecm -debug:$debug -release:$release
}


######################
# Compiling KArchive #
######################

# Just runs CMake for KArchive
function Cmake-KArchive-Internals  {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [string] $buildType
    )

    $buildProfile = $buildType.ToLower()

    Write-Host "cmake for karchive, profile $buildProfile"

    $ZLIB_ROOT_DIR = (Resolve-Path -Path "$root\3rdparty\zlib\build\$buildProfile\bin").Path
    $ECM_DIR = (Resolve-Path -Path "$root\3rdparty\ecm\build\$buildProfile\bin\share\ECM\cmake").Path
    $LZMA_DIR = "$root\3rdparty\lzma\build"
    Push-Location -StackName $loc_stack -Path "$root\3rdparty\karchive"
    $lib_dir = (Get-Location).Path
    Set-Location (Resolve-Path -Path "build\$buildProfile").Path
    &$cmakeEXE "$lib_dir\src" `
        -DCMAKE_BUILD_TYPE=$buildType `
        -DCMAKE_INSTALL_PREFIX="$((Get-Location).Path)\bin" `
        -G 'Visual Studio 15 2017 Win64' `
        -DZLIB_INCLUDE_DIR="$ZLIB_ROOT_DIR\include" `
        -DZLIB_LIBRARY="$ZLIB_ROOT_DIR\lib\zlibd.lib" `
	    -DECM_DIR="$ECM_DIR" `
	    -DCMAKE_PREFIX_PATH="$qtdir" `
        -DLIBLZMA_INCLUDE_DIR="$LZMA_DIR\include" `
        -DLIBLZMA_LIBRARY="$LZMA_DIR\bin_x86-64\liblzma.lib"
    Pop-Location -StackName $loc_stack
}

function Cmake-KArchive  {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )

    if ($debug) { Cmake-KArchive-Internals -cmakeEXE $cmakeEXE -buildType Debug }

    if ($release) { Cmake-KArchive-Internals -cmakeEXE $cmakeEXE -buildType Release }
}

# Building KArchive
function Build-KArchive  {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )
    
    Cmake-Build-Lib -libname karchive -cmakeEXE $cmakeEXE -debug:$debug -release:$release
}

# Installng KArchive
function Install-KArchive {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )
    
    Cmake-Build-Lib -libname karchive -target install -cmakeEXE $cmakeEXE -debug:$debug -release:$release
}

# Just cleans things built by compilers.
function Clean-KArchive {
    Param (
        [string] $cmakeEXE = 'cmake.exe',
        [switch] $debug,
        [switch] $release
    )
    
    Cmake-Build-Lib -libname karchive -target clean -cmakeEXE $cmakeEXE -debug:$debug -release:$release
}

# Wipes out everything built, by Cmake or compilers.
function Wipeout-KArchive {
    Param (
        [switch] $debug,
        [switch] $release
    )

    Wipeout-Lib -libname karchive -debug:$debug -release:$release
}


##################
# Compiling LZMA #
##################

# Extracting from the archive
function Extract-LZMA {
    Expand-Archive -Path (Resolve-Path -Path "$root\3rdparty\lzma\src\*.zip").Path -DestinationPath "$root\3rdparty\lzma\build"
}

# Building .lib file
function Build-LZMA {
    lib /def:"$root\3rdparty\lzma\build\doc\liblzma.def" /out:"$root\3rdparty\lzma\build\bin_x86-64\liblzma.lib" /machine:x64
}

# Cleaning things built by Build-LZMA
function Clean-LZMA {
    Push-Location -StackName $loc_stack -Path "$root\3rdparty\lzma\build\bin_x86-64"
    Remove-Item -Force -Path liblzma.lib,liblzma.exp
    Pop-Location -StackName $loc_stack
}

# Wipes out LZMA build directory content (except .gitkeep)
function Wipeout-LZMA {
    Push-Location -StackName $loc_stack -Path "$root\3rdparty\lzma\build"
    Remove-Item -Recurse -Force * -Exclude .gitkeep
    Pop-Location -StackName $loc_stack
}

##############
# Deployment #
##############

function Deploy-DLLs-Internals {
    param (
        [string] $buildType,
        [switch] $useDebug,
        [switch] $includeQtDLLs,
        [switch] $includeQtPDBs
    )

    # List files to copy
    $lzmaDLL = "$root\3rdparty\lzma\build\bin_x86-64\liblzma.dll"
    $zlibDLL = "$root\3rdparty\zlib\build\$buildType\bin\bin\zlibd.dll"
    $karchiveDLL = "$root\3rdparty\karchive\build\$buildType\bin\bin\KF5Archive.dll"
    $files = @($lzmaDLL, $zlibDLL, $karchiveDLL)

    if ($includeQtDLLs) {
        $QtDLLsVerbs = @('Core', 'QuickControls2', 'Gui', 'QML', 'Quick', 'QuickTemplates2', 'Network')
        $QtBinFormat = "$qtdir\bin\Qt5{0}{1}.{2}"
        $debugSuffix = @('', 'd')[$useDebug.ToBool()]

        # DLLs
        $files += ($QtDLLsVerbs | ForEach-Object { $QtBinFormat -f $_, $debugSuffix, 'dll' })

        # PDBs
        if ($includeQtPDBs) {
            $files += ($QtDLLsVerbs | ForEach-Object { $QtBinFormat -f $_, $debugSuffix, 'pdb' })
        }
    }
    
    Copy-Item -Force -Verbose -Path $files -Destination "$root\bin\$buildType"
}

function Deploy-DLLs {
    Param (
        [switch] $debug,
        [switch] $release,
        [switch] $useDebug,
        [switch] $includeQtDLLs,
        [switch] $includeQtPDBs
    )

    if ($debug) {
        Deploy-DLLs-Internals -buildType debug -includeQtDLLs:$includeQtDLLs -useDebug:$useDebug -includeQtPDBs:$includeQtPDBs
    }

    if ($release) {
        Deploy-DLLs-Internals -buildType release -includeQtDLLs:$includeQtDLLs -useDebug:$useDebug -includeQtPDBs:$includeQtPDBs
    }
}

########
# Main #
########

# Script location, which is also supposed to be FlickrBrowser project's root lacation.
$root = Split-Path -Path $MyInvocation.MyCommand.Path
$loc_stack = 'FB_stack'
Push-Location -StackName $loc_stack -Path $root

Augment-Env

# Command switch
switch -exact ($Cmd) {
    # TODO: retirer à la fin.
    'dummy' {
        # Bloc pour faire des tests
        dummyMoore @args
        break
    }

    ############################
    # Compiling Flickr Browser #
    ############################

    # Runs QMake
    'Qmake-FB' { Run-QMake @args ; break }

    # Just build Flickr Browser, not 3rd party libs
    'Build-FB' { Build-FB @args ; break }

    # Clean stuff built by compilers for debug and/or release
    'Clean-FB' { Clean-FB @args ; break }

    # Wipes out FB building stuff: stuff built by compilers + makefiles
    'Wipeout-FB' { Wipeout-FB @args ; break }

    ##################
    # Compiling zlib #
    ##################

    # Just Runs CMake for zlib
    'Cmake-zlib' { Cmake-zlib -libname zlib @args ; break }

    # Building zlib
    'Build-zlib' { Build-zlib @args ; break }

    # Installng zlib
    'Install-zlib' { Install-zlib @args ; break }

    # Just cleans things built by compilers.
    'Clean-zlib' { Clean-zlib @args ; break }

    # Wipes out everything built, by Cmake or compilers.
    'Wipeout-zlib' { Wipeout-zlib @args ; break }

    # Resets the underlying git submodule
    'Reset-Submodule-zlib' {
        Reset-Submodule -libname zlib -gitURL https://github.com/madler/zlib.git @args
        break
    }

    # Updates the zlib submodule
    'Update-Submodule-zlib' { Update-Submodule -libname zlib @args ; break }

    # Setting a new version to use for zlib
    'Set-Version-zlib' { Set-Version-Lib -libname zlib @args ; break }

    #################
    # Compiling ECM #
    #################

    # Just Runs CMake for ECM
    'Cmake-ECM' { Cmake-ECM @args ; break }

    # Building ECM
    'Build-ECM' { Build-ECM @args ; break }

    # Installng ECM
    'Install-ECM' { Install-ECM @args ; break }

    # Just cleans things built by compilers.
    'Clean-ECM' { Clean-ECM @args ; break }

    # Wipes out everything built, by Cmake or compilers.
    'Wipeout-ECM' { Wipeout-ECM @args ; break }

    # Resets the underlying git submodule
    'Reset-Submodule-ECM' {
        Reset-Submodule -libname ecm -gitURL git://anongit.kde.org/extra-cmake-modules.git @args
        break
    }

    # Updates the ECM submodule
    'Update-Submodule-ECM' { Update-Submodule -libname ecm @args ; break }

    # Setting a new version to use for ECM
    'Set-Version-ECM' { Set-Version-Lib -libname ecm @args ; break }

    ##################
    # Compiling LZMA #
    ##################

    # Extracting from the archive
    'Extract-LZMA' { Extract-LZMA ; break }

    # Building .lib file
    'Build-LZMA' { Build-LZMA ; break }

    # Cleaning things built by Build-LZMA
    'Clean-LZMA' { Clean-LZMA ; break }

    # Wipes out LZMA build directory content (except .gitkeep)
    'Wipeout-LZMA' { Wipeout-LZMA ; break }


    ######################
    # Compiling KArchive #
    ######################

    # Just runs CMake for KArchive
    'Cmake-KArchive' { Cmake-KArchive @args ; break }

    # Building KArchive. Dependencies are supposed to be OK.
    'Build-KArchive'  { Build-KArchive @args ; break }

    # Installng KArchive
    'Install-KArchive' { Install-KArchive @args ; break }

    # Just cleans things built by compilers.
    'Clean-KArchive' { Clean-KArchive @args ; break }

    # Wipes out everything built, by Cmake or compilers.
    'Wipeout-KArchive' { Wipeout-KArchive @args ; break }

    # Set Karchive ready to use.
    'Ready-Karchive' {
        Extract-LZMA
        Build-LZMA
        Cmake-zlib @args
        Build-zlib @args
        Install-zlib @args
        Cmake-ECM @args
        Build-ECM @args
        Install-ECM @args
        Cmake-KArchive @args
        Build-KArchive @args
        Install-KArchive @args
        break
    }

    # Resets the underlying git submodule
    'Reset-Submodule-KArchive' {
        Reset-Submodule -libname karchive -gitURL git://anongit.kde.org/karchive.git @args
        break
    }

    # Updates the KArchive submodule
    'Update-Submodule-KArchive' { Update-Submodule -libname karchive @args ; break }

    # Setting a new version to use for KArchive
    'Set-Version-KArchive' { Set-Version-Lib -libname karchive @args ; break }

    ####################
    # Erasing all libs #
    ####################

    # Clean stuff built by compilers for debug and/or release
    'Clean-All-Libs' {
        Clean-KArchive @args
        Clean-ECM @args
        Clean-zlib @args
        Clean-LZMA
        break
    }

    # Wipes out FB building stuff: stuff built by compilers + makefiles
    'Wipeout-All-Libs' {
        Wipeout-KArchive @args
        Wipeout-ECM @args
        Wipeout-zlib @args
        Wipeout-LZMA
        break
    }

    ######################
    # Erasing everything #
    ######################

    # Clean stuff built by compilers for debug and/or release
    'Clean-All' {
        Clean-FB @args
        Clean-KArchive @args
        Clean-ECM @args
        Clean-zlib @args
        Clean-LZMA
        break
    }

    # Wipes out FB building stuff: stuff built by compilers + makefiles
    'Wipeout-All' {
        Wipeout-FB @args
        Wipeout-KArchive @args
        Wipeout-ECM @args
        Wipeout-zlib @args
        Wipeout-LZMA
        break
    }

    ##############
    # Deployment #
    ##############

    # Deploy DLLs for the build
    'Deploy-DLLs' {
        Deploy-DLLs @args
    }

    # TODO

    #################
    # Documentation #
    #################

    # TODO

    ########################
    # Internationalization #
    ########################

    # TODO

    default {
        # TODO: proper error message
        Write-Host "La commande $cmd n'existe pas dans ce script."
        break
    }
}
 
Pop-Location -StackName $loc_stack
