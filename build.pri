# build.pri: Build & run stuff.

# Deploying required DLLs to run the program properly

# All DLLs
deploy_dlls.name = deploy_dlls
CONFIG(debug, debug | release) {
	deploy_dlls.commands = $$POWERSHELL -Cmd Deploy-DLLs -debug -includeQtDLLs -useDebug -includeQtPDBs
}
CONFIG(release, debug | release) {
	deploy_dlls.commands = $$POWERSHELL -Cmd Deploy-DLLs -release -includeQtDLLs
}

# Only 3rd party DLLs
deploy_no_qt_dlls.name = deploy_no_qt_dlls
deploy_no_qt_dlls.commands = $$POWERSHELL -Cmd Deploy-DLLs -$$MODE

QMAKE_EXTRA_TARGETS += deploy_dlls deploy_no_qt_dlls

# Qt Creator generated (and useful) stuff

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
