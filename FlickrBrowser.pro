# FlickrBrowser.pro		Qt Project file

include(flickrbrowser_priv.pri)

QT += quick quickcontrols2
CONFIG += c++11

# Some general definitions
POWERSHELL = $$POWERSHELL_EXEC -ExecutionPolicy Bypass -File $$PWD/extratargets.ps1

CONFIG(debug, debug | release) {
	MODE = debug
}
CONFIG(release, debug | release) {
	MODE = release
}

# Sources
include(sources.pri)

# QML files
include(qml.pri)

# KArchive
include(karchive.pri)

# Building
include(build.pri)

RESOURCES += resources.qrc

DISTFILES += \
	.gitignore \
	buildkarchive.ps1 \
	extratargets.ps1 \
	flickrbrowser_priv.pri \
	flickrbrowser_priv.pri.sample \
	README.md

# Default rules for deployment.
include(deployment.pri)
