import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
	id: fb_tabs

	// Minimal sizes
	property int min_width: Math.max(tabs_bar.implicitWidth, tabs.min_width)
	property int min_height: tabs_bar.height + tabs.min_height

	// TODO: barre pas jusqu'au bout
	TabBar {
		id: tabs_bar

		anchors {
			top: fb_tabs.top
			left: fb_tabs.left
			right: fb_tabs.right
		}

		TabButton {
			text: qsTr("Voir les photos")
		}

		TabButton {
			text: qsTr("Voir un album")
		}

		TabButton {
			text: qsTr("Télécharger des albums")
		}
	}

	StackLayout {
		id: tabs
		currentIndex: tabs_bar.currentIndex

		// Minimal sizes
		property int min_width: Math.max(
									roll_tab.min_width,
									albums_tab.min_width,
									save_tab.min_width
								)
		property int min_height: Math.max(
									 roll_tab.min_height,
									 albums_tab.min_height,
									 save_tab.min_height
								)

		anchors {
			top: tabs_bar.bottom
			left: fb_tabs.left
			right: fb_tabs.right
			bottom: fb_tabs.bottom
		}

		// Looking at all photos here.
		CameraRollTab {
			id: roll_tab
			Layout.fillHeight: true
			Layout.fillWidth: true
		}

		// Looking at albums here
		ViewAlbumTab {
			id: albums_tab
			Layout.fillHeight: true
			Layout.fillWidth: true
		}

		// TODO: Download albums here
		BackupTab {
			id: save_tab
			Layout.fillHeight: true
			Layout.fillWidth: true
		}
	}

	// Tab at launch
	// TODO: which tab choosing? Albums (1)? Roll (0)?
	Component.onCompleted: {
		tabs_bar.currentIndex = 2
	}
}
