import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ColumnLayout {
	id: progress_prompt
	spacing: constants.margin

	property string label: pp_label.text

	property int min_width: pp_label.contentWidth

	Constants { id: constants }

	ProgressBar {
		id: pp_bar
		Layout.fillWidth: true
		indeterminate: true
	}

	FlickrBrowserLabel {
		id: pp_label
		Layout.fillWidth: true
	}
}
