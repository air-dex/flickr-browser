import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ColumnLayout {
	id: progress_prompt
	spacing: constants.margin

	property string label: ""

	property int min_width: pp_label.contentWidth

	Constants { id: constants }

	ProgressBar {
		id: pp_bar
		Layout.fillWidth: true
		from: 0
		value: 0
		to: 1
	}

	FlickrBrowserLabel {
		id: pp_label
		Layout.fillWidth: true
		text: qsTr("%1 n° %L2 / %L3")
				.arg(progress_prompt.label)
				.arg(pp_bar.value)
				.arg(pp_bar.to)
	}

	function updateProgress(progressGauge) {
		pp_bar.value = progressGauge.no
		pp_bar.to = progressGauge.nb
	}
}
