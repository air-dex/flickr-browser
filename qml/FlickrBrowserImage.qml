import QtQuick 2.12
import FlickrBrowserCore 1.0

Image {
	id: flickr_photo

	/// @brief internals to retrieve the photo
	PhotoController { id: controller }

	property string photoID
	property alias photo: controller.photo

	source: photo.getFileURL()
	fillMode: Image.PreserveAspectFit
	horizontalAlignment: Image.AlignHCenter
	verticalAlignment: Image.AlignVCenter

	onPhotoIDChanged: {
		controller.setPhoto(photoID)
	}

	// TODO: photo fallback
}
