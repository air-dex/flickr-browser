import QtQuick 2.12
import QtQuick.Window 2.12
import FlickrBrowserCore 1.0

Window {
	id: sfs_dialog
	title: album.isRoll() ?
			   qsTr("Pellicule")
			 : qsTr("%1 - Album \"%2\"").arg(constants.program_title).arg(album.title)
	modality: Qt.NonModal

	// TODO: compute minimum sizes

	Constants { id: constants }

	AlbumRoll {
		id: album_roll
		roll: album.photo_ids

		anchors {
			fill: parent
			margins: constants.margin
		}
	}

	// Album management
	property string album_id: ""
	property alias album: controller.album

	AlbumController { id: controller }

	onAlbum_idChanged: controller.setAlbum(album_id)
}
