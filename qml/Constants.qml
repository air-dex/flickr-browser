import QtQuick 2.12
import FlickrBrowserCore 1.0

/// @brief QML constants
QtObject {
	/// @brief Universal margin for the program (in pixels).
	readonly property int margin: 5

	// Border

	/// @brief Universal border width
	readonly property int border_width: 1

	/// @brief Universal border color
	readonly property color border_color: "black"

	// Font properties

	/// @brief Program name text size (in points)
	readonly property int program_name_text_size: 30

	/// @brief Title text size (in points)
	readonly property int title_text_size: 18

	/// @brief Medium text size (in points)
	readonly property int medium_text_size: 11

	/// @brief Small text size (in points)
	readonly property int small_text_size: 9

	/// @brief Font Family
	readonly property string font_family: "Calibri"


	/// @brief Minimum width for popups (in pixels).
	/// It might avoid some "QWindowsWindow::setGeometry: Unable to set
	/// geometry" bugs and some error popups with update timers.
	readonly property int popup_min_width: 200

	/// @brief Program title
	readonly property string program_title: "Flickr Browser"

	// Color logging style
	readonly property color info_color: "steelblue"
	readonly property color warn_color: "goldenrod"
	readonly property color critical_color: "crimson"

	// TODO: debug color?
	function getLevaelColor(levael) {
		switch (levael) {
			case LogLevael.INFO:
				return info_color

			case LogLevael.WARN:
				return warn_color

			case LogLevael.CRITICAL:
				return critical_color

			default:
				return ''
		}
	}
}
