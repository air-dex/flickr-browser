import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.12
import FlickrBrowserCore 1.0

Item {
	id: backup_tab

	// Minimal sizes
	property int min_width:   2*constants.margin
							+ Math.max(
								albums_row.width,
								format_row.width,
								location_row.minimumWidth,
								name_row.minimumWidth,
								save_prompt.min_width
							  )
	property int min_height:   7*constants.margin
							 + albums_row.height
							 + format_row.height
							 + location_row.height
							 + name_row.height
							 + warning.contentHeight
							 + save_prompt.min_height

	Constants { id: constants }

	Component.onCompleted: {
		// Automatic extension removal for name.
		format_select.currentIndexChanged.connect(name_row.removeExtension)
		name_entry.textChanged.connect(name_row.removeExtension)
	}

	// Tab content
	Item {
		id: internals

		anchors {
			fill: parent
			margins: constants.margin
		}

		// Selecting album
		RowLayout {
			id: albums_row
			spacing: constants.margin

			anchors {
				top: parent.top
				left: parent.left
				right: parent.right
			}

			FlickrBrowserLabel {
				id: albums_legend
				text: qsTr("Albums :")
				text_style: FlickrBrowserLabel.TextStyle.SectionTitle
			}

			AlbumsMultiSelect {
				id: albums_select
				Layout.fillWidth: true
				onModelChanged: save_prompt.albums_ids = albums_select.getSelectedAlbums()
			}

			Button {
				id: select_all_albums
				text: qsTr("Tout sélectionner")
				onClicked: albums_select.selectAllAlbums()
			}

			Button {
				id: unselect_all_albums
				text: qsTr("Tout désélectionner")
				onClicked: albums_select.unselectAllAlbums()
			}
		}

		// Archive format select
		RowLayout {
			id: format_row
			spacing: constants.margin

			anchors {
				top: albums_row.bottom
				topMargin: constants.margin
				left: parent.left
			}

			FlickrBrowserLabel {
				id: format_legend
				text: qsTr("Sauvegarder au format :")
				text_style: FlickrBrowserLabel.TextStyle.SectionTitle
			}

			// Selecting format
			FlickrBrowserComboBox {
				id: format_select
				unselected_text: qsTr("Sélectionnez un format")
				Layout.preferredWidth: width

				ArchiveType { id: artype }

				model: [
					artype.toModel(ArchiveType.DIRECTORY),
					artype.toModel(ArchiveType.ZIP),
					artype.toModel(ArchiveType.TARBALL),
					artype.toModel(ArchiveType.TAR),
					artype.toModel(ArchiveType.SEPT_ZIP),
				]

				getModelText: function (i) {
					return format_select.model[i].text
				}

				function getArchiveType() {
					return format_select.model[currentIndex].id
				}

				function getArchiveText() {
					return format_select.model[currentIndex].text
				}

				function getArchiveFormat() {
					return artype.toArchiveFormat(format_select.model[currentIndex].id)
				}

				function isDirectorySelected() {
					return format_select.getArchiveType() === ArchiveType.DIRECTORY
				}
			}
		}

		// Output location
		RowLayout {
			id: location_row
			spacing: constants.margin

			anchors {
				top: format_row.bottom
				topMargin: constants.margin
				left: parent.left
				right: parent.right
			}

			FlickrBrowserLabel {
				id: location_legend
				text: qsTr("Emplacement de la sauvegarde :")
				text_style: FlickrBrowserLabel.TextStyle.SectionTitle
			}

			TextField {
				id: location_entry
				text: change_location_dialog.fileurlWithoutFileScheme()
				placeholderText: qsTr("Emplacement de la sauvegarde")
				Layout.fillWidth: true
			}

			Button {
				id: change_location_button
				text: qsTr("Changer")

				FlickrBrowserFileDialog {
					id: change_location_dialog
					title: qsTr("Choisir l'emplacement de la sauvegarde")
					selectFolder: true
					nameFilters: []
				}

				onClicked: {
					if (!change_location_dialog.visible) {
						change_location_dialog.open()
					}
				}
			}
		}

		// Output name
		RowLayout {
			id: name_row
			spacing: constants.margin

			anchors {
				top: location_row.bottom
				topMargin: constants.margin
				left: parent.left
				right: parent.right
			}

			FlickrBrowserLabel {
				id: name_legend
				text: qsTr("Nom de la sauvegarde :")
				text_style: FlickrBrowserLabel.TextStyle.SectionTitle
			}

			TextField {
				id: name_entry
				placeholderText: qsTr("Nom de la sauvegarde")
				Layout.fillWidth: true
			}

			FlickrBrowserLabel {
				id: extension_label
				text: ".%1".arg(format_select.getArchiveFormat())
				visible: !format_select.isDirectorySelected()
			}

			function getBackupName() {
				var name = name_entry.text

				if (!format_select.isDirectorySelected()) {
					name += extension_label.text
				}

				return name
			}

			function removeExtension() {
				if (format_select.isDirectorySelected()) {
					return
				}

				var ext = format_select.getArchiveFormat().replace('.', '\\.')
				var regex = new RegExp("^(.*)\\.%1$".arg(ext))
				var match = regex.exec(name_entry.text)

				if (match !== null) {
					name_entry.text = match[1]
				}
			}
		}

		// Warning
		FlickrBrowserLabel {
			id: warning
			text: qsTr("Attention d'avoir suffisamment d'espace mémoire ainsi que le droit de sauvegarder là où vous sauvegardez.
Tout fichier ayant le même nom qu'un élément de sauvegarde sera écrasé.")
			text_style: FlickrBrowserLabel.TextStyle.Paragraph
			font.bold: true
			color: "red"

			anchors {
				top: name_row.bottom
				topMargin: constants.margin
				left: parent.left
				right: parent.right
			}
		}

		// Displaying backup information and backup commands.
		SavePrompt {
			id: save_prompt

			anchors {
				top: warning.bottom
				topMargin: constants.margin
				left: parent.left
				right: parent.right
				bottom: parent.bottom
			}

			can_launch_save:    albums_select.hasSelectedAlbums
							 && location_entry.text !== ""
							 && name_entry.text !== ""

			albums_ids: albums_select.getSelectedAlbums()
			backup_location: location_entry.text
			backup_name: name_row.getBackupName()
			archive_type: format_select.getArchiveType()
		}
	}
}
