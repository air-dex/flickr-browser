import QtQuick 2.12
import QtQuick.Window 2.12

Window {
	visible: true

	width: 640
	height: 480
	minimumWidth: flickr_browser.min_width + 2*constants.margin
	minimumHeight: flickr_browser.min_height + 2*constants.margin

	onMinimumWidthChanged: {
		if (width < minimumWidth) {
			width = minimumWidth
		}
	}

	onMinimumHeightChanged: {
		if (height < minimumHeight) {
			height = minimumHeight
		}
	}

	title: constants.program_title

	Constants { id: constants }

	FlickrBrowser {
		id: flickr_browser
		anchors {
			fill: parent
			margins: constants.margin
		}
	}
}
