import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.12
import FlickrBrowserCore 1.0

ScrollView {
	id: save_prompt

	ScrollBar.horizontal.policy: ScrollBar.AsNeeded
	ScrollBar.vertical.policy: ScrollBar.AsNeeded

	// Min sizes
	property int min_width: Math.max(sp_rect.min_width, save_button_row.width)
	property int min_height: sp_rect.min_height + constants.margin + save_button_row.height

	Constants { id: constants }

	// Backups
	Rectangle {
		id: sp_rect

		anchors {
			top: parent.top
			left: parent.left
			right: parent.right
			bottom: save_button_row.top
			bottomMargin: constants.margin
		}

		border {
			color: constants.border_color
			width: constants.border_width
		}

		// Minimal sizes
		property int min_width: bmin_size_center.computeMinWidth()
		property int min_height: bmin_size_center.computeMinHeight()

		QtObject {
			id: bmin_size_center

			property var min_widths: ({})
			property var min_heights: ({})

			function  computeMinWidth() {
				var res = 0

				for (var guid in min_widths) {
					res = Math.max(res, min_widths[guid])
				}

				return res + 2*constants.margin
			}

			function computeMinHeight() {
				var res = 0

				for (var guid in min_widths) {
					res = Math.max(res, min_heights[guid])
				}

				return res + 2*constants.margin
			}

			function updateSizes(backupID, minW, minH) {
				min_widths[backupID] = minW
				min_heights[backupID] = minH
				sp_rect.min_width = computeMinWidth()
				sp_rect.min_height = computeMinHeight()
			}
		}

		// The Backups List
		ListView {
			id: backups
			clip: true
			spacing: constants.margin
			flickableDirection: Flickable.AutoFlickIfNeeded

			anchors {
				fill: parent
				margins: constants.margin
			}

			// Model & Delegate
			model: ListModel {/* Backups are reprensentated through their UUIDs:
				ListElement { backup_id: uuid.toString() }
			*/}

			function getBackupUUID(i) {
				return backups.model.get(i).backup_id
			}

			function forceRemove(i) {
				backups.model.remove(i)
				save_prompt.has_finished_backups = hasFinishedBackups()
			}

			delegate: BackupPrompt {
				uuid: backup_id
				width: parent.width

				onStahp: controller.stahpBackup(backup_id)
				onClear: controller.deleteBackupManager(backup_id)

				function killPrompt(uuid) {
					if (uuid.toString() === backup_id && controller.isBackupFinished(backup_id)) {
						backups.forceRemove(index)
					}
				}

				Component.onCompleted: {
					/*
					 * Retrieving backup information before connecting BackupPrompt
					 * and SavePrompt. The program may destroy the BackupPrompt,
					 * that might imply displaying wrong information about backup
					 * progress when the BackupPrompt is rebuilt by the program.
					 * So the BackupPrompt asks the state of the backup to
					 * the BackupManager (through the ArchiveController).
					 */

					// Backup state
					this.newState(backup_id, controller.getBackupState(backup_id))
					controller.newBackupState.connect(this.newState)

					// Copying progress
					this.updateProgress(backup_id, controller.getCopyingProgress(backup_id))
					controller.backupProgressing.connect(this.updateProgress)

					// Rollback progress
					this.updateRollback(backup_id, controller.getRollbackProgress(backup_id))
					controller.rollbackProgressing.connect(this.updateRollback)

					// Backup logs
					var logs = controller.getBackupLogs(backup_id)

					for (var i in logs) {
						this.addLog(backup_id, logs[i])
					}
					controller.newLog.connect(this.addLog)

					// Clear
					controller.backupCleared.connect(this.killPrompt)

					// Updating sizes
					bmin_size_center.updateSizes(backup_id, this.min_width, this.min_height)
				}

				Component.onDestruction: {
					controller.backupProgressing.disconnect(this.updateProgress)
					controller.newLog.disconnect(this.addLog)
					controller.rollbackProgressing.disconnect(this.updateRollback)
					controller.newBackupState.disconnect(this.newState)
					controller.backupCleared.disconnect(this.killPrompt)
					bmin_size_center.updateSizes(backup_id, 0, 0)
				}

				// Separators
				Rectangle {
					height: constants.border_width
					color: constants.border_color
					width: parent.width
					anchors {
						bottom: parent.bottom
						horizontalCenter: parent.horizontalCenter
					}

					visible: index !== backups.count -1
				}
			}
		}
	}

	// Save button
	RowLayout {
		id: save_button_row
		spacing: constants.margin

		anchors {
			right: parent.right
			bottom: parent.bottom
		}

		Button {
			id: save_button
			text: qsTr("Sauvegarder")
			enabled: can_launch_save

			onClicked: controller.saveAlbum(
				save_prompt.albums_ids,
				save_prompt.backup_location,
				save_prompt.backup_name,
				save_prompt.archive_type
			)
		}

		Button {
			id: clear_all_button
			text: qsTr("Retirer les sauvegardes terminées")
			enabled: save_prompt.has_finished_backups
			onClicked: {
				// Removing all finished backups from the list
				var i = 0

				while (i < backups.count) {
					var uuid = backups.getBackupUUID(i)
					if (controller.isBackupFinished(uuid)) {
						backups.forceRemove(i)
					}
					else {
						i++
					}
				}
			}
		}

		Button {
			id: stahp_all_button
			text: qsTr("Arrêter toutes les sauvegardes en cours")
			onClicked: {
				// Stahpping all backups that are not finished.
				for (var i = 0; i < backups.count; i++) {
					var uuid = backups.getBackupUUID(i)
					if (!controller.isBackupFinished(uuid)) {
						controller.stahpBackup(uuid)
					}
				}
			}
		}

		MessageDialog {
			id: backup_res_dialog
			title: constants.program_title
			visible: false

			icon: StandardIcon.Critical
			text: qsTr("Déroulement de la sauvegarde inconnu.")
			informativeText: ''

			function setInfo(text, info, uuid) {
				setAndOpen(StandardIcon.Information, text, info)
			}

			function setCritical(text, info, uuid) {
				setAndOpen(StandardIcon.Critical, text, info)
			}

			function setAndOpen(icon, text, info) {
				backup_res_dialog.icon = icon
				backup_res_dialog.text = text
				backup_res_dialog.informativeText = info
				backup_res_dialog.open()
			}
		}
	}

	// Backend handling
	ArchiveController {
		id: controller

		onNewBackupProcess: {
			// backupID: New backup's ID.
			backups.model.append({backup_id: backupID.toString()})
		}

		onNewBackupState: {
			// backupState : new backup state
			has_finished_backups = (backupState === BackupState.FINISH) ?
						true
					  : hasFinishedBackups()
		}
	}

	// Save entries
	property bool can_launch_save: false

	property var albums_ids: ([])
	property string backup_location: ""
	property string backup_name: ""
	property int archive_type: 0
	property bool has_finished_backups: false

	Component.onCompleted: {
		// Clear
		controller.cannotClearBackup.connect(backup_res_dialog.setCritical)

		// Backup results
		controller.backupOK.connect(backup_res_dialog.setInfo)
		controller.backupKO.connect(backup_res_dialog.setCritical)
	}

	function hasFinishedBackups() {
		for (var i = 0; i < backups.count; i++) {
			if (controller.isBackupFinished(backups.getBackupUUID(i))) {
				return true
			}
		}

		return false
	}
}
