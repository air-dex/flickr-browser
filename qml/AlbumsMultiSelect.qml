import QtQuick 2.12
import QtQuick.Controls 2.12
import FlickrBrowserCore 1.0

// Adapted from: https://forum.qt.io/topic/87306/multiselect-combobox/2
ComboBox {
	id: albums_select
	width: popup.width
	displayText: qsTr("Sélectionnez vos albums")

	// Have to put the model in a separate variable in order to be able to modify it.
	property var albums: helper.getAlbumSelectModel()
	model: albums

	FlickrBrowserHelper { id: helper }

	/*
	 * Multiple delegate
	 *
	 * ComboBox closes the popup when its items (anything AbstractButton
	 * derivative) are activated. Wrapping the delegate into a plain Item
	 * prevents that.
	 */
	delegate: Item {
		width: parent.width
		height: checkDelegate.height

		function toggle() { checkDelegate.toggle() }

		CheckDelegate {
			id: checkDelegate
			anchors.fill: parent
			text: albums_select.model[index].title
			highlighted: albums_select.highlightedIndex === index
			checked: albums_select.model[index].selected
			onCheckedChanged: {
				albums[index].selected = checkDelegate.checked
				hasSelectedAlbums = checkDelegate.checked || hasSomeSelectedAlbums()
				albums_select.model = albums
			}
		}
	}

	// Override space key handling to toggle items when the popup is visible
	Keys.onSpacePressed: {
		if (albums_select.popup.visible) {
			var currentItem = albums_select.popup.contentItem.currentItem
			if (currentItem) {
				currentItem.toggle()
				event.accepted = true
			}
		}
	}

	Keys.onReleased: {
		if (albums_select.popup.visible)
			event.accepted = (event.key === Qt.Key_Space)
	}

	// Selected albums management
	property bool hasSelectedAlbums: false

	function getSelectedAlbums() {
		var res = []

		albums.forEach(function (album) {
			if (album.selected) {
				res.push(album.id)
			}
		})

		return res
	}

	function hasSomeSelectedAlbums() {
		return albums.some(function (album) {
			return album.selected
		})
	}

	function selectAllAlbums() {
		albums.forEach(function (album) {
			album.selected = true
		})

		albums_select.model = albums
		hasSelectedAlbums = true
	}

	function unselectAllAlbums() {
		albums.forEach(function (album) {
			album.selected = false
		})

		albums_select.model = albums
		hasSelectedAlbums = false
	}

	Component.onCompleted: hasSelectedAlbums = hasSomeSelectedAlbums()
}
