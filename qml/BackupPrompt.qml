import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import FlickrBrowserCore 1.0

Item {
	id: backup_prompt

	property bool details_visible: showhide_progress.checked

	// Sizes
	property int min_width: Math.max(
		legend_row.min_width,
		state_of_backup.min_width
	)
	property int min_height:   3*constants.margin
							 + legend_row.height
							 + state_of_backup.min_height

	height:   legend_row.height
			+ state_of_backup.height
			+ 3*constants.margin
			+ showhide_progress.checked *
				(log_view.contentHeight + 2*constants.margin)

	Constants { id: constants }
	FlickrBrowserHelper { id: helper }

	// Legend row
	RowLayout {
		id: legend_row
		spacing: constants.margin

		property int min_width: constants.margin
								+ showhide_progress.implicitWidth
								+ stahp_button.visible * stahp_button.implicitWidth
								+ clear_button.visible * clear_button.implicitWidth

		anchors {
			top: backup_prompt.top
			left: backup_prompt.left
			right: backup_prompt.right
		}

		FlickrBrowserLabel {
			id: backup_legend
			text: qsTr("Sauvegarde %1").arg(backup_name)
			text_style: FlickrBrowserLabel.TextStyle.SectionTitle
			Layout.fillWidth: true
		}

		Switch {
			id: showhide_progress
			text: qsTr("Détails de la sauvegarde")
			checked: false
			onClicked: details_visible = !details_visible
		}

		Button {
			id: stahp_button
			text: qsTr("Arrêter")
			onClicked: stahp()
		}

		Button {
			id: clear_button
			text: qsTr("Supprimer")
			onClicked: clear()
		}
	}

	Item {
		id: state_of_backup

		// Sizes
		property int min_width: Math.max(
			rollback_prompt.min_width,
			2*constants.margin + album_prompt.min_width + photo_prompt.min_width,
			infinite_bar.min_width,
			conclusion.implicitWidth
		)
		property int min_height: Math.max(
			rollback_prompt.height,
			album_prompt.height,
			infinite_bar.height,
			conclusion.implicitHeight
		)

		height: Math.max(
			rollback_prompt.height,
			album_prompt.height,
			infinite_bar.height,
			conclusion.height
		)

		anchors {
			top: legend_row.bottom
			topMargin: 2*constants.margin
			left: backup_prompt.left
			right: backup_prompt.right
		}

		// Progress bars
		ProgressPrompt {
			id: album_prompt
			label: qsTr("Album")

			anchors {
				top: state_of_backup.top
				left: state_of_backup.left
				right: state_of_backup.horizontalCenter
				rightMargin: constants.margin
			}
		}

		ProgressPrompt {
			id: photo_prompt
			label: qsTr("Photo")

			anchors {
				top: album_prompt.top
				left: state_of_backup.horizontalCenter
				leftMargin: constants.margin
				right: state_of_backup.right
				bottom: album_prompt.bottom
			}
		}

		ProgressPrompt {
			id: rollback_prompt

			anchors {
				top: state_of_backup.top
				left: state_of_backup.left
				right: state_of_backup.right
			}
			label: qsTr("Élément à effacer")
		}

		IndefiniteProgress {
			id: infinite_bar

			anchors {
				top: state_of_backup.top
				left: state_of_backup.left
				right: state_of_backup.right
			}
		}

		FlickrBrowserLabel {
			id: conclusion

			anchors {
				top: state_of_backup.top
				left: state_of_backup.left
				right: state_of_backup.right
			}
		}
	}

	// Logs
	ListView {
		id: log_view
		visible: showhide_progress.checked
		clip: true

		anchors {
			top: state_of_backup.bottom
			topMargin: 2*constants.margin
			left: backup_prompt.left
			leftMargin: constants.margin
			right: backup_prompt.right
			rightMargin: constants.margin
			bottom: backup_prompt.bottom
			bottomMargin: constants.margin
		}

		model: ListModel {/* Logs are reprensentated like this:
			ListElement {
				levael: LogLevael.INFO|LogLevael.WARN|LogLevael.CRITICAL
				log_text: "Log text about what happens."
			}
		*/}

		delegate: FlickrBrowserLabel {
			text: log_text
			text_style: FlickrBrowserLabel.TextStyle.Paragraph
			color: constants.getLevaelColor(levael)
			width: parent.width
		}
	}

	// Backup states
	BackupState {
		id: bstate
		readonly property string begin: stringState(BackupState.BEGIN)
		readonly property string copy: stringState(BackupState.COPY)
		readonly property string closing: stringState(BackupState.CLOSING)
		readonly property string finish: stringState(BackupState.FINISH)
		readonly property string rollback: stringState(BackupState.ROLLBACK)
		readonly property string fail: stringState(BackupState.FAIL)
		readonly property string stahp: stringState(BackupState.STAHP)
		readonly property string nothing: stringState(BackupState.NOTHING)
	}

	states: [
		// Default state.
		State {
			name: ""
			PropertyChanges {
				target: album_prompt
				visible: false
			}
			PropertyChanges {
				target: photo_prompt
				visible: false
			}
			PropertyChanges {
				target: rollback_prompt
				visible: false
			}
			PropertyChanges {
				target: infinite_bar
				visible: true
				label: qsTr("En attente d'infos quant à la sauvegarde...")
			}
			PropertyChanges {
				target: conclusion
				visible: false
				text: qsTr("En attente d'infos quant à la sauvegarde...")
			}
			PropertyChanges {
				target: stahp_button
				visible: true
			}
			PropertyChanges {
				target: clear_button
				visible: true
			}
		},

		// Begin: init the backup, before copying something.
		State {
			name: bstate.begin
			PropertyChanges {
				target: album_prompt
				visible: false
			}
			PropertyChanges {
				target: photo_prompt
				visible: false
			}
			PropertyChanges {
				target: rollback_prompt
				visible: false
			}
			PropertyChanges {
				target: infinite_bar
				visible: true
				label: qsTr("Début de la sauvegarde")
			}
			PropertyChanges {
				target: conclusion
				visible: false
			}
			PropertyChanges {
				target: stahp_button
				visible: true
			}
			PropertyChanges {
				target: clear_button
				visible: false
			}
		},

		// Copy: when albums and photos are added to the backup support.
		State {
			name: bstate.copy
			PropertyChanges {
				target: album_prompt
				visible: true
			}
			PropertyChanges {
				target: photo_prompt
				visible: true
			}
			PropertyChanges {
				target: rollback_prompt
				visible: false
			}
			PropertyChanges {
				target: infinite_bar
				visible: false
			}
			PropertyChanges {
				target: conclusion
				visible: false
			}
			PropertyChanges {
				target: stahp_button
				visible: true
			}
			PropertyChanges {
				target: clear_button
				visible: false
			}
		},

		// Closing: ending backup, after albums & photos are backuped.
		State {
			name: bstate.closing
			PropertyChanges {
				target: album_prompt
				visible: false
			}
			PropertyChanges {
				target: photo_prompt
				visible: false
			}
			PropertyChanges {
				target: rollback_prompt
				visible: false
			}
			PropertyChanges {
				target: infinite_bar
				visible: true
				label: qsTr("Clôture de la sauvegarde")
			}
			PropertyChanges {
				target: conclusion
				visible: false
			}
			PropertyChanges {
				target: stahp_button
				visible: true
			}
			PropertyChanges {
				target: clear_button
				visible: false
			}
		},

		// Finish: the backup is over
		State {
			name: bstate.finish
			PropertyChanges {
				target: album_prompt
				visible: false
			}
			PropertyChanges {
				target: photo_prompt
				visible: false
			}
			PropertyChanges {
				target: rollback_prompt
				visible: false
			}
			PropertyChanges {
				target: infinite_bar
				visible: false
			}
			PropertyChanges {
				target: conclusion
				visible: true
				text: qsTr("Sauvegarde terminée. Voir les détails pour plus d'informations sur son déroulement.")
			}
			PropertyChanges {
				target: stahp_button
				visible: false
			}
			PropertyChanges {
				target: clear_button
				visible: true
			}
		},

		// Rollback: backup is rollbacked (failed backup or stahp)
		State {
			name: bstate.rollback
			PropertyChanges {
				target: album_prompt
				visible: false
			}
			PropertyChanges {
				target: photo_prompt
				visible: false
			}
			PropertyChanges {
				target: rollback_prompt
				visible: true
			}
			PropertyChanges {
				target: infinite_bar
				visible: false
			}
			PropertyChanges {
				target: conclusion
				visible: false
			}
			PropertyChanges {
				target: stahp_button
				visible: false
			}
			PropertyChanges {
				target: clear_button
				visible: false
			}
		},

		// Fail: Backup has failed.
		State {
			name: bstate.fail
			PropertyChanges {
				target: album_prompt
				visible: false
			}
			PropertyChanges {
				target: photo_prompt
				visible: false
			}
			PropertyChanges {
				target: rollback_prompt
				visible: false
			}
			PropertyChanges {
				target: infinite_bar
				visible: true
				label: qsTr("Sauvegarde échouée. Annulation en cours...")
			}
			PropertyChanges {
				target: conclusion
				visible: false
			}
			PropertyChanges {
				target: stahp_button
				visible: false
			}
			PropertyChanges {
				target: clear_button
				visible: false
			}
		},

		// Stahp: backup was interrupted.
		State {
			name: bstate.stahp
			PropertyChanges {
				target: album_prompt
				visible: false
			}
			PropertyChanges {
				target: photo_prompt
				visible: false
			}
			PropertyChanges {
				target: rollback_prompt
				visible: false
			}
			PropertyChanges {
				target: infinite_bar
				visible: true
				label: qsTr("Sauvegarde interompue. Annulation en cours...")
			}
			PropertyChanges {
				target: conclusion
				visible: false
			}
			PropertyChanges {
				target: stahp_button
				visible: false
			}
			PropertyChanges {
				target: clear_button
				visible: false
			}
		},

		// Redirects nohing to default state (invalid).
		State {
			name: bstate.nothing
			StateChangeScript {
				name: "nothing2DefaultState"
				script: backup_prompt.state = ""
			}
		}
	]

	// Backend handling
	property string uuid: ""
	property string backup_name: helper.getBackupNameFromUUID(uuid)

	signal stahp
	signal clear

	function newState(uuid, brandNewState) {
		if (uuid.toString() !== backup_prompt.uuid) {
			return
		}

		backup_prompt.state = bstate.stringState(brandNewState)
	}

	function updateProgress(uuid, progress) {
		if (uuid.toString() !== backup_prompt.uuid) {
			return
		}

		album_prompt.updateProgress(progress.albums)
		photo_prompt.updateProgress(progress.photos)
	}

	function updateRollback(uuid, progress) {
		if (uuid.toString() !== backup_prompt.uuid.toString()) {
			return
		}

		rollback_prompt.updateProgress(progress)
	}

	function addLog(uuid, log) {
		if (uuid.toString() !== backup_prompt.uuid) {
			return
		}

		log_view.model.append(log)
	}
}
