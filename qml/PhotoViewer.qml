import QtQuick 2.12
import QtQuick.Controls 2.12

// TODO: add key navigation : left arrow prev photo & right arrow next photo
Item {
	id: photo_viewer

	width: Math.max(prev_button.width + next_button.width + photo_display.width, indicator.width)

	// Minimal sizes
	property int min_width: Math.max(
								prev_button.implicitWidth + next_button.implicitWidth,
								indicator.width
							)
	property int min_height: prev_button.implicitHeight
							 + indicator.height
							 + constants.margin

	/// @brief String list of photo IDs to display
	property var photoIDs
	property int currentIndex: 0

	function getCurrentPhotoID() {
		return photoIDs[photo_viewer.currentIndex]
	}

	/// @brief Getting a positive result for modulo.
	function computeModulo(nb) {
		if (nb < 0) {
			return (Math.abs(nb)*(photoIDs.length -1)) % photoIDs.length
		}
		else {
			return nb % photoIDs.length
		}
	}

	Constants { id: constants }

	// Previous Button
	Button {
		id: prev_button
		flat: true

		// TODO: une icône à la place
		text: "Prev"

		anchors {
			top: photo_viewer.top
			left: photo_viewer.left
			bottom: photo_display.bottom
		}

		// Previous photo in the album.
		// Going to the end of the album if it is the first photo.
		onClicked: currentIndex = computeModulo(currentIndex -1)
	}

	// Next Button
	Button {
		id: next_button
		width: prev_button.width
		flat: true

		// TODO: une icône à la place
		text: "Next"

		anchors {
			top: photo_viewer.top
			right: photo_viewer.right
			bottom: photo_display.bottom
		}

		// Next photo in the album.
		// Going back to the beginning if it is the last photo.
		onClicked: currentIndex = computeModulo(currentIndex +1)
	}

	// The photo
	FlickrBrowserImage {
		id: photo_display
		photoID: getCurrentPhotoID()

		height: photo_viewer.height - indicator.implicitHeight - constants.margin

		anchors {
			top: photo_viewer.top
			horizontalCenter: photo_viewer.horizontalCenter
		}
	}

	// Indicator
	FlickrBrowserLabel {
		id: indicator
		text: qsTr("Photo %1/%2")
				.arg((currentIndex +1).toString())
				.arg(photoIDs.length.toString())

		anchors {
			horizontalCenter: photo_viewer.horizontalCenter
			bottom: photo_viewer.bottom
		}

		horizontalAlignment: Text.AlignHCenter
	}
}
