import QtQuick 2.12
import QtQuick.Controls 2.12

/// @brief Base component for combo box in this program.
ComboBox {
	id: fb_select

	/// @brief Function to access text to display depending on the index.
	property var getModelText: function (i) { return "" }

	property string unselected_text: ""

	displayText: currentIndex < 0 ?
					 unselected_text
				   : getModelText(currentIndex)

	delegate: ItemDelegate {
		text: getModelText(index)
		highlighted: fb_select.highlightIndex === index
	}

	// Enough width to fit the largest album name
	// Adapted from: https://stackoverflow.com/questions/45029968/how-do-i-set-the-combobox-width-to-fit-the-largest-item
	property int modelWidth: 0
	width: modelWidth + 2*leftPadding + 2*rightPadding

	TextMetrics { id: text_metrics }

	onModelChanged: {
		// Computing modelWidth again.
		modelWidth = 0
		text_metrics.font = fb_select.font
		for(var i = 0; i < model.length; i++) {
			text_metrics.text = getModelText(i)
			modelWidth = Math.max(text_metrics.width, modelWidth)
		}
	}
}
