import QtQuick 2.12
import FlickrBrowserCore 1.0

/// @brief File Dialog for saving images.
FlickrBrowserFileDialog {
	id: save_dialog

	// Internals. Have to nest it in a QML for God knows why.
	Item {
		id: sid_internals
		PhotoController { id: controller }
		SaveImageKOMessageBox {
			id: save_ko
			namefile: save_dialog.fileUrl
		}
	}

	title: qsTr("Sauver l'image %1").arg(photo.name)

	// Save file dialog
	selectFolder: false

	/*
	 * WARNING: A "QWindowsNativeFileDialogBase::selectNameFilter: Invalid
	 * parameter not found in" warning will appear when the file extension
	 * changes.
	 */
	nameFilters: [
		qsTr("Fichiers %1 (*.%2)").arg(photo_ext.toUpperCase()).arg(photo_ext),
		qsTr("Fichiers image (*.%1)").arg(photo_ext),
		qsTr("Tous les fichiers (*)")
	]

	// TODO: photo.name or photo.filename as namefile placeholder.

	// Saving photo
	/// @brief Saves after accepting by default.
	property bool save_photo: true

	onSave_photoChanged: switchPhotoChanged()

	function switchPhotoChanged() {
		if (save_photo) {
			accepted.connect(save)
		}
		else {
			accepted.disconnect(save)
		}
	}

	function save() {
		if (!controller.savePhoto(fileUrl)) {
			save_ko.open()
		}
	}

	// Binding for accepted()
	Component.onCompleted: switchPhotoChanged()

	// Photo management
	property string photo_id
	property alias photo: controller.photo
	property string photo_ext: photo.getFileExtension()

	onPhoto_idChanged: controller.setPhoto(photo_id)
}
