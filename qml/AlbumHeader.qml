import QtQuick 2.12

Item {
	id: album_header

	height: title.height + stats.height + description.height + 4*constants.margin

	// Minimal sizes
	property int min_width: title.implicitWidth
	property int min_height: title.implicitHeight
							 + stats.implicitHeight
							 + description.implicitHeight
							 + 4*constants.margin

	/// @brief Underlying album. Its type is FlickrBrowserCore.Album
	default property var album

	Constants { id: constants }

	// Title
	// FIXME: titre qui dépasse du cadre si trop long et fenêtre trop petite. Lié aux minimal sizes?
	FlickrBrowserLabel {
		id: title
		text_style: FlickrBrowserLabel.TextStyle.Title

		text: album.title

		anchors {
			top: album_header.top
			horizontalCenter: album_header.horizontalCenter
		}

		font.pointSize: constants.title_text_size
	}

	// Stats
	FlickrBrowserLabel {
		id: stats
		text_style: FlickrBrowserLabel.TextStyle.Paragraph

		// FIXME: régler le cas du pluriel
		text: qsTr("%L1 photos. Créé le %2 à %3, mis à jour le %4 à %5.")
				.arg(album.photo_count)
				.arg(album.created_at.toLocaleDateString())
				.arg(album.created_at.toLocaleTimeString())
				.arg(album.last_updated_at.toLocaleDateString())
				.arg(album.last_updated_at.toLocaleTimeString())

		anchors {
			top: title.bottom
			topMargin: 2*constants.margin
			left: album_header.left
			right: album_header.right
		}

		font {
			pointSize: constants.small_text_size
			italic: true
		}
	}

	// Description
	FlickrBrowserLabel {
		id: description
		text_style: FlickrBrowserLabel.TextStyle.Paragraph

		// FIXME: remplacer les caractères HTML genre &quot;
		text: album.description

		anchors {
			top: stats.bottom
			topMargin: 2*constants.margin
			left: album_header.left
			right: album_header.right
		}
	}
}
