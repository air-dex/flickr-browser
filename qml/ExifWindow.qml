import QtQuick 2.12
import QtQuick.Window 2.12
import FlickrBrowserCore 1.0

Window {
	id: exif_window
	title: qsTr("Données EXIF de %1").arg(photo.name)
	modality: Qt.NonModal

	Constants { id: constants }

	// Fixed width
	property int ew_width: exif_table.contentWidth + 2*constants.margin
	minimumWidth: Math.max(constants.popup_min_width, ew_width)
	width: Math.max(constants.popup_min_width, ew_width)
	maximumWidth: ew_width

	property int ew_height: exif_table.contentHeight + 2*constants.margin
	minimumHeight: 2*constants.margin
	height: Math.min(ew_height, Screen.desktopAvailableHeight)
	maximumHeight: Screen.desktopAvailableHeight

	// Photo management
	property string photo_id: ""
	property alias photo: controller.photo

	PhotoController { id: controller }

	onPhoto_idChanged: controller.setPhoto(photo_id)

	// EXIF table
	ExifDatasView {
		id: exif_table
		exifDatas: photo.exif

		anchors {
			fill: parent
			margins: constants.margin
		}
	}
}
