import QtQuick 2.12
import FlickrBrowserCore 1.0

// EXIF table
// TODO: use a Qt Quick Control 2.x instead of a Qt Quick when possible.
TableView {
	id: exif_table

	property var exifDatas: ({})

	Constants { id: constants }

	model: ExifTableModel {
		exifDatas: exif_table.exifDatas
	}

	delegate: FlickrBrowserLabel {
		id: cell
		text: display
		text_style: FlickrBrowserLabel.TextStyle.FuMaStyle

		font.bold: column === 0
		horizontalAlignment: Text.AlignLeft
		wrapMode: Text.NoWrap
		elide: Text.ElideNone
	}

	rowSpacing: constants.margin
	columnSpacing: constants.margin

	// Compute column width
	TextMetrics {
		id: tm

		// Same as delegate
		font {
			family: constants.font_family
			pointSize: constants.medium_text_size
		}
	}

	function computeStringListWidth(stringList, bold) {
		var cellWidth = 0

		tm.font.bold = bold

		stringList.forEach(function (val) {
			tm.text = val
			cellWidth = Math.max(cellWidth, tm.width)
		})

		return cellWidth
	}

	columnWidthProvider: function (column) {
		switch (column) {
			case 0:
				return computeStringListWidth(model.getExifTags(), true)
			case 1:
				return computeStringListWidth(model.getExifValues(), false)
			default:
				return 0
		}
	}

	rowHeightProvider: function (row) {
		var texts = model.getExifRow(row)

		if (texts.length < 2) {
			return 0
		}

		var rowHeight = 0

		tm.font.bold = true
		tm.text = texts[0]
		rowHeight = tm.height
		tm.font.bold = false
		tm.text = texts[1]

		return Math.max(rowHeight, tm.height)
	}
}
