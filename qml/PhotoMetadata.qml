import QtQuick 2.12
import QtQuick.Controls 2.12
import FlickrBrowserCore 1.0

/// @brief Show Photo data
Item {
	id: photo_metadata

	height: title_part.height
			+ details_visible * (
				legend.height
				+ Math.max(dates.height, exif_button.height)
				+ 2*constants.margin
			  )

	// Minimal sizes
	property int min_width: Math.max(title_part.min_width, details_visible * exif_button.width)
	property int min_height: title_part.min_height
							 + legend.implicitHeight
							 + Math.max(
								 dates.implicitHeight,
								 exif_button.implicitHeight
								)
							 + 2*constants.margin

	Constants { id: constants }

	/// @brief Details visibility. Defaults to true
	property bool details_visible: true

	// Photo management
	PhotoController { id: controller }

	property string photo_id: ""
	property alias photo: controller.photo

	onPhoto_idChanged: controller.setPhoto(photo_id)

	// Details header
	Item {
		id: title_part

		height: Math.max(title.height, showhide_button.height)

		// Minimal sizes
		property int min_width: showhide_button.width
		property int min_height: Math.max(
									 title.implicitHeight,
									 showhide_button.implicitHeight
									)

		anchors {
			top: photo_metadata.top
			left: photo_metadata.left
			right: photo_metadata.right
		}

		// Photo title
		FlickrBrowserLabel {
			id: title
			text_style: FlickrBrowserLabel.TextStyle.SectionTitle
			text: photo.name

			anchors {
				top: title_part.top
				left: title_part.left
				right: showhide_button.left
				rightMargin: constants.margin
				bottom: title_part.bottom
			}
		}

		// Toggle details visibility
		// TODO: replace it with a switch?
		Button {
			id: showhide_button
			text: qsTr("Voir/cacher les détails")
			width: showhide_button.implicitWidth

			// TODO: add an icon

			anchors {
				top: title_part.top
				right: title_part.right
				bottom: title_part.bottom
			}

			states: [
				// Show details
				State {
					name: "show details"
					when: !photo_metadata.details_visible

					PropertyChanges {
						target: showhide_button
						text: qsTr("Voir les détails")
					}
				},

				// Hide details
				State {
					name: "hide details"
					when: photo_metadata.details_visible

					PropertyChanges {
						target: showhide_button
						text: qsTr("Cacher les détails")
					}
				}
			]

			// Toggle details visibility
			onClicked: details_visible = !details_visible
		}
	}

	// Legend
	FlickrBrowserLabel {
		id: legend
		text_style: FlickrBrowserLabel.TextStyle.Paragraph
		text: photo.description
		visible: details_visible

		anchors {
			top: title_part.bottom
			topMargin: constants.margin
			left: photo_metadata.left
			right: photo_metadata.right
		}
	}

	// Dates
	FlickrBrowserLabel {
		id: dates
		text_style: FlickrBrowserLabel.TextStyle.ElidableParagraph
		text: qsTr("Prise le %1 à %2, importée le %3 à %4.")
				.arg(photo.taken_at.toLocaleDateString())
				.arg(photo.taken_at.toLocaleTimeString())
				.arg(photo.imported_at.toLocaleDateString())
				.arg(photo.imported_at.toLocaleTimeString())
		visible: details_visible

		anchors {
			top: legend.bottom
			topMargin: constants.margin
			left: photo_metadata.left
			right: exif_button.left
			rightMargin: constants.margin
		}

		font {
			pointSize: constants.small_text_size
			italic: true
		}
	}

	// EXIF button
	Button {
		id: exif_button
		text: qsTr("EXIF")
		visible: details_visible

		anchors {
			right: photo_metadata.right
			bottom: photo_metadata.bottom
		}

		ExifWindow {
			id: exif_dialog
			photo_id: photo_metadata.photo_id
			visible: false
		}

		// Show EXIF datas
		onClicked: {
			if (!exif_dialog.visible) {
				exif_dialog.show()
			}
		}
	}
}
