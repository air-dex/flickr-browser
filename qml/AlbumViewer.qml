import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import FlickrBrowserCore 1.0

Item {
	id: album_viewer

	// Minimal sizes
	property int min_width: Math.max(tabs_bar.implicitWidth, tabs.min_width)
	property int min_height: tabs_bar.height + tabs.min_height

	// Backend
	Constants { id: constants }
	AlbumController { id: controller }

	/// @brief ID of the album displayed by the viewer
	property string album_id: ""

	readonly property alias album: controller.album

	// Changing the underlying album
	onAlbum_idChanged: controller.setAlbum(album_id)

	TabBar {
		id: tabs_bar

		anchors {
			top: album_viewer.top
			left: album_viewer.left
			right: album_viewer.right
		}

		TabButton {
			text: qsTr("Description")
		}

		TabButton {
			text: qsTr("Photos")
		}
	}

	StackLayout {
		id: tabs
		currentIndex: tabs_bar.currentIndex

		// Minimal sizes
		property int min_width: Math.max(header.min_width, photos.min_width)
		property int min_height: Math.max(header.min_height, photos.min_height)

		anchors {
			top: tabs_bar.bottom
			left: album_viewer.left
			right: album_viewer.right
			bottom: album_viewer.bottom
		}

		// Album description
		// TODO: le composant est-il pertinent ? Le conserver ou pas ?
		AlbumHeader {
			id: header
			album: album_viewer.album
			Layout.fillWidth: true
			Layout.fillHeight: true
		}

		// Display photos
		AlbumRoll {
			id: photos
			roll: album.photo_ids
			Layout.fillWidth: true
			Layout.fillHeight: true
		}
	}

	function getCurrentPhotoID() {
		return photos.getCurrentPhotoID()
	}

	// Show photos at startup
	Component.onCompleted: {
		tabs_bar.currentIndex = 1
	}
}
