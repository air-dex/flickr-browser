import QtQuick 2.12

Item {
	id: album_roll

	/// @brief List of IDs of photo which are in the roll
	property var roll

	// Going back at the beginning of the roll.
	// TODO: structure pour stocker l'index et revenir au même endroit.
	onRollChanged: photo_viewer.currentIndex = 0

	// Minimal sizes
	property int min_width: Math.max(
								photo_viewer.min_width,
								current_photo_metadata.min_width
							)
	property int min_height: photo_viewer.min_height
							 + current_photo_metadata.min_height
							 + 4*constants.margin

	// Photo Viewer
	PhotoViewer {
		id: photo_viewer
		photoIDs: roll

		anchors {
			horizontalCenter: album_roll.horizontalCenter
			top: album_roll.top
			topMargin: constants.margin
			bottom: current_photo_metadata.top
			bottomMargin: 2*constants.margin
		}
	}

	// Photo metadata
	PhotoMetadata {
		id: current_photo_metadata
		photo_id: photo_viewer.getCurrentPhotoID()

		anchors {
			left: album_roll.left
			right: album_roll.right
			bottom: album_roll.bottom
			bottomMargin: constants.margin
		}
	}

	function getCurrentPhotoID() {
		return photo_viewer.getCurrentPhotoID()
	}
}
