import QtQuick 2.12
import FlickrBrowserCore 1.0

// TODO: considering photograph's biography, with a real fake album (author details in "account_profile.json")?
Item {
	// For program constants
	Constants { id: constants }

	// Back office
	AlbumController {
		id: controller

		// Set camera roll
		Component.onCompleted: controller.setAlbum(controller.album.ROLL_ID)
	}

	// Minimum sizes
	property int min_width: 2*constants.margin
							+ Math.max(
								camera_roll.min_width,
								bottom_buttons.width
							  )
	property int min_height: 3*constants.margin
							 + camera_roll.min_height
							 + bottom_buttons.height

	// TODO: compute width
	height: 3*constants.margin
			+ camera_roll.height
			+ bottom_buttons.height

	// Tab content
	Item {
		id: camera_roll_tab

		anchors {
			fill: parent
			margins: constants.margin
		}

		// Album roll
		AlbumRoll {
			id: camera_roll
			// TODO: sorting pictures by date?
			roll: controller.album.photo_ids

			anchors {
				top: camera_roll_tab.top
				left: camera_roll_tab.left
				right: camera_roll_tab.right
				bottom: bottom_buttons.top
				bottomMargin: constants.margin
			}
		}

		// Album button row
		AlbumButtonRow {
			id: bottom_buttons
			show_download: false
			show_fullscreen: true
			album_id: controller.album.album_id
			photo_id: camera_roll.getCurrentPhotoID()

			anchors {
				right: camera_roll_tab.right
				bottom: camera_roll_tab.bottom
			}
		}
	}
}
