import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Item {
	id: flickr_browser

	Constants { id: constants }

	// Minimal sizes
	property int min_width: Math.max(
								title.width,
								tabs.min_width,
								bottom_buttons.width
							)
	property int min_height: title.height
							 + slogan.height
							 + tabs.min_height
							 + bottom_buttons.height
							 + 12*constants.margin

	// Title
	FlickrBrowserLabel {
		id: title
		text_style: FlickrBrowserLabel.TextStyle.Title
		text: constants.program_title

		anchors {
			horizontalCenter: flickr_browser.horizontalCenter
			top: flickr_browser.top
		}

		font.pointSize: constants.program_name_text_size
	}

	// Slogan
	FlickrBrowserLabel {
		id: slogan
		text_style: FlickrBrowserLabel.TextStyle.Paragraph

		// TODO: improve slogan
		text: qsTr("Consulter ses données personnelles Flickr.")

		anchors {
			top: title.bottom
			topMargin: 2*constants.margin
			left: flickr_browser.left
			right: flickr_browser.right
		}
	}

	// App tabs
	FlickrBrowserTabs {
		id: tabs

		anchors {
			top: slogan.bottom
			topMargin: 2*constants.margin
			left: flickr_browser.left
			right: flickr_browser.right
			bottom: bottom_buttons.top
			bottomMargin: 2*constants.margin
		}
	}

	// Bottom buttons row
	RowLayout {
		id: bottom_buttons

		spacing: constants.margin

		anchors {
			right: flickr_browser.right
			bottom: flickr_browser.bottom
		}

		// Program configuration
		// TODO: options du programme.
		Button {
			text: qsTr("Options")
			onClicked: console.log("TODO: options du programme.")
		}

		// About the program. And about Qt too?
		// TODO: à propos du programme. Parler de Qt ?
		Button {
			text: qsTr("À propos de %1").arg(constants.program_title)
			onClicked: console.log("TODO: à propos du programme. Parler de Qt ?")
		}

		// Quit Flickr Browser
		Button {
			text: qsTr("Quitter")

			// TODO: warning if it is processing.
			onClicked: Qt.quit()
		}
	}
}
