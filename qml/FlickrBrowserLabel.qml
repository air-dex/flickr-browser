import QtQuick 2.12
import QtQuick.Controls 2.12

// TODO: improve description
/// @brief Custom Label QML Widget for Flickr Browser.
/// It handles styling, enabling developers to focus on the text to display,
/// the positioning, and sometimes style options to override. parameters while
/// using it. However it is still possible to fine-tune the label style.
Label {
	id: fb_label

	Constants { id: constants }

	horizontalAlignment: Text.AlignLeft
	verticalAlignment: Text.AlignVCenter

	font {
		family: constants.font_family
		pointSize: constants.medium_text_size
	}

	/// @brief Text Styles
	enum TextStyle {
		/// @brief For Full Manual Style. You get full control on style with it.
		FuMaStyle,
		/// @brief For important titles
		Title,
		/// @brief For less important titles.
		SectionTitle,
		/// @brief For paragraphs.
		Paragraph,
		/// @brief For paragraphs that can be elidable.
		ElidableParagraph
	}

	/// @brief Determining text style. FuMa by default.
	property int text_style: FlickrBrowserLabel.TextStyle.FuMaStyle

	// One state == 1 one custon style
	states: [
		State {
			name: ""
			when: text_style === FlickrBrowserLabel.TextStyle.FuMaStyle
		},

		State {
			name: "title"
			when: text_style === FlickrBrowserLabel.TextStyle.Title

			PropertyChanges {
				target: fb_label
				horizontalAlignment: Text.AlignHCenter
				font.bold: true
			}
		},

		State {
			name: "section title"
			when: text_style === FlickrBrowserLabel.TextStyle.SectionTitle

			PropertyChanges {
				target: fb_label
				horizontalAlignment: Text.AlignLeft
				elide: Text.ElideRight
				font.bold: true
			}
		},

		State {
			name: "§"
			when: text_style === FlickrBrowserLabel.TextStyle.Paragraph

			PropertyChanges {
				target: fb_label
				horizontalAlignment: Text.AlignJustify
				wrapMode: Text.WrapAtWordBoundaryOrAnywhere
			}
		},

		State {
			name: "elidable §"
			when: text_style === FlickrBrowserLabel.TextStyle.ElidableParagraph

			PropertyChanges {
				target: fb_label
				horizontalAlignment: Text.AlignJustify
				wrapMode: Text.WrapAtWordBoundaryOrAnywhere
				elide: Text.ElideRight
			}
		}
	]
}
