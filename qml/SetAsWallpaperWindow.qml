import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import FlickrBrowserCore 1.0

// TODO: window content in a separate component?
// FIXME: background color which reset at computer reboot.
Window {
	id: wallpaper_window
	title: qsTr("Définir %1 en tant que papier peint").arg(photo.name)

	// Minimal sizes.
	minimumWidth:   2*constants.margin
				  + Math.max(
						photo_section.min_width,
						wallpaper_opts.width,
						wallpaper_location.min_width,
						wallpaper_new_color.min_width,
						bottom_buttons.width
					)
	minimumHeight:   11*constants.margin
				   + photo_section.min_height
				   + wallpaper_opts.height
				   + wallpaper_location.min_height
				   + wallpaper_new_color.min_height
				   + bottom_buttons.implicitHeight

	Constants { id: constants }

	// Photo management
	property string photo_id
	property alias photo: photo_controller.photo

	PhotoController { id: photo_controller }

	onPhoto_idChanged: photo_controller.setPhoto(photo_id)

	// Wallpaper management
	WallpaperController { id: wp_controller }

	/// @brief Wallpaper original settings, before any changes.
	QtObject {
		id: original_wallpaper

		property string picture
		property int opt
		property color bg_color

		function update() {
			original_wallpaper.picture = wp_controller.wallpaper_path
			original_wallpaper.opt = wp_controller.wallpaper_option
			original_wallpaper.bg_color = wp_controller.background_color
		}

		function resetWallpaper() {
			return wp_controller.setAsWallpaper(
				original_wallpaper.picture,
				original_wallpaper.opt,
				original_wallpaper.bg_color
			)
		}
	}

	/// @brief Reset original settings (QML side)
	function reinitWallpaperSettings() {
		opts.currentIndex = opts.model.findIndex(function (opt) {
			return opt.id === wp_controller.wallpaper_option
		})
		color_dialog.setNewColor(wp_controller.background_color)
	}

	onVisibleChanged: {
		// Refreshing the window while opening it
		if (wallpaper_window.visible) {
			wp_controller.refreshWallpaperDatas()
			original_wallpaper.update()
			reinitWallpaperSettings()
			window_content.state = ""
		}
	}

	Component.onCompleted: {
		// First populate with values at program launch.
		original_wallpaper.update()
		reinitWallpaperSettings()
	}

	// Window content
	Item {
		id: window_content

		readonly property string wallpaper_changed_state: "wallpaper changed"

		anchors {
			fill: parent
			margins: constants.margin
		}

		// Photo section
		Item {
			id: photo_section
			height:   wallpaper_window.height
					- wallpaper_opts.height
					- wallpaper_location.height
					- wallpaper_new_color.height
					- bottom_buttons.height
					- 10*constants.margin

			property int min_width: image_label.implicitWidth + constants.margin
			property int min_height: image_label.implicitHeight

			anchors {
				top: parent.top
				left: parent.left
				right: parent.right
			}

			FlickrBrowserLabel {
				id: image_label
				text: qsTr("Image :")
				text_style: FlickrBrowserLabel.TextStyle.SectionTitle

				anchors {
					top: photo_section.top
					left: photo_section.left
				}
			}

			FlickrBrowserImage {
				id: wallpaper
				photoID: wallpaper_window.photo_id

				anchors {
					top: photo_section.top
					left: image_label.right
					leftMargin: 2*constants.margin
					right: photo_section.right
					bottom: photo_section.bottom
				}
			}
		}

		// Wallpaper option combo box
		RowLayout {
			id: wallpaper_opts
			spacing: 2*constants.margin

			anchors {
				top: photo_section.bottom
				topMargin: 2*constants.margin
				left: parent.left
			}

			FlickrBrowserLabel {
				id: opts_label
				text: qsTr("Taille du papier peint :")
				text_style: FlickrBrowserLabel.TextStyle.SectionTitle
			}

			// Choosing wallpaper option
			FlickrBrowserComboBox {
				id: opts
				Layout.preferredWidth: width
				unselected_text: qsTr("Sélectionnez une option")

				WallpaperOpts { id: wopts }

				// TODO: think windows 7 for span
				model: [
					wopts.toModel(WallpaperOpts.CENTER),
					wopts.toModel(WallpaperOpts.TILE),
					wopts.toModel(WallpaperOpts.STRETCH),
					wopts.toModel(WallpaperOpts.KEEP_ASPECT),
					wopts.toModel(WallpaperOpts.CROP_TO_FIT),
					wopts.toModel(WallpaperOpts.SPAN),
				]

				getModelText: function (i) {
					return model[i].text
				}

				function getSelectedWallpaperOption() {
					return opts.model[currentIndex].id
				}
			}
		}

		// Save the wallpaper as...
		Item {
			id: wallpaper_location
			height: location_title.height + location_row.height + constants.margin

			property int min_width: Math.max(
										location_title.implicitWidth,
										location_row.implicitWidth
									)
			property int min_height:   location_title.implicitHeight
									 + location_row.implicitHeight
									 + constants.margin

			anchors {
				top: wallpaper_opts.bottom
				topMargin: 2*constants.margin
				left: parent.left
				right: parent.right
			}

			// Section title
			FlickrBrowserLabel {
				id: location_title
				text: qsTr("Emplacement du papier peint :")
				text_style: FlickrBrowserLabel.TextStyle.SectionTitle

				anchors {
					top: wallpaper_location.top
					left: wallpaper_location.left
					right: wallpaper_location.right
				}
			}

			RowLayout {
				id: location_row

				anchors {
					left: wallpaper_location.left
					right: wallpaper_location.right
					bottom: wallpaper_location.bottom
				}

				spacing: constants.margin

				// Display file location
				TextField {
					id: location_entry
					text: photo_name.fileurlWithoutFileScheme()

					// TODO: better placeholder, with photo full path?
					placeholderText: qsTr("Emplacement du papier-peint")

					Layout.fillWidth: true
				}

				// Changing future wallpaper location
				Button {
					id: change_location
					text: qsTr("Changer")

					// Dialog for choosing location.
					SaveImageDialog {
						id: photo_name
						photo_id: wallpaper_window.photo_id
						save_photo: false
						title: qsTr("Emplacement du futur papier-peint")
						visible: false
					}

					onClicked: if (!photo_name.visible) photo_name.open()
				}
			}
		}

		// Choosing a new background color if you want to
		Item {
			id: wallpaper_new_color
			height: new_color_switch.height + constants.margin + second_row.implicitHeight

			// Minimal sizes
			property int min_width: Math.max(
										new_color_switch.implicitWidth,
										second_row.implicitWidth
									)
			property int min_height:   new_color_switch.implicitHeight
									 + second_row.implicitHeight
									 + constants.margin

			anchors {
				top: wallpaper_location.bottom
				topMargin: 2*constants.margin
				left: parent.left
				right: parent.right
			}

			Switch {
				id: new_color_switch
				checked: false

				contentItem: FlickrBrowserLabel {
					text: qsTr("Changer aussi la couleur ?")
					text_style: FlickrBrowserLabel.TextStyle.SectionTitle
				}

				anchors {
					top: wallpaper_new_color.top
					left: wallpaper_new_color.left
					right: wallpaper_new_color.right
				}
			}

			RowLayout {
				id: second_row
				spacing: constants.margin

				anchors {
					top: new_color_switch.bottom
					topMargin: constants.margin
					left: wallpaper_new_color.left
					right: wallpaper_new_color.right
					bottom: wallpaper_new_color.bottom
				}

				// The original color. At least a square
				Rectangle {
					id: future_color
					color: {
						if (chameleon.checked) {
							return photo.chameleon_color
						}
						else {
							return color_dialog.color
						}
					}

					onColorChanged: color_dialog.setNewColor(future_color.color)

					Layout.preferredHeight: Math.max(chameleon.height, choose_color.height)
					Layout.fillHeight: true
					Layout.fillWidth: true
				}

				// Choose new color
				Button {
					id: choose_color
					text: qsTr("Changer")
					enabled: new_color_switch.checked && !chameleon.checked

					ColorDialog {
						id: color_dialog
						visible: false
						currentColor: future_color.color
						color: future_color.color

						function setNewColor(newColor) {
							color_dialog.currentColor = newColor
							color_dialog.color = newColor
						}
					}

					onClicked: {
						if (!color_dialog.visible) {
							color_dialog.open()
						}
					}
				}

				// Chameleon switch
				Switch {
					id: chameleon
					text: qsTr("Couleur caméléon")
					enabled: new_color_switch.checked
					checked: true
				}
			}
		}

		// Actions: set as wallpaper, reinit and cancel.
		RowLayout {
			id: bottom_buttons
			spacing: constants.margin

			anchors {
				right: parent.right
				bottom: parent.bottom
			}

			// Set as wallpaper button
			Button {
				id: set_wallpaper_button
				text: qsTr("Définir en tant que papier-peint")
				enabled: location_entry.text.length > 0

				SaveImageKOMessageBox {
					id: save_ko
					namefile: location_entry.text
				}

				// Save photo and set wallpaper
				onClicked: {
					if (photo_controller.savePhoto(location_entry.text)) {
						// Set wallpaper
						var newBgColor = new_color_switch.checked ?
									future_color.color
								  : wp_controller.background_color

						if (wp_controller.setAsWallpaper(location_entry.text, opts.getSelectedWallpaperOption(), newBgColor)) {
							window_content.state = window_content.wallpaper_changed_state
						}

						// TODO: else err msg?
					}
					else {
						save_ko.open()
					}
				}
			}

			// Reset wallpaper button.
			Button {
				id: reinit_button
				text: qsTr("Remettre le papier-peint original")
				enabled: false

				onClicked: {
					if (original_wallpaper.resetWallpaper()) {
						window_content.state = ""
					}

					// TODO: else err msg?
				}
			}

			// Closing window
			Button {
				id: close_button
				text: qsTr("Fermer")

				onClicked: wallpaper_window.close()
			}
		}

		// Window states
		states: [
			// Default state, when wallpaper is not changed.
			State {
				name: ""
				PropertyChanges {
					target: new_color_switch
					checked: false
				}
				PropertyChanges {
					target: reinit_button
					enabled: false
				}
				StateChangeScript {
					name: "reinitWpSettings"
					script: reinitWallpaperSettings()
				}
			},

			// When wallpaper is changed
			State {
				name: window_content.wallpaper_changed_state
				PropertyChanges {
					target: reinit_button
					enabled: true
				}
				StateChangeScript {
					name: "newBgColor"
					script: color_dialog.setNewColor(wp_controller.background_color)
				}
			}
		]
	}
}
