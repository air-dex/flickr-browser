import QtQuick 2.12
import QtQuick.Dialogs 1.3
import FlickrBrowserCore 1.0

/// @brief Base component for file dialogs.
// TODO: using a hind of Qt Quick Controls 2's SaveDialog instead when possible.
// TODO: verify that bugs due to Qt will be definitely over with Qt 5.12.1.
FileDialog {
	id: fb_file_dialog

	// Internals. Have to nest it in a QML for God knows why.
	Item {
		id: fbfd_internals
		FlickrBrowserHelper { id: helper }
	}

	modality: Qt.WindowModal
	selectMultiple: false
	selectExisting: false

	// TODO: option to choose home dir instead
	folder: helper.getPicturesFolderURL()

	// Removes the file: scheme from file URL.
	function fileurlWithoutFileScheme() {
		var regex

		switch (Qt.platform.os) {
			// No / for root on Windows
			case "windows":
			case "winrt":
				regex = /^file\:\/*(.*)$/i
				break

			case "osx":
			case "linux":
			case "unix":
			case "android":
				regex = /^file\:\/*(\/.*)$/
				break

			// Don't know (and don't care) for other OSes.
			default:
				return ""
		}

		var match = regex.exec(fileUrl)
		return (match === null) ? "" : match[1]
	}
}
