import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import FlickrBrowserCore 1.0

/// @brief Tab for looking at one album
Item {
	// For program constants
	Constants { id: constants }

	// Back office
	FlickrBrowserHelper { id: controller }

	// Minimum sizes
	property int min_width: 2*constants.margin
							+ Math.max(
								album_row.width,
								album_viewer.min_width,
								bottom_buttons.width
							  )
	property int min_height: 6*constants.margin
							 + album_row.height
							 + album_viewer.min_height
							 + bottom_buttons.height

	// TODO: compute width
	height: 6*constants.margin
			+ album_row.height
			+ album_viewer.height
			+ bottom_buttons.height

	// Tab content
	Item {
		id: view_album_tab
		anchors {
			fill: parent
			margins: constants.margin
		}

		// Album row
		RowLayout {
			id: album_row

			anchors {
				top: view_album_tab.top
				left: view_album_tab.left
			}

			spacing: 2*constants.margin

			// Label album
			FlickrBrowserLabel {
				id: album_label
				text: qsTr("Album :")
				text_style: FlickrBrowserLabel.TextStyle.SectionTitle
			}

			// Selecting album
			FlickrBrowserComboBox {
				id: select_album
				unselected_text: qsTr("Sélectionnez un album")
				Layout.preferredWidth: width

				getModelText: function (i) {
					return model[i].title
				}

				model: controller.getAlbumSelectModel()

				function getSelectedAlbumID() {
					return model[currentIndex].id
				}
			}
		}

		// Album viewer
		AlbumViewer {
			id: album_viewer

			// Binding the selected album to the viewer.
			album_id: select_album.getSelectedAlbumID()

			anchors {
				top: album_row.bottom
				topMargin: 2*constants.margin
				left: view_album_tab.left
				right: view_album_tab.right
				bottom: bottom_buttons.top
				bottomMargin: 2* constants.margin
			}
		}

		// Bottom buttons row
		AlbumButtonRow {
			id: bottom_buttons
			show_download: true
			show_fullscreen: true
			album_id: select_album.getSelectedAlbumID()
			photo_id: album_viewer.getCurrentPhotoID()

			anchors {
				right: view_album_tab.right
				bottom: view_album_tab.bottom
			}
		}
	}
}
