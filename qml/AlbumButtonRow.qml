import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
//import FlickrBrowserCore 1.0

// Bottom buttons row
RowLayout {
	id: bottom_buttons
	spacing: constants.margin

	Constants { id: constants }

	property alias show_download: dl_button.visible
	property alias show_fullscreen: sfs_button.visible
	property string album_id: ""
	property string photo_id: ""

	// Set selected photo as wallpaper
	Button {
		id: wallpaper_button
		text: qsTr("Définir en tant que papier-peint")
		visible: Qt.platform.os === "windows"

		SetAsWallpaperWindow {
			id: wallpaper_window
			photo_id: bottom_buttons.photo_id
			visible: false
		}

		onClicked: {
			if (!wallpaper_window.visible) {
				wallpaper_window.showNormal()
			}
		}
	}

	// Save photo
	Button {
		id: save_photo_button
		text: qsTr("Sauvegarder la photo")

		SaveImageDialog {
			id: save_dialog
			photo_id: bottom_buttons.photo_id
			visible: false
		}

		onClicked: {
			if (!save_dialog.visible) {
				save_dialog.open()
			}
		}
	}

	// Show album fullscreen
	Button {
		id: sfs_button
		text: qsTr("Voir l'album en plein écran")

		ShowFullscreenWindow {
			id: sfs_dialog
			album_id: bottom_buttons.album_id
			visible: false
		}

		onClicked: {
			if (!sfs_dialog.visible) {
				sfs_dialog.showMaximized()
			}
		}
	}

	// TODO: download album
	Button {
		id: dl_button
		text: qsTr("Télécharger l'album")
		onClicked: console.log("TODO: download album")
	}
}
