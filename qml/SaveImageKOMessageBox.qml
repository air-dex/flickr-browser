import QtQuick 2.12
import QtQuick.Dialogs 1.3

MessageDialog {
	id: save_ko

	property string namefile

	// Internals. Have to nest it in a QML for God knows why.
	Item { Constants { id: constants } }

	title: constants.program_title
	icon: StandardIcon.Critical
	text: qsTr("L'image %1 n'a pas pu être enregistrée.").arg(namefile)
}
